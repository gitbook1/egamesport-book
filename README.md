# Introduction

## TODOs
- [ ] 前台
  - [ ] 電競新聞列表配置調整
  - [ ] 電競指南選單項目配置（去除下拉選單配置，直接以電競新聞形式配置！）
  - [ ] 側邊欄內容動態配置
  - [ ] 聯絡我們表單送出功能

  - [x] 公司推薦內容欄位動態對應（包含授權到區域配置）
  - [x] 表格於行動版顯示狀況修正！「直接隱藏」
  - [x] 表格的read more機制
- [x] 首頁行動版面配置調整
  - [x] 數據表格統整篩選
  - [x] 加入回頂端按鈕

- [ ] 後台網址
＝＝＝＝＝＝＝＝＝＝＝＝
- [ ] 確認修正數據抓取後的遊戲列表對應改用名稱比對處理

[規則]抓取下來的遊戲對應改用[Name]名稱比對避免重複出現(抓取數據會出現Id不同但Name相同狀況！)

[WebpToPng](https://ezgif.com/webp-to-png)
先將data:image/webp; base64;..內容透過網頁下載儲存再上傳。

[討論]

**爬蟲數據抓下來的電競遊戲名稱跟編號會有無法對應問題（縮寫、大小寫、有無空白等）**
=> 解法：使用統一小寫去除空白方式的名稱做基本對應。
（目前新聞跟平台設定的遊戲使用相同來源）



前台
- [x] 提供靜態頁面內容以變更，建議給圖片
- [x] 數據顯示順序無差異
- [x] 數據Odds切換規則？
- [ ] RWD手機模板顯示方式
後台
- [x] 語系文字檔先以畫面截圖或描述替換方式處理
- [x] 聯絡我們作法
  - [x] 表單形式指定email位置後台可
- [x] 排程執行1小時1次

- [ ] 電競新聞內頁編輯模式限制與格式

[規則] 列表顯示以15分為間隔單位，距離當下時間最近的時間點開始呈現。

## Done
- [x] 建立scriptsAPI目錄進行資料抓取並於aws上配置對應環境開始爬蟲
  - [ ] * 12,18,0,6 * * * /usr/local/php /Website/egameboard/scripts/cli.php main fetch
- [x] 本機Vagrant資料庫替換！改每2小時抓一次！
/usr/local/lsws/lsphp72/bin/php scripts/cli.php main fetch boarddata
- [x] 完成models目錄schema檔建立
  - 爬蟲資訊
    - [x] 修正定期爬蟲取得資訊
- [x] 後台選單配置 => 先顯示DataTable資料 => 以最新消息來建立contentbuilder

CentOS7
IP: 154.215.31.223
NETMASK：255.255.255.128
GATEWAY：154.215.31.1
登录帐号：root
登录密码：mdD$1214.=oOu

```bash
$ ssh root@154.215.31.223

mdD$1214.=oOu
```

Mysql root password
FWFV32fu5,tnj53@T2

CREATE USER 'esportUser'@'localhost' IDENTIFIED BY 'FV32fu5,tnj53@T2';
GRANT ALL PRIVILEGES ON *.* TO 'esportUser'@'localhost' IDENTIFIED BY 'FV32fu5,tnj53@T2'
GRANT ALL PRIVILEGES ON *.* TO 'esportUser'@'%' IDENTIFIED BY '@Omge09544'

建立新的使用者
esportUser
ssh 密碼 FV3%2fgk%!rjf4TR

```bash
$ ssh esportUser@154.215.31.223

FV3%2fgk%!rjf4TR
```