<?php

//load Phalcon
use \Phalcon\DI\FactoryDefault\CLI as CliDI,
    \Phalcon\CLI\Console as ConsoleApp,
    \Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

use \Personalwork\Mail;

define('VERSION', '1.0.0');

date_default_timezone_set('Asia/Taipei');

 //Using the CLI factory default services container
 $di = new CliDI();

 // Define path to application directory
 defined('PPS_APP_APPSPATH')
 || define('PPS_APP_APPSPATH', realpath(dirname(__FILE__) . '/../apps'));
 defined('PPS_APP_PROJECTNAME')
 || define('PPS_APP_PROJECTNAME', 'egameboard');

 /**
  * Register the autoloader and tell it to register the tasks directory
  */
 $loader = new \Phalcon\Loader();
 $loader->registerDirs(
     array(
         PPS_APP_APPSPATH . '/tasks',
         PPS_APP_APPSPATH . '/../vendor/'
     )
 );
 $loader->registerNamespaces(
     array(
         ucfirst(PPS_APP_PROJECTNAME) . '\Models'=>PPS_APP_APPSPATH . '/models'
     )
 );
 if (!file_exists(PPS_APP_APPSPATH . '/../vendor/autoload.php')) {
    echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
    exit(1);
}
require_once PPS_APP_APPSPATH . '/../vendor/autoload.php';
 $loader->register();

 // Load the configuration file (if any)
 if(is_readable(PPS_APP_APPSPATH . '/../config/config.php')) {
    $config = include PPS_APP_APPSPATH . '/../config/config.php';
 }elseif(is_readable(PPS_APP_APPSPATH . '/../config/config.php')) {
    $config = include PPS_APP_APPSPATH . '/../config/config.php';
 }
    $di->set('config', $config);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di['db'] = function () use ($di) {
    return new DbAdapter(array(
        "host"      => $di->get('config')->database->host,
        "username"  => $di->get('config')->database->username,
        "password"  => $di->get('config')->database->password,
        "dbname"    => $di->get('config')->database->dbname,
        'charset'   => $di->get('config')->database->charset,
        "options"   => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));
};
/**
 * Mail service use Personalwork\Mail(swiftmailer)
 */
$di->set('mail', function () {
    return new Mail();
});

/**
 * 2015-02-10
 * 建立Logger DI
 */
$di->set('logger', function () use ($di) {
    $logger = new \Personalwork\Logger\Adapter\Database(array(
                    'db'    => $di->get('db'),
                    'table' => "logger",
                    'name'  => 'info'
                  ));
    return $logger;
});

 //Create a console application
 $console = new ConsoleApp();
 $console->setDI($di);

 /**
 * Process the console arguments
 */
 $arguments = array();
 foreach($argv as $k => $arg) {
     if($k == 1) {
         $arguments['task'] = $arg;
     } elseif($k == 2) {
         $arguments['action'] = $arg;
     } elseif($k >= 3) {
        $arguments['params'][] = $arg;
     }
 }

 // define global constants for the current task and action
 define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
 define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));

 try {
     // handle incoming arguments
     $console->handle($arguments);
 }
 catch (\Phalcon\Exception $e) {
     echo $e->getMessage();
     exit(255);
 }
?>