/**
 * 過往開發特定應用的jQuery plugin類型函式集合
 * 1. $.keyupCalculate()
 * 2. $.clickToggle()
 * */

/**
 * keyup quality calculate from pkerp
 * 輸入後計算特定欄位值作法
 * */
$.fn.keyupCalculate = function( ) {

    prices = 0; amount = 0;

    this.each(function(index, elm){
        //顯示小數
        price = parseFloat($(elm).find(".price").val());
        qty = parseInt($(elm).find(".quality").val());
        // console.log( qty );
        if( isNaN(price) || isNaN(qty) ) return;
        prices += price*qty;
        amount += qty;
        // console.log( amount );
    });

    $(this).parents("table").find('.totalPrice').val(new Number(prices).toFixed(2))
                    .attr({value: new Number(prices).toFixed(2), 'data-original-title': new Number(prices).toFixed(2)});
    $(this).parents("table").find('.totalGoods').val(amount)
                    .attr({value: amount});

}


/**
 * toggle function
 * click單按與雙按事件
 * */
$.fn.clickToggle = function( f1, f2 ) {
    return this.each( function() {
        var clicked = false;
        $(this).bind('click', function() {
            if(clicked) {
                clicked = false;
                return f2.apply(this, arguments);
            }

            clicked = true;
            return f1.apply(this, arguments);
        });
    });

}