/**
 * 預設首頁底部載入js檔清單
 */
function _bind_multiselect($with_filter){
    if( $with_filter ){
        $('.multiselect-filter').multiselect({
            nonSelectedText: '請選擇(可搜尋)',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            maxHeight: 200,
            onChange: function(option, checked) {
                if( typeof multiselectfilter_onChange == 'function' ){
                    multiselectfilter_onChange(option, checked);
                }
            }
        });
    }else{
        $('select.multiselect').multiselect({
            nonSelectedText: '請選擇',
            maxHeight: 300,
            onChange: function(option, checked) {
                if( typeof multiselect_onChange == 'function' ){
                    multiselect_onChange(option, checked);
                }
            }
        });
    }
}

function _bind_widgetfilter()
{
    $(".widget-filter").on({
        click: function(){
            target=$(this).parents('.widget-inside').find('.widget-content > div');
            q = $(this).data('filter');
            $(target).find(".searchable").hide().filter(q).show();
        }
    });
}

function _bind_widgetsreach()
{
    $('.widget-search').next().on({
        click: function(){
            q = $(this).prev().val();
            BLOCK = $( $(this).prev().data('target') );
            if( q == '' ){
                BLOCK.find(".searchable").show();
            }else{
                BLOCK.find(".searchable:visible").hide().filter(":contains('"+q+"')").show();
            }
        }
    });
    if( $('.widget-search-recovery').length ){
        $('.widget-search-recovery').on({
            click: function() {
                $($(this).data('target')).find(".searchable").show();
            }
        });
    }
}

/**
 * 1. （針對block-nav-leftside.volt）選單結構為.main-menu > .sub-menu-toggle時
 * 2. 處理收合展開次選單
 * */
function _bind_menu_collapseexpend()
{
    $('.main-menu .sub-menu-toggle').off('click').on('click',function(e){
        $li = $(this).parents('li');
        if( !$li.hasClass('active')){
            $li.find('.toggle-icon').removeClass('fa-angle-left').addClass('fa-angle-down');
            $li.addClass('active');
        } else {
            $li.find('.toggle-icon').removeClass('fa-angle-down').addClass('fa-angle-left');
            $li.removeClass('active');
        }
        $li.find('.sub-menu').slideToggle(300);
        e.preventDefault();
    });

    // 防止觸發下面的clickToggle
    $(".main-menu, .sidebar-content").click(function(e){
        e.stopPropagation();
    });

    //設定left-sidebar高度與視窗高度一致
    windowHeight = $(window).innerHeight();
    $('.left-sidebar, .content-wrapper').css('min-height', windowHeight);
    $('.left-sidebar, .js-toggle-minified').clickToggle(
        function() {
            $('.left-sidebar').addClass('minified');
            $('.content-wrapper').addClass('expanded');

            $('.left-sidebar .sub-menu')
            .css('display', 'none')
            .css('overflow', 'hidden');

            $('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
        },
        function() {
            $('.left-sidebar').removeClass('minified');
            $('.content-wrapper').removeClass('expanded');
            $('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
        }
    );
}

/*******************************************
 *  TAB CONTENT AJAX LOAD
 * 使用a作為tab，屬性加入data-target（div#id），選擇性搭配callback function
 * callback function定義在
/********************************************/
function _tabs_toggle_ajaxload( $nowtab )
{
    var loadurl = $nowtab.attr('href'),
        targ = $nowtab.data('target');
    var callback = $nowtab.data('callback');
    var defaultStatusHtml = '<i class="fa fa-refresh fa-spin fa-3x fa-fw margin-bottom"></i><span class="">載入中...</span>';

    $.ajax({
        url : loadurl,
        dataType : 'html',
        beforeSend : function(){
            $nowtab.tab('show');
            $(targ).html(defaultStatusHtml);
        },
        success : function( resp ){
            $(targ).html(resp);
        },
        complete :  function(){
            if( typeof window[callback] == "function" ){
                window[callback]();
            }
        }
    });
}


/**
 * 配置讓套用DataTables的物件區塊處理ajax-load
 * */
function _trigger_datatables_ajaxload(something_with_data_setfor_datatable_info) {

    TABLEID = $(something_with_data_setfor_datatable_info).data('tableid');
    AJAXURL = $(something_with_data_setfor_datatable_info).data('dtconf');

    if ( ! $.fn.DataTable.isDataTable( TABLEID ) ) {
        $.ajax({
            url : AJAXURL,
            dataType : 'json',
            // 處理dataTable內容更新
            success : function( resp ) {
                dTable = getDataTableInstance(TABLEID, resp);
                // with yadcf fliter
                if( resp.yadcf ){
                  // 載入欄位過濾套件
                  yadcf.init(dTable, resp.yadcf);
                }
                dTable.ajax.reload();
            }
        });
    }else{
        $(TABLEID).DataTable().ajax.reload();
    }
}


/**
 * 處理以頁籤形式放置的dataTable透過Ajax方式重新載入作法
 * */
function _tabs_toggle_datatable_ajaxload() {
var dTable;

    if( $(".block-tabs-ajaxtable").length ){
        $(".block-tabs-ajaxtable a").click(function(e){
            e.preventDefault();
            $(this).tab('show');
        });
        $('.block-tabs-ajaxtable a[data-toggle="tab"]')
        .on('shown.bs.tab', function (event) {
            _trigger_datatables_ajaxload(event.target);
        });

        $('.block-tabs-ajaxtable a[data-toggle="tab"]:first').tab('show');
    }
}

/**
 * 配置透過指定tabeId以及給予特定table option值來初始化dataTable
 * 預設由 function _tabs_toggle_datatable_ajaxload()內
 * 當顯示頁籤事件觸發時呼叫
 *
 * 2018-04-02
 * San
 * 附加透過tableDef函式取得extconf內配置dtFooterColumns變數定義是否附加底部總計整欄數值處理
 *
 * @param string tableId
 * @param object extconf [由ajax呼叫後端取得json配置]
 * */
function getDataTableInstance(tableId, extconf) {
    var options = {
            retrieve: true,
            destroy : true,
            processing : true,
            serverSide : true,
            // 不使用分頁項目
            dom : "<'col-sm-12'i><'col-sm-12'tr><'col-sm-12'p>",
            language : {
                loadingRecords : "載入中...",
                processing :     "處理中...",
                emptyTable : "目前無任何數據紀錄",
                info : "顯示全部 _TOTAL_ 筆紀錄中從 _START_ 到 _END_ 筆",
                infoEmpty : "顯示 0 筆紀錄",
                infoFiltered : "(篩選出 _MAX_ 筆紀錄)",
                paginate : {
                  "first": "第一頁",
                  "last": "最後頁",
                  "previous": "上頁",
                  "next": "下頁"
                }
            },
            initComplete: function (settings, resp) {
            },
            /**
             * called before initComplete
             * @see https://datatables.net/forums/discussion/27159/footercallback-both-totals-coming-back-as-page-totals
             * */
            footerCallback : function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                $( extconf.dtFooterColumns ).each(function(idx, COLUMN_INDEX ){
                    // 以下是不論分頁整欄加總值
                    // var total = api
                    //         .column( 4 )
                    //         .data()
                    //         .reduce( function (a, b) {
                    //             return intVal(a) + intVal(b);
                    //         }, 0 );

                    // Total over this page
                    var pageTotal = api
                                    .column( COLUMN_INDEX , { page: 'current'} )
                                    .data()
                                    .reduce( function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0 );

                    // Update footer
                    $( api.column( COLUMN_INDEX  ).footer() ).html( "<div style='width:100%;text-align:left;color:red;'>"+pageTotal+"</div>");
                });
                // console.dir( row );

            }
        };
    $.extend(options , extconf);

    var dTable  = $(tableId).DataTable(options);

    return dTable;
}


$(function() {
    $('.main-header > em').click(function(){ $('article.alert').show(); });

	$("[rel=tooltip]").tooltip();

    $('.demo-cancel-click').click(function(){return false;});

	if( $('#flow-wizard').length > 0 && $('.steps li:first').is('.active') ){
		$(".wizard-prev").hide();
	}

    /************************
     * Leftside Main Navigation
    /************************/
    _bind_menu_collapseexpend();


    /************************
     * Widget Toggle Expand
     ************************/
    $('.widget .btn-toggle-expand').clickToggle(
        function(e) {
            e.preventDefault();
            target=$(this).parents('.widget').find('.widget-content');
            if( target.is(':visible') )
                target.slideUp(300);
            else
                target.slideDown(300);
            $(this).find('i.fa-chevron-down').toggleClass('fa-chevron-up').end().find('i.fa-chevron-up').toggleClass('fa-chevron-down');
        },
        function(e) {
            e.preventDefault();
            target=$(this).parents('.widget').find('.widget-content');
            if( target.is(':visible') )
                target.slideUp(300);
            else
                target.slideDown(300);
            $(this).find('i.fa-chevron-up').toggleClass('fa-chevron-down').end().find('i.fa-chevron-down').toggleClass('fa-chevron-up');
        }
    );

    /************************
    /*  BOOTSTRAP POPOVER
    /************************/
    $('.helper').popover({
        container: 'body',
        html: true,
        title: '<i class="fa fa-book"></i> Help',
        content: "Help summary goes here. Options can be passed via data attributes <code>data-</code> or JavaScript. Please check <a href='http://getbootstrap.com/javascript/#popovers'>Bootstrap Doc</a>"
    });

    /**
     * @see js/plugins/bootstrap-multiselect
     */
    // if( $('select.multiselect').length ){
    //     _bind_multiselect(0);
    // }

    // if( $('.multiselect-filter').length ){
    //     _bind_multiselect(1);
    // }

    /*******************************************
     *  Widget Content Search
    /********************************************/
    // if( $('.widget-search').length ){
    //     _bind_widgetsreach();
    // }
    // if( $(".widget-filter").length ){
    //     _bind_widgetfilter();
    // }

    /************************
    /*  CUSTOM TOP MENU
    /************************/
    // $(".sub-menu li").draggable({
    //     cursor: "move",
    //     cursorAt: { left: 10, top:10 },
    //     handle: 'i.fa',
    //     // addClasses: false,
    //     appendTo: "body",
    //     containment: "body",
    //     helper: "clone",
    //     revert: true,
    //     start : function(event, ui) {
    //         $(ui.helper).addClass("menu-item-helper");
    //     }
    // });
    // $("#topCustomMenu").droppable({
    //     accept: ".sub-menu li",
    //     hoverClass: "drag-active-highlight",
    //     addClasses: false,
    //     activate : function(event, ui ){
    //         $("#topCustomMenu").addClass( "drag-active-highlight" );
    //     },
    //     drop : function(event, ui){
    //         //$( this ).Class( "ui-state-highlight" );
    //         $item =ui.draggable;
    //         $item.fadeOut(function() {
    //             $item.addClass('menu-item')
    //                  .addClass('pull-left')
    //                  .appendTo( $("#topCustomMenu") ).fadeIn(function(){});
    //         });
    //         $("#topCustomMenu").removeClass( "drag-active-highlight" );

    //         $.ajax({
    //             url : '/frontend/index/customTopMenu',
    //             data : { link: $item.find("a").attr('href'), label: $item.find("span").text() },
    //             type : 'POST',
    //             dataType : 'html',
    //             success : function( resp ){
    //                 $(resp).insertBefore(".content-breadcrumb");
    //             }
    //         });
    //         // console.log( url+' '+label );
    //     },
    //     deactivate : function(event, ui){
    //         $("#topCustomMenu").removeClass( "drag-active-highlight" );
    //     }
    // });


    /*******************************************
     * 處理鍵盤操作
     * 1. 偵測Enter事件可以改為TAB作法
     * @see http://stackoverflow.com/questions/2335553/jquery-how-to-catch-enter-key-and-change-event-to-tab
    /********************************************/
    $(document).on("keydown", "input", function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        //add select too
        if(key == 13) {
            e.preventDefault();
            var inputs = $(this).closest('form').find(':input:visible');
            inputs.eq( inputs.index(this)+ 1 ).focus();
            e.preventDefault();
            return false;
        }
    }).on("focus", ".numfield", function(e){
        $(this).select();
    });
});


/*******************************************
 * 預設編輯視窗/刪除按鈕觸發動作
/********************************************/
$(document).on("click", ".btn-modal",function(e){
    $url = $(this).data('remote');
    $exclass = $(this).data('mclass');
    var $callback = window[$(this).data('callback')];

    $.ajax({
        url : $url,
        dataType : 'html',
        beforeSend : function(){
            $("#layoutModal").empty();
        },
        success : function( resp ){
            $("#layoutModal").html( resp ).find('.modal-dialog').addClass($exclass);
        },
        complete : function(){
            $("#layoutModal").modal('show');
            if (typeof $callback === "function" ) {
                $callback();
            }
        }
    });
}).on("click", ".btn-delete, .btn-remove",function(e){
    window.location = $(this).data('remote');

// 建檔列表編輯模式切換開關
}).on("click", "#enableSwitch",function(e){
    $('.editable').editable('toggleDisabled');
});

$(document).on("click", ".btn-append", function(){
    targetid = $(this).data('target');
    field = $( targetid ).find('.fieldforclone').clone().removeClass('hide').removeClass('fieldforclone').addClass('row').appendTo(targetid);

    field.find("[id$='forclone']").map(function(){

        $(this).attr('name', $(this).attr('id').replace('forclone','[]') )
               .removeAttr('id');

        if( $(this).prop('tagName') == 'SELECT' ) {
            if( $(this).data('class') == 'multiselect-filter' ){
                $(this).addClass('multiselect-filter').multiselect({
                        nonSelectedText: '請選擇(可查詢)',
                        enableFiltering : true,
                        enableCaseInsensitiveFiltering : true,
                        maxHeight: 200,
                        onChange: function(option, checked) {
                            if( typeof multiselectfilter_onChange == "function" ){
                                multiselectfilter_onChange(option, checked);
                            }
                        }
                   });
            }else{
                $(this).addClass('multiselect').multiselect({
                        nonSelectedText: '請選擇(可查詢)',
                        maxHeight: 200,
                        onChange: function(option, checked) {
                            if( typeof multiselect_onChange == "function" ){
                                multiselect_onChange(option, checked);
                            }
                        }
                    });
            }
        }
    });
    // .removeAttr('id').attr('name','SupplierId[]').addClass('multiselect').multiselect({
    //     nonSelectedText: '請選擇(可查詢)',
    //     enableFiltering: true,
    //     enableCaseInsensitiveFiltering: true,
    //     maxHeight: 200
    // });
    // field.find('#BasicUnitIdforclone').removeAttr('id').attr('name','BasicUnitId[]').addClass('multiselect').multiselect({
    //     nonSelectedText: '請選擇',
    //     maxHeight: 200
    // });
    // field.find('#basicPriceforclone').removeAttr('id').attr('name','basicPrice[]');
});

/*******************************************
 *	FLOW WIZARD
 * 1. 流程型表單
/********************************************/
$(document).on('click' ,'.wizard-next', function(){
	$(".wizard-form .help-block").empty().hide();
	$('#flow-wizard').wizard('next');
});
$(document).on('click' ,'.wizard-prev', function(){
	$(".wizard-form .help-block").empty().hide();
	$('#flow-wizard').wizard('previous');
});

/*
 * 參考king-common.js line 114 處理widget focus作法
 * 並附加 line 24 收合側邊選單區塊 js-toggle-minified 作法
 * 處理控制區塊全螢幕
 */
$(document).on('click' ,'.btn-fullmode', function(e){
	e.preventDefault();
	$target = $(this).data('target');
	//強制側邊選單收合狀態
	$('.left-sidebar').addClass('minified');
	$('.content-wrapper').addClass('expanded');
	$('.left-sidebar .sub-menu')
	.css('display', 'none')
	.css('overflow', 'hidden');
	$('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
	//強制上方工具列隱藏
	$('.top-bar').addClass('hide');

	$(this).removeClass('btn-fullmode').addClass('btn-normalmode')
		   .find('.fa-arrows-alt').addClass('fa-minus-circle text-danger').removeClass('fa-arrows-alt');

	$( $target ).parents('.widget').find('.btn-remove').addClass('link-disabled');
	$( $target ).parents('.widget').addClass('widget-focus-enabled').addClass('widget-fullwindow');
	$('<div id="focus-overlay"></div>').hide().appendTo('body').fadeIn(300);

    //detect dataTable
    if( $("#dataTable").length ){
        $("#dataTable").DataTable().draw();
    }
});
$(document).on('click' ,'.btn-normalmode', function(e){
	e.preventDefault();
	$target = $(this).data('target');
	//強制側邊選單收合狀態
	$('.left-sidebar').removeClass('minified');
	$('.content-wrapper').removeClass('expanded');
	$('.left-sidebar .sub-menu')
	.css('display', 'none')
	.css('overflow', 'visible');
	$('.sidebar-minified').find('i.fa-angle-left').toggleClass('fa-angle-right');
	//強制上方工具列隱藏
	$('.top-bar').removeClass('hide');

	$(this).addClass('btn-fullmode').removeClass('btn-normalmode')
		   .find('.fa-minus-circle').addClass('fa-arrows-alt').removeClass('fa-minus-circle text-danger');

	$theWidget = $( $target ).parents('.widget');
	$($target).find('.fa-arrow-alt').toggleClass('fa-ban');
	$theWidget.find('.btn-remove').removeClass('link-disabled');
	$('body').find('#focus-overlay').fadeOut(function(){
		$(this).remove();
		$theWidget.removeClass('widget-fullwindow').removeClass('widget-focus-enabled');
	});

    //detect dataTable
    if( $("#dataTable").length ){
        $("#dataTable").DataTable().draw();
    }
});
