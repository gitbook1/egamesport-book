<?php
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 'On');

/**
 * 預設使用mbstring來判斷字元長度
 * @see https://github.com/phalcon/cphalcon/issues/971
 */
if( function_exists('mb_internal_encoding') )
    mb_internal_encoding("utf-8");

define('PPS_APP_PROJECTNAME', 'egameboard');
define('PPS_APP_APPSPATH', __DIR__ . '/../apps');

// 資料庫儲存UTC標準，必須扣除台北時區
define('TIMEZONE_DIFF', 8*3600);

/**
 * 讀取README.md抓取裡面最後一筆版本定義
 */
if( $fp=fopen(getcwd().'/../README.md','r') ){
    while ( ($line = fgets($fp)) !== false ) {
        if( preg_match("/^v\s([^\(]+)/",$line,$m) ){
            define('VERSION', $m[1]);
        break;
    }
}
if( !defined('VERSION') ){ define('VERSION','0.5.0'); }
}



require PPS_APP_APPSPATH . '/../config/services.php';
($DEBUG=new \Phalcon\Debug())->listen();


$application = new \Phalcon\Mvc\Application($di);
/**
 * Register application default modules
 */
$application->registerModules(array(
    'core' => array(
        'className' => ucfirst('egameboard').'\Core\Module',
        'path' => PPS_APP_APPSPATH . '/core/Bootstrap.php'
    ),
    'backend' => array(
        'className' => ucfirst('egameboard') . '\Backend\Module',
        'path' => PPS_APP_APPSPATH . '/backend/Bootstrap.php'
    )
));
echo $application->handle()->getContent();