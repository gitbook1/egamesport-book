<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class SportsTeam extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $teamId;

    /**
     * @Comment("關聯電競編號")
     *
     * @var integer
     */
    public $SportId;

    /**
     * @Comment("電競隊伍名稱")
     *
     * @var string
     */
    public $Name;

    /**
     * @Comment("電競隊伍圖標連結")
     *
     * @var string
     */
    public $IconUrl;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('SportId', '\Egameboard\Models\Sports', 'Id', array('alias' => 'Sports'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sports_team';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SportsTeam[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SportsTeam
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
