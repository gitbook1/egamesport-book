<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Category extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("類別名稱")
     *
     * @var string
     */
    public $label;

    /**
     * @Comment("識別碼")
     *
     * @var string
     */
    public $alias;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $useat;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('Id', '\Egameboard\Models\GuidesCategory', 'CategoryId', array('alias' => 'GuidesCategory'));
        $this->hasMany('Id', '\Egameboard\Models\NewsCategory', 'CategoryId', array('alias' => 'NewsCategory'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'category';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Category[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Category
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
