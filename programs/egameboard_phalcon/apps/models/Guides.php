<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Guides extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $title;


    /**
     * @Comment("")
     *
     * @var string
     */
    public $photo;


    /**
     * @Comment("")
     *
     * @var string
     */
    public $content;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $summary;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $initTime;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'guides';
    }


    public function afterFetch()
    {
        $this->initTimeFt = date('Y-m-d H:i:s', $this->initTime);

        $this->content = html_entity_decode( htmlspecialchars_decode($this->content) );
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Guides[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Guides
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
