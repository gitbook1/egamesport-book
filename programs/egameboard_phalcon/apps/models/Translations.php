<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Translations extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("語系")
     *
     * @var string
     */
    public $LanguageCode;

    /**
     * @Comment("識別碼")
     *
     * @var string
     */
    public $Label;

    /**
     * @Comment("顯示文字")
     *
     * @var string
     */
    public $Text;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'translations';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Translations[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Translations
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
