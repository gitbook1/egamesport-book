<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class News extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("新聞標題")
     *
     * @var string
     */
    public $title;

    /**
     * @Comment("內文")
     *
     * @var string
     */
    public $content;

    /**
     * @Comment("新聞照片")
     *
     * @var string
     */
    public $photo;

    /**
     * @Comment("摘要")
     *
     * @var string
     */
    public $summary;

    /**
     * @Comment("發布時間")
     *
     * @var integer
     */
    public $initTime;

    /**
     * @Comment("內容狀態")
     *
     * @var integer
     */
    public $statecode;

    /**
     * @Comment("")
     *
     * @var string
     */
    public $author;

    /**
     * @Comment("是否置頂")
     *
     * @var integer
     */
    public $isTop;


    /**
     * 附加欄位:分類標籤
     * */
    public $NewsCategories;


    const STAT_UNPUBLISH = 2;

    const STAT_PUBLISH = 1;

    const STAT_INIT = 0;

    public $stateLabels = array('草稿', '已發佈', '已下架');

    const statecodeLabels = [
        '草稿',
        '已發佈',
        '下架',
    ];

    public static function getStatecodeLabels() {
        return self::statecodeLabels;
    }


    public function getStatecodeLabel() {
        return $this->stateLabels[$this->statecode];
    }


    public function getinitTimeFt() {
        return date('Y-m-d H:i:s', $this->initTime);
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasManyToMany(
            'Id',
            '\Egameboard\Models\NewsCategory',
            'NewsId',
            'CategoryId',
            '\Egameboard\Models\Category',
            'Id',
            array('alias'=>'NewsCategory')
        );
        $this->hasMany('Id', '\Egameboard\Models\NewsSportsicon', 'NewsId', array('alias' => 'NewsSportsicon'));
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'news';
    }


    /**
     * 轉換特定欄位
     */
    public function afterFetch()
    {
        $this->pId = $this->Id;

        $this->initTimeFt = date('Y-m-d H:i:s', $this->initTime);

        $this->content = html_entity_decode( htmlspecialchars_decode($this->content) );

        $this->stateLabel = $this->stateLabels[$this->statecode];
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return News[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return News
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
