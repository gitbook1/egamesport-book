<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Egames extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("電競遊戲名稱")
     *
     * @var string
     */
    public $Name;

    /**
     * @Comment("電競遊戲圖示名稱")
     *
     * @var string
     */
    public $Icon;

    /**
     * @Comment("遊戲別名")
     *
     * @var string
     */
    public $alias;

    /**
     * @Comment("遊戲圖示路徑")
     *
     * @var string
     */
    public $Iconpath;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('Id', 'NewsEgameicon', 'EgameId', array('alias' => 'NewsEgameicon'));
        $this->hasMany('Id', 'ProviderEgameicon', 'EgameId', array('alias' => 'ProviderEgameicon'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'egames';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Egames[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Egames
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
