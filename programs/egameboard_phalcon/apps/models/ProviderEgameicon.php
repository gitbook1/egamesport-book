<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class ProviderEgameicon extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("關聯平台商編號")
     *
     * @var integer
     */
    public $ProviderId;

    /**
     * @Comment("關聯電競遊戲編號")
     *
     * @var integer
     */
    public $EgameId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('EgameId', '\Egameboard\Models\Egames', 'Id', array('alias' => 'Egames'));
        $this->belongsTo('ProviderId', '\Egameboard\Models\Providers', 'ProviderId', array('alias' => 'Providers'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'provider_egameicon';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProviderEgameicon[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProviderEgameicon
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
