<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Sports extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("電競遊戲名稱")
     *
     * @var string
     */
    public $Name;

    /**
     * @Comment("電競遊戲圖示名稱")
     *
     * @var string
     */
    public $Icon;

    /**
     * @Comment("排序值")
     *
     * @var integer
     */
    public $SortOrder;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('Id', '\Egameboard\Models\Boarddata', 'SportId', array('alias' => 'Boarddata'));
        $this->hasMany('Id', '\Egameboard\Models\SportsTeam', 'SportId', array('alias' => 'SportsTeam'));
        $this->hasMany('Id', '\Egameboard\Models\NewsSportsicon', 'SportId', array('alias' => 'NewsSportsicon'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sports';
    }


    /**
     * 轉換特定欄位
     */
    public function afterFetch()
    {
        $this->pId = $this->Id;
        $this->alias = ( empty($this->alias) ) ? '' : $this->alias;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Sports[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Sports
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
