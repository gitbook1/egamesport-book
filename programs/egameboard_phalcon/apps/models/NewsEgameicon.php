<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class NewsEgameicon extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("關聯新聞編號")
     *
     * @var integer
     */
    public $NewsId;

    /**
     * @Comment("關聯遊戲圖示編號")
     *
     * @var integer
     */
    public $EgameId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('EgameId', '\Egameboard\Models\Egames', 'Id', array('alias' => 'Egames'));
        $this->belongsTo('NewsId', '\Egameboard\Models\News', 'Id', array('alias' => 'News'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'news_egameicon';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return NewsEgameicon[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return NewsEgameicon
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
