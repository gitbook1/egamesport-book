<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class GuidesCategory extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("關聯遊戲指南編號")
     *
     * @var integer
     */
    public $GuidesId;

    /**
     * @Comment("關聯類別編號")
     *
     * @var integer
     */
    public $CategoryId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('CategoryId', '\Egameboard\Models\Category', 'Id', array('alias' => 'Category'));
        $this->belongsTo('GuidesId', '\Egameboard\Models\Guides', 'Id', array('alias' => 'Guides'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'guides_category';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GuidesCategory[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GuidesCategory
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
