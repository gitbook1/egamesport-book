<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Boarddata extends PersonalworkModel
{

    /**
     * @Comment("主鍵")
     *
     * @var integer
     */
    public $boarddataId;

    /**
     * @Comment("數據源主鍵")
     *
     * @var integer
     */
    public $MatchId;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $LinkMatchId;

    /**
     * @Comment("客隊")
     *
     * @var string
     */
    public $AwayTeam;

    /**
     * @Comment("客隊圖標連結")
     *
     * @var string
     */
    public $AwayTeamIconUrl;

    /**
     * @Comment("類型")
     *
     * @var string
     */
    public $CategoryName;

    /**
     * @Comment("主隊")
     *
     * @var string
     */
    public $HomeTeam;

    /**
     * @Comment("主隊圖標連結")
     *
     * @var string
     */
    public $HomeTeamIconUrl;

    /**
     * @Comment("關聯競賽平台編號")
     *
     * @var integer
     */
    public $TournamentId;

    /**
     * @Comment("競賽平台")
     *
     * @var string
     */
    public $TournamentName;

    /**
     * @Comment("關聯電競遊戲編號")
     *
     * @var integer
     */
    public $SportId;

    /**
     * @Comment("比賽資訊")
     *
     * @var string
     */
    public $MatchName;

    /**
     * @Comment("競賽時間(UTC標準)")
     *
     * @var string
     */
    public $StartTime;

    /**
     * @Comment("資料抓取時間")
     *
     * @var integer
     */
    public $dateTime;


    /**
     * 附加欄位：數據所屬遊戲分類名稱(Sports::Icon)
     * */
    public $SportName;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('boarddataId', '\Egameboard\Models\BoarddataOdds', 'BoarddataId', array('alias' => 'BoarddataOdds'));
        $this->belongsTo('SportId', '\Egameboard\Models\Sports', 'Id', array('alias' => 'Sports'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'boarddata';
    }


    public function afterFetch()
    {
        //預設使用UTC時間儲存！
        $this->StartTimeUTC = strtotime($this->StartTime);
        $this->StartTimeFt = date('M-d H:i', strtotime($this->StartTime)+TIMEZONE_DIFF );
        $this->StartTimeFt2 = date('H:i - m月 d, Y', strtotime($this->StartTime)+TIMEZONE_DIFF );
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Boarddata[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Boarddata
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
