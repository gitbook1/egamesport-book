<?php

namespace Egameboard\Models;

use Phalcon\Mvc\Model\Validator\Email as Email;use \Personalwork\Mvc\Model as PersonalworkModel;

class Contact extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $id;

    /**
     * @Comment("主旨")
     *
     * @var string
     */
    public $subject;

    /**
     * @Comment("訊息")
     *
     * @var string
     */
    public $message;

    /**
     * @Comment("聯絡郵件")
     *
     * @var string
     */
    public $email;

    /**
     * @Comment("聯絡姓名")
     *
     * @var string
     */
    public $name;

    /**
     * @Comment("聯繫時間")
     *
     * @var integer
     */
    public $initTime;

    /**
     * @Comment("是否已讀")
     *
     * @var integer
     */
    public $isRead;

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );

        if ($this->validationHasFailed() == true) {
            return false;
        }

        return true;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'contact';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contact[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contact
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
