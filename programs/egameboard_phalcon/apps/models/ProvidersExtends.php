<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class ProvidersExtends extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("關聯平台商編號")
     *
     * @var integer
     */
    public $ExProviderId;

    /**
     * @Comment("區塊名稱")
     *
     * @var string
     */
    public $block;

    /**
     * @Comment("內容")
     *
     * @var string
     */
    public $content;

    const LICENSE = 'licenses';
    const LICENSE_keys = [
        'curacao' => 'https://www.esports.net/wp-content/themes/esports-net/images/licenses/curacao.png',
    ];

    const PAYMENT = 'payments';
    const PAYMENT_keys = [
        'visa' => 'data:image/webp;base64,UklGRmQJAABXRUJQVlA4TFcJAAAvfUAMANfkIJIkRerZZ0b/Jl4b7jTYcFvbDpt82eScU89GjMpMVLS2QfrSh2TbbtsGAOXe26xvwLv32Ovp3U5oAv/Pf0QcCAAAKiikgqkolEGMXMJUIUHAIjI4Q2hszhSikFy5mgCG678zqZOSRdM4HJNESgIaGC30oCwhkRNICMECwtlNMihFR5i/MD0woVDMH4g1rSwOtsQs6IoBsUIkUhk//leoYM+oxE5dhX+GciQooAkJxQRmgDL+EylJCFTNgLCZMgMTgqgyKBhQVkAJwFAEBYEKP7aQoEIFSAxTMFJRFtAUzCT20TKXVcuylK0yqgI0jenNetREnPQkbFewv/0bk4djt/X9tj1fe7d9sX9k93MDtgeP5z9MnhpviylkTIQaSqBM5Uw6zIben+Q5X1tZy342tPGZGzZW7cheAysMLLq5w1YsZ27LuVEDr/cO/G/btjjNtn/7nNnnBCpIBbuw6kXdi1WgXkotOY7pmRmYhBCZQJVy6+XC5e7c/24S7mWZX+/7h4j+TwD+F5AkwOYg2crQZ6MlQPokCbIVSUMa0hjQmBb0DOF5JD0yNcYzJK21Fp7xW1lrGgxprfVMg6Fvm9E3mXaABEjSb2WspWkwRErtf+mzhckQLXcYHy3b0ILAVk/fwED/3l0A2kwLthMtmZJfftruz3+0+7YF+6YmJiYnJqYud5tdF6enpqampi/sZDN0HZm4/WDj6avHC/euHNu/owWIgbMTE5MTE1PnttLy/WfffPN1828++c3zmpHnXySNL+basO9tkiSVZO2GbwgSnSfny9VinI+iKC5U1/7diZbm1Nu1epLUy2tv98G3abA7/vjlh8++3vzyyy83N7/+4S/bDD3ziyIiSy+HiJFSJKKSnIfnGeLofLkg4rRRRGpzbDVUKkTSmF89CB9p9H3Lv379fvOrz7/88vOvPv2j1Xh9UVWkdnPL4GRdVTVcOQ6aDp5cLTrnNGii6pKLaH02kayqqqyvnQWZBna0+775/cNPNr/a3Pzys9/Q8lrFiYrUj1sfl5osvh4CDUdfvslKJEEQuijvVKV+stXOO8Vn0sRVrjEdgCXp3/7Xx99ufv7lNz8jQ+OTu9+LXc4FhQc98DlbDVRl6eE+WHberakTl3XV8tONJ6/KSVwaBgH6PkdLS+JUQlWJ73cykw4AxLm1f3z47ebm1z95Po01GHodBRK45BINdt4tiqoU7+6ybRhflcal2o3jA339Q8cm51/2AQ2GV+oizrmG6Ek/mBavncMvl15+8PkXX//wlzW0GVyu5pzLLr0doEH3g1hUg8p1GPJaVURVlie2iKZ7TnQRAHz0b8Q5cVEsqhqWj6SHPnbfK0TFD7794tM//IxP7nu0LoE8q86wg9i/sS6qUj9nDDvvF0RU1qWX7TQkfYIkYAwm6+LEyXuRU9XkTHoAcDrJxi8/3PzmV0vTgVN1UZX15VEYYDDbkK+Pw5juhVhENV86BRqf2KZv9j2MRSS+f2a5ySWYFOH4mpP4bx9/9bO1Pttu1kQ1Gy/sAYCjZSeq0at3LNk13+Byb16ftPD97RBnEhGRZKLvaaSqtZs+U8T+J1HWxf/87kdr2znweklUXXKVDWeThvjhPpD+jaqIqri4NDcI2m1g191YVKPS6I73YlWNH/SkCTvvFEXC5fd//Mtvw7nEiWp+9QBoielmd3eRFqfWQnFhoM5VnlzcBxiSBImDpayoFu93YramqvlXg0ixsZfqKuH63z/605rOewWnqsU7u0lL/1ZNQgmq13yPROetJAhUVUXWVxfOdCHT4NG/XglyQVi/AFyqqqqrn0wVRkv5MOsK//zNYnxFGlaOoc0SPQtxoKL1czA0PvfcTkIRpxqIi9fuD8F4pIfh11Eguvi4jzidiKpUp8EUcc9CIZLs4up/0HatrKJBYb6btLSDrxcDkbA+jgw9a9BzfTVWaYiy2dqjQZKZNkwnGoqsXKfxDpZF1RXv7ESKM+2z5ciF2eoM+56sB6Hk1i4hQ5PBu3VR1fybd0CS8LF15lGyJCJOcmEumTP0Orj/YSyhSH2cbex/vCQuXNzYnyaDs4mISnyPZ1adhLL0dgCGpgNnE9FA8xv70NwA/ddfVvNZcYFK/tU7NIbvJiIq8UIPDDvvxSKaWx5NEzH00qmG8cN9N2s5EanNtRuv3bRhuqIaSPxeVzMa20EM31iJnYQqklyEMZyuiqpULxEAZlbCQLR+FiZNnfdjVY02zjxZDFwuXzoKkgZtt2JVl4vv7mxB3zPk1umHca6hNkeybyNW1ezyudGxsQOjM+UgzAW1q0yTxVRdXM7JQqSqUry90xLwsffhoqg8q87R0JI0AOiROLosTlWLt9oMLyWRqqp7XW584wIVV3ivC76XGmK8LqoikTYmp+EBoDf4KmqoT8JkCBhLgr61md6NKGyYhel5EAcNIhJFTkRU1UWvBmHS1LcRiRN1TlXjh3sJAB7eXVFVcaunPZo9J3oA0POMIY6XnKpIcpk4leQCVdmuqqqsHIMxafENd9+Nc4E2r07AI2A9nK2raiDLY2jnwOp7Z/p3oXH3+MPFUFSkfMTuvF3UcFtR1CS5CC81ADlby4qq5gIXFUfQaIGZqogGS0/70YYj5UL9yd2ZSxcuTN5djUMVJ8X3ujCyHKm6IFurVquVSqVSLT8TVanc7ADTA5xPpEEDied7yCZbt4sN8Xtd8O3Zush6XE3qSRLnRdQ9XVw7B3s1EQ3VZW/Mzs7Ozc7Nzt6SvKrEC3vTNfT6TaiqoejqRZgGYO/DWFWDyhxJXKlpYxiqBoGqSrR2ZQuDr/Kimi3f8AHAWrBzviCq4fKITZPtmi84EZHs+tsB2iZm4PW6iLjqZRpu3Y41CFQlaFTVwurUbprLiROR9fpptGznbFWCQJMzTBPs1dqic27R1WaJluMrLnomz+qnQe59lBQiEXEizrml2ur8OMl9D+Mokmz8eD9bGJxPnIhUZtLF08+XG0vPj6I5ceF5qVQqlZ+Pgdj17rX5l0k9qVYqlfrK69unukHDsy/KpVJp+cV1297Cekefl0ql0tr93anq6Dk02ryzvZmXGTh8cGxs7MDBLkMS7B4+eXH62ty16YvHBncDxtrM0OGDB8fGxg730bQguw8eaBzdnUkTiZb0m6ENLekTHolt0xiSBq39bRiiKdtNmlJNz8P/RwQA',
            'Mastercard' => 'data:image/webp;base64,UklGRhQGAABXRUJQVlA4TAgGAAAvfUAMAKcEObZtVdF7uIwYEQRBE55be797LA23bSNJEnv83fvyj/LsbbmtbYVBMGdre2uXcP8d7KxyBv7/MP+RehQzHYVEMQqxFEX+QlQIkGQQYqkRoSALBpbEyR/7M+cBJ5Y84FIAhR5pJIKJkZImGUSL1kiUKE2JIu6vETc1m7E0IwaMpNFINfSTQQbMdhZ252J/1qba+gxMNe7BfV9g7/Y8PXv4KWBO/ncMDJnvNV99/bUEBjUasCkSndKe5mlJqHRTVekcwE8f43emaUmISBRRIZDCYEj/uSGwIlKFQiIVYAgFINY4wKAAdqBLhWqggkIsVYCoBGoKNDTA2MShCogQFTpeR+9dWth6BKf/wuHeMdIEREQo2ba1tW0+p8O+CM6YmdtwIv+yVMmMcszMLHL5jY3/sLP7RfR/Amhr25vGrpRhZuaZlUzWyFlJkzK34ykzMzMz9wJ62F4z7fGjbDqTI/o/AfQ/NJCrDhaO57v2pFdO75YstvrjFYv1uNfIx8Mn07IVse3ltJrYxij2phJZ/Bql2cmHS3bwQ+zuNeMbKmMp/irKQSE8kq0fYr9uOULg/BDFUmU3GxIlX+zfzlzrolj6rGWEQVPIVN33C2d5jL0ovaYqReezzVIB4zBGraEKqc8fPJovUAFjz6BVCAqZ+sU34NgRBxUwtkklfSFTvTg9Z9ZndyxUsCxSGgq5T97eWZ1LVMFmnE5eSFX107O1Z45LVMFNOjMp6sXhxuNH90oldpKKpUjRj18/2LhZoxJsUhkIqaq+5bWLm6QKXonSiHlS9NOnW89RiczRsIKCNSDErAHYJfT99PmLbXegcH2tvWWsacYWjb9BoXjADw4tPUDZYemxKra8espZNx10ehNcgs0oBjSGwpjHotwyf2SijB8GM4cEa4YJ5mL059039+/h/hveuGHvRnZbCWDmceBr7QrHY8dzhPb8SSI7c8Y0FsLsE1ALDwiwDHcymv7QsBwPhrPLDP05PPnyEZ9PThw6ePyQ/gGcKuQSltnvsu3PRlqMV31z5PQh7zomDUeYNYpTxbQoRnWPAGQcO0lA2osRRp0ogWcX70VeRT7cg9JsnWtPgJmT4wnoEk+qBmQ01H1h0fCElqciKjYlLQym1V4/S6Ui0v3eeE6pXp3ig3Yg9w349vMPEJ/6gnW/0mpnoNAFTtZLHK2mgDu+sGn4oR/JaJcKRdRzsLV5v5WjWp6yCyPcpX8n6XhtOk6kvhr3D598BmVfMLuzYbuZhUoDCu4QoqNgYsDYo+JqDmUmFAvEp7G4myekohhN44Tsskq/blC5QID1jmvr51moB4LXVvPAqRi0itAJapB1PcuI21pMadjGGHmbgMqCrDqyrpknO7zsRql1RVM/kJi4tuO/gK/XrzyHjAj8oAnpiS5CNw2joMw5P+hwRniuSWNstqnmHwCLASoFKodGkcTCufQSVMvQ7DJLJf/SmT+GA0f0d+B8p9/KQcEZJGKDKE+DJkR7gySU3ZE5odE2iuj9IGTNMnVbhAPAKlMESEwPCLVFnHCA5i3Yv425AsycbQIbwJkupIVnxuDnlm60hzSKarNoK5XiyGwVF6JmFUfl3qh0Vag7Wq9Unppuu1xszF+9PX9Nn1z0aiUeYT1b6JRawxK7whsVc+1mabzaHbVpJH9oRkgYrAkYITCYWWNmNjVhsHn+6PErZobWWzZv2YS+1tpzNHua3TWfBbPHrs96zRFcpIGxkKocftt87MgchUorTqQYlKKK881nT6CaNhCNOFL0s9e37qyfCxQqZYYKqooMVk9xtvbMcakEu0A2spDC+vHh0eqJ7Quh0k7TgaXIOXz9gFlvxoWjApvJBB00pbAudOZrp/ZKoRD7mUY1SgcjKax/ffJ0x2YpFKKZTFWLBqHoRM7pi+cLLVRaWeCYAZRjExms88uFUIhmBuhH+kEJP0rXV3B5OExDKFrT4J7QTxPidVsuB82KASEZqDr7CM7ytDbdneFfQ7uVgBCNVWbKDj9GV7Rltm3hH6FcaaYgbHP1sbtcbw8radoxUWqPLfuX1rCVj8L/oA==',
            'Bitcoin' => 'data:image/webp;base64,UklGRoYGAABXRUJQVlA4THkGAAAvfUAMAGfjKpIkKyJmH4Eh3CAVV/yx3dVwZ8Jh2zaSZC+2/07eK+8Syw4jyW0byTYA59pctUtK4P9h/uuQznRsBtkA0s2mAUgHAAAAOOaYYwBAOjSimCP/YM0C0IpjaxbHAADHtJIOAGCNOLJmcQzgAEwrjjnmmFa0ohXHAFp5/i0d6QCkA+CYVv7seTXxJ3+5DTVXYqGLJbG5Tfq4+/d0c5wvU0/dqRS6L/bzwLlqOmktzamauktTKqWm1F1KqTk0ld6rcZTWi6ZQCs2pFK7fG7xr2/Ym2bZta0xOOYJ0FUTaiV3hQJGcEFsgrPZ6nPbeu+f4D4DZ9qD+vq5piug/AwFI4sDuw4FNtzfg/3LY9f50e3V99/zRQY1uRFJpDT7UzPgvDT9KPRTXf0Te/dHK2ieXT5/81RRKNbviqyPxu1EK40eZrtbHjO+Le9jf2JbcWj988ZETtkmOwYfRBskZ/CQzS6Q7+l1tH+dya0ceWLuFwpRDun3w4Vj7E7YOwEj0BvATrJJk5bvz2ZaMt7m1u+s9YP1GdemSiyZ8OEmS0wgMWbZbMfETtEi6w9+MfyWu2NnbeThb2d8Smw8QnCFZ85WYmnPdRhxxh2QRP8Ihx21Vvzk4T57R3Y3j/f33o9tnmf79dwAwGySH/DVG/s6ZQIpkM42fYV8xrX/z2JFnc2/1ER+4eb2Rh7YuASC1RLYHwpGwqUQNBAzDgF74RL3vl2kGRNUboUgsFunRoYqIhhSnuhkwuoWG7p5wJBoJGZ3xsiUjrN/j463r7WJ912c17JLudGOhMdUvpIfHy6VSCmblE/PlqdLvjA4YfaPWUqs5P50zAcSHraWWYw1FhKf+yVJpKgItnBmvNhadBWsk1hHXMsLu3g1uV05fcb+1o5zWpqmymdUAZL07BxCyqTCvITTWUkTUwjCy84oIKw4AE96VIQQrPgYa8U4Q+97uM47+/HupYWXX270CgnX6cKlXuXO+G4k2Ffaixy8zeaBIH06bgFaTbSnmKHq+0qX2FRy84+7o4BEvG2L3FOifI+lUKyLcuNI1qtDyJN2G9U+9EsIUSbaqpXK9FkO6RdKp/PYub6UhB2cISJN0a6WqS7LZCSsKzvD20gW87It5+0TDkHcmE0Sf98RqEGHvASMIeGFnE7F4IoSBNsl6JggkEnrQImmndWSaJIeBAa9aemGMkbRiMEtt0vnaYm/vFrerB/ddryqAsucIAEblC5RxSCZhWiSVrj1LciGhOMi7JNMATE9OyRAt2TERnCFZBpBtfgEHorWvHj/j8M+frVc87ohGf4G/PO1lA0plJzDSJpd6EHPJVg4AEPeSOqEYMDxzs7qHMslKAEKIhsiiHAIUWl/YufCirD0CuFhZP3zHo2zyd+i3Sea0TxS9RRS/SU4bWoak3QcoNtwBxUC8IasPsramNPGGDMum6KQBlLwbO7ZYb7F5cPGMrteXD+BQYPkF/7XIdhzArzLJGSMkRlo3prxMynY12SabEcXAwALJFACELfGWp5dEY9D/lkOGgOU9qRM+9sXh7ce764+Xl+fTbbF/pmGcpBUEEG2J73tyXpjotkhOAVCS6IQVA4MOSTEWfaJMMOSKnAQqImlA3P7SZ+letpmX05uX1c0NeX7lGb+qnrsAYJRILg4g7wqLwXmyPQIAEFHdpA+UhShGKwLP7WwAoQXl5yTXFGnpuDr2Dm2trl+e7+zKeeMa6G2QzGowxtskJ3WR4JqJsKN0HWgouiRLQoMux2RMh15skxzTeiyS4zp6STqDSsedMzsCbwfy0+6dE9w66wJyLsmxwWGrLTMYqnkmuhHy4EwNTVSTSCx6q+roSKmahngf3amRaUWoqJaMJppiPQJoVVG7nYHXg61tX26cAtDG6Uc7BQwsyi4S+IcKs0CZCt0cMOzT+bkwCHhhWxEYFaU6YnWSef1rv3RrPhubK7cA0F3xsdCaTQAoUJaenlM/Z0NAsKY6LXhSFRHtRhrQSl5YE6ZNcgLKIPbja3w5W1lb39hYX9u/egcA6Nna3JLTbDoLlszTyJxtz/QA0Ar2kuM4C42MBpjjtiduqZ4GEJAi7MkwgO6ybdtFAxHbtuuDAApztm31fBHoer65OD27vHvtAuTUk8qPjk+MZCJi1mKp/mRMSDB686NjQ5moFJEsjk+M5RJSRGpofLyYNISGRLI/GdJgJpPJviCASDKZTOj4H3sCAA==',
            'Skinpay' => 'https://www.esports.net/wp-content/themes/esports-net/images/payment/skinpay.png',
    ];

    const REGIONS = 'regions';
    const REGIONS_keys = [
        'eu' => 'https://www.esports.net/wp-content/themes/esports-net/images/flags/eu.svg',
        'jp' => 'https://www.esports.net/wp-content/themes/esports-net/images/flags/japan.svg',
        'ru' => 'https://www.esports.net/wp-content/themes/esports-net/images/flags/russia.svg',
        'cn' => 'https://www.esports.net/wp-content/themes/esports-net/images/flags/china.svg',
    ];


    const BLOCK_1 = '網站設計';

    const BLOCK_2 = '優惠紅利';

    const BLOCK_3 = '支付方式';

    const BLOCK_4 = '存提時間';

    const BLOCK_5 = '客戶服務';

    const BLOCK_6 = '監管牌照';

    const BLOCK_7 = '用戶體驗';

    const BLOCK_Keys = [
        'BLOCK_1',
        'BLOCK_2',
        'BLOCK_3',
        'BLOCK_4',
        'BLOCK_5',
        'BLOCK_6',
        'BLOCK_7',
    ];

    const BlockLabels = array(
        'BLOCK_1' => '網站設計',
        'BLOCK_2' => '優惠紅利',
        'BLOCK_3' => '支付方式',
        'BLOCK_4' => '存提時間',
        'BLOCK_5' => '客戶服務',
        'BLOCK_6' => '監管牌照',
        'BLOCK_7' => '用戶體驗',
    );

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ExProviderId', '\Egameboard\Models\Providers', 'ProviderId', array('alias' => 'Providers'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'providers_extends';
    }


    public static function getBlockLabels($key=null) {
        if( !empty($key) ){
            return self::BlockLabels[$key];
        }else{
            return self::BlockLabels;
        }
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProvidersExtends[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ProvidersExtends
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
