<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class BoarddataOdds extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $boId;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("關聯看板數據編號")
     *
     * @var integer
     */
    public $BoarddataId;

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $OddTypeId;

    /**
     * @Comment("關聯平台商編號")
     *
     * @var integer
     */
    public $ProviderId;

    /**
     * @Comment("數據")
     *
     * @var double
     */
    public $Value;

    /**
     * @Comment("是否標註置頂")
     *
     * @var integer
     */
    public $isBest;

    /**
     * @Comment("排序")
     *
     * @var integer
     */
    public $sort;

    /**
     * @Comment("最佳排序")
     *
     * @var integer
     */
    public $sort_best;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('BoarddataId', '\Egameboard\Models\Boarddata', 'boarddataId', array('alias' => 'Boarddata'));
        $this->belongsTo('ProviderId', '\Egameboard\Models\Providers', 'ProviderId', array('alias' => 'Providers'));
    }


    /**
     * Convert a decimal (e.g. 3.5) to a fraction (e.g. 7/2).
     * Adapted from: http://jonisalonen.com/2012/converting-decimal-numbers-to-ratios/
     *
     * @param float $decimal the decimal number.
     *
     * @return array|bool a 1/2 would be [1, 2] array (this can be imploded with '/' to form a string)
     */
    protected function decimalToFraction($decimal)
    {
      if ($decimal < 0 || !is_numeric($decimal)) {
          // Negative digits need to be passed in as positive numbers
          // and prefixed as negative once the response is imploded.
          return false;
      }
      if ($decimal == 0) {
          return [0, 0];
      }

      $tolerance = 1.e-4;

      $numerator = 1;
      $h2 = 0;
      $denominator = 0;
      $k2 = 1;
      $b = 1 / $decimal;
      do {
          $b = 1 / $b;
          $a = floor($b);
          $aux = $numerator;
          $numerator = $a * $numerator + $h2;
          $h2 = $aux;
          $aux = $denominator;
          $denominator = $a * $denominator + $k2;
          $k2 = $aux;
          $b = $b - $a;
      } while (abs($decimal - $numerator / $denominator) > $decimal * $tolerance);

      return [
              $numerator,
              $denominator
          ];
    }


    public function afterFetch()
    {
        $fraction = $this->decimalToFraction($this->Value);
        $this->ValueFractions = "{$fraction[0]} / {$fraction[1]}";
        $this->ValueAmerican = ($this->Value-1)*100;
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'boarddata_odds';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BoarddataOdds[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BoarddataOdds
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
