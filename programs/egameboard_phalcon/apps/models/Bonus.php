<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Bonus extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("關聯平台商編號")
     *
     * @var integer
     */
    public $ProviderId;

    /**
     * @Comment("設定名次")
     *
     * @var integer
     */
    public $level;

    /**
     * @Comment("優惠簡介")
     *
     * @var string
     */
    public $intro;

    /**
     * @Comment("評分值")
     *
     * @var integer
     */
    public $rank;

    /**
     * @Comment("按鈕說明文字")
     *
     * @var string
     */
    public $infomation;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('ProviderId', '\Egameboard\Models\Providers', 'ProviderId', array('alias' => 'Providers'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'bonus';
    }

    /**
     * 轉換特定欄位
     */
    public function afterFetch()
    {
        $this->intro = html_entity_decode(htmlspecialchars_decode($this->intro));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Bonus[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Bonus
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
