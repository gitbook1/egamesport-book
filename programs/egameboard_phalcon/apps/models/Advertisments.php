<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Advertisments extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $Id;

    /**
     * @Comment("廣告區塊位置")
     *
     * @var string
     */
    public $position;

    /**
     * @Comment("廣告圖片")
     *
     * @var string
     */
    public $photo;

    /**
     * @Comment("介紹")
     *
     * @var string
     */
    public $intro;

    /**
     * @Comment("廣告連結")
     *
     * @var string
     */
    public $link;


    const POS_INDEX_TOP = 0;

    const POS_INDEX_BTMLEFT = 1;

    const POS_INDEX_BTMMIDDLE = 2;

    const POS_INDEX_BTMRIGHT = 3;

    const POS_RIGHT_BOX = 4;

    public $positionLabels = array('首頁置頂', '首頁底部左側', '首頁底部中間', '首頁底部右側', '左側廣告區塊');

    const positionLabels = [
        '首頁置頂',
        '首頁底部左側',
        '首頁底部中間',
        '首頁底部右側',
        '左側廣告區塊',
    ];

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'advertisments';
    }

    public static function getPositionLabels()
    {
        return self::positionLabels;
    }


    public function getPositionLabel()
    {
        return $this->PositionLabels[$this->position];
    }


    /**
     * 轉換特定欄位
     */
    public function afterFetch()
    {
        $this->pId = $this->Id;

        $this->positionLabel = $this->positionLabels[$this->position];
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Advertisments[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Advertisments
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
