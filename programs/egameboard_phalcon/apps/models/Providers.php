<?php

namespace Egameboard\Models;

use \Personalwork\Mvc\Model as PersonalworkModel;

class Providers extends PersonalworkModel
{

    /**
     * @Comment("")
     *
     * @var integer
     */
    public $ProviderId;

    /**
     * @Comment("平台商名稱")
     *
     * @var string
     */
    public $Name;

    /**
     * @Comment("排序值")
     *
     * @var integer
     */
    public $SortOrder;

    /**
     * @Comment("是否顯示")
     *
     * @var integer
     */
    public $IsVisible;

    /**
     * @Comment("是否啟用")
     *
     * @var integer
     */
    public $IsEnabled;

    /**
     * @Comment("平台商資訊連結")
     *
     * @var string
     */
    public $Link;

    /**
     * @Comment("平台商圖標")
     *
     * @var string
     */
    public $Icon;

    /**
     * @Comment("簡介")
     *
     * @var string
     */
    public $description;

    /**
     * @Comment("評分值")
     *
     * @var integer
     */
    public $rank;

    /**
     * @Comment("設定圖片")
     *
     * @var string
     */
    public $localIcon;


    /**
     * 附加欄位
     *
     * @var string
     */
    public $lisenses;

    /**
     * 附加欄位
     *
     * @var string
     */
    public $payments;

    /**
     * 附加欄位
     *
     * @var string
     */
    public $regions;

    /**
     * 附加欄位
     *
     * @var string
     */
    public $blocks;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('ProviderId', '\Egameboard\Models\BoarddataOdds', 'ProviderId', array('alias' => 'BoarddataOdds'));
        $this->hasOne('ProviderId', '\Egameboard\Models\Bonus', 'ProviderId', array('alias' => 'Bonus'));
        $this->hasMany('ProviderId', '\Egameboard\Models\ProvidersExtends', 'ExProviderId', array('alias' => 'ProvidersExtends'));
        $this->hasMany('ProviderId', '\Egameboard\Models\ProviderSportsicon', 'ProviderId', array('alias' => 'ProviderSportsicon'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'providers';
    }


    /**
     * 轉換特定欄位
     */
    public function afterFetch()
    {
        $this->pId = $this->ProviderId;

        $this->description = html_entity_decode(htmlspecialchars_decode($this->description));;

        if (is_file(PPS_APP_APPSPATH . '/../public/' . $this->Icon)) {
            $this->displayIcon =  $this->Icon;
        } elseif (is_file(PPS_APP_APPSPATH . '/../public/' . $this->localIcon)) {
            $this->displayIcon =  $this->localIcon;
        } else {
            $this->displayIcon =  'https://via.placeholder.com/150x80';
        }

        if( count($this->ProvidersExtends) ){
            $self = $this;
            $keys = \Egameboard\Models\ProvidersExtends::BLOCK_Keys;
            $this->ProvidersExtends->filter(function($row) use (&$self, $keys) {
                foreach($keys as $key){
                    if($row->block== $key){
                        $self->blocks[] = [
                            'id' => $row->block,
                            'label' => \Egameboard\Models\ProvidersExtends::getBlockLabels($row->block),
                            'content' => html_entity_decode(htmlspecialchars_decode($row->content))
                        ];
                    }
                }
                if (in_array($row->block, ['lisenses', 'payments', 'regions'])) {
                    $self->{$row->block} = explode(',',$row->content);
                }
            });
        }
    }


    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Providers[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Providers
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
