<?php

namespace Egameboard\Core\Controllers;

class DefaultController extends \Personalwork\Mvc\Controller\Base\Application
{
  /**
   * 首頁
   *
   * 遊戲選單配置規則：
   *  1. 使用Icon縮寫作為關鍵字
   *  2. 判斷Name字數少的顯示
   */
  public function indexAction()
  {
    $this->view->ODDS = [
      'Decimal' => '小数',
      'Fractions' => '分数',
      'American' => '美式'
    ];

    $this->view->DEFAULT_ODD = '小数';

    $this->view->TIMEZONES = [
      '-12' => 'UTC-12:00',
      '-11' => 'UTC-11:00',
      '-10' => 'UTC-10:00',
      '-9' => 'UTC-09:00',
      '-8' => 'UTC-08:00',
      '-7' => 'UTC-07:00',
      '-6' => 'UTC-06:00',
      '-5' => 'UTC-05:00',
      '-4' => 'UTC-04:00',
      '-3' => 'UTC-03:00',
      '-2' => 'UTC-02:00',
      '-1' => 'UTC-01:00',
      '0' => 'UTC',
      '1' => 'UTC+01:00',
      '2' => 'UTC+02:00',
      '3' => 'UTC+03:00',
      '4' => 'UTC+04:00',
      '5' => 'UTC+05:00',
      '6' => 'UTC+06:00',
      '7' => 'UTC+07:00',
      '8' => 'UTC+08:00',
      '9' => 'UTC+09:00',
      '10' => 'UTC+10:00',
      '11' => 'UTC+11:00',
      '12' => 'UTC+12:00',
    ];

    $this->view->DEFAULT_TIMEZONE = 'UTC+08:00';

    $tableRightProviders = [
      23=>0, 26=>1, 19=>2, 14=>3, 29=>4, 24=>5, 25=>6, 32=>7, 18=>8, 27=>9, 15=>10, 30=>11, 17=>12, 35=>13
    ];

    $sports = [];
    $tabledatas = [];
    $counter=0;
    $boarddatas = \Egameboard\Models\Boarddata::find([
                    "StartTime > :nowtime:",
                    'bind' => [ 'nowtime' => date('Y-m-d\TH:i:00', time() ) ],
                    'order' => "StartTime ASC"
                  ])->filter(function($row) use (&$sports, &$tabledatas, &$counter, $tableRightProviders){

                    // 處理數據列表顯示電競遊戲名稱
                    if( empty($row->Sports->Icon) ){
                      $row->Sports->Icon = strtolower( preg_replace('/\s+/', '', $row->Sports->Name) );
                    }
                    $iconpath = 'misc/img/moduodds/egame/'.$row->Sports->Icon.'.svg';

                    if( array_key_exists($row->Sports->Icon, $sports) ){
                      if( strlen($sports[$row->Sports->Icon]['name']) > strlen($row->Sports->Name) ){
                        $sports[$row->Sports->Icon]['name']=$row->Sports->Name;
                      }
                    }else{

                      if( is_file( PPS_APP_APPSPATH.'/../public/'.$iconpath) ){
                        $sports[$row->Sports->Icon]=[
                          'name'=> $row->Sports->Name,
                          'icon'=> $iconpath
                        ];
                      }else{
                        $sports[$row->Sports->Icon]=[
                          'name'=> $row->Sports->Name,
                          'icon'=> null
                        ];
                      }
                    }
                    // 配置數據所屬電競分類
                    $row->SportName = $row->Sports->Icon;

                    // 處理單純數據顯示表格
                    $static_iconpath = '/misc/img/moduodds/sportsicon/'. $row->Sports->Icon.'.png';

                    $tabledatas[$row->boarddataId]['SportName'] = $row->Sports->Icon;
                    $tabledatas[$row->boarddataId]['left'] = [
                      //icon
                      $static_iconpath ,
                      $row->StartTimeUTC,
                      // date
                      date('M-d', $row->StartTimeUTC),
                      // time
                      date('H:i', $row->StartTimeUTC),
                      // Match
                      $row->MatchName,
                      $row->TournamentName,
                    ];

                    $tableRight = array_fill(0, 13, [['-', '-', '-', 0], ['-', '-', '-', 0]] );
                    foreach($row->BoarddataOdds as $odds){
                      // tableRight rule: sort%2=0(HomeTeam) /
                      if( array_key_exists($odds->Providers->ProviderId, $tableRightProviders) ) {
                        $tableRight[$tableRightProviders[$odds->Providers->ProviderId]][$odds->sort%2] = [$odds->Value, $odds->ValueAmerican, $odds->ValueFractions, $odds->isBest];
                      }

                      if( $odds->isBest ){
                        if( $odds->sort_best == 0){
                          $row->bestLeftName = $row->HomeTeam;
                          $row->bestLeftLink = $odds->Providers->Link;
                          $row->bestLeftIcon = $odds->Providers->displayIcon;
                          $row->bestLeftValue = $odds->Value;
                          $row->bestLeftValueAmerican = $odds->ValueAmerican;
                          $row->bestLeftValueFractions = $odds->ValueFractions;
                        }elseif( $odds->sort_best == 1 ){
                          $row->bestRightName = $row->AwayTeam;
                          $row->bestRightLink = $odds->Providers->Link;
                          $row->bestRightIcon = $odds->Providers->displayIcon;
                          $row->bestRightValue = $odds->Value;
                          $row->bestRightValueAmerican = $odds->ValueAmerican;
                          $row->bestRightValueFractions = $odds->ValueFractions;
                        }
                      }
                    }
                    // $row->tableRight = $tableRight;
                    $tabledatas[$row->boarddataId]['right']= $tableRight;
                    $tabledatas[$row->boarddataId]['index']= $counter;
                    $counter++;
                    // dd($static_iconpath, $tableRight);
                    return $row;
                  });
    // dd( $boarddatas[0]->tableRight );
    $this->view->sports = $sports;
    // dd($this->view->sports);
    $this->view->boarddatas = $boarddatas;
    $this->view->tabledatas = $tabledatas;

    // 廣告區塊內容
    $ads = [];
    $ads['carousel'] = \Egameboard\Models\Advertisments::find(["position = ".\Egameboard\Models\Advertisments::POS_INDEX_TOP]);
    $ads['boxleft'] = \Egameboard\Models\Advertisments::findFirst(["position = ".\Egameboard\Models\Advertisments::POS_INDEX_BTMLEFT]);
    $ads['boxmiddle'] = \Egameboard\Models\Advertisments::findFirst(["position = ".\Egameboard\Models\Advertisments::POS_INDEX_BTMMIDDLE]);
    $ads['boxright'] = \Egameboard\Models\Advertisments::findFirst(["position = ".\Egameboard\Models\Advertisments::POS_INDEX_BTMRIGHT]);
    // dd($ads['boxright']);

    $this->view->advertisments = $ads;

    return $this->view->pick('default/index');
  }


  /**
   * 公司推荐
   */
  public function bettingAction()
  {

    $this->view->egames = \Egameboard\Models\Egames::find(["Iconpath IS NOT NULL"]);

    $this->view->providers = \Egameboard\Models\Providers::find(["order"=>"rank DESC"]);

    $this->view->rightad = \Egameboard\Models\Advertisments::findFirst(["position = ".\Egameboard\Models\Advertisments::POS_RIGHT_BOX]);;

    $this->view->rightnews = array_fill(0, 3, 0);

    $this->view->rightguides = array_fill(0, 3, 0);

    return $this->view->pick('default/betting');
  }


  /**
   * 公司内页
   */
  public function bettingDetailAction()
  {
    $this->view->egames = \Egameboard\Models\Egames::find(["Iconpath IS NOT NULL"]);

    $providerId = $this->dispatcher->getParam('id', 'int');

    $this->view->providerblocks = \Egameboard\Models\ProvidersExtends::getBlockLabels();

    $this->view->provider = \Egameboard\Models\Providers::findFirst($providerId);
    // dd($this->view->provider);

    return $this->view->pick('default/betting-detail');
  }


  /**
   * 电竞新闻
   */
  public function newsAction()
  {
    // $this->view->newsitems = \Egameboard\Models\News::find(["order"=>"initTime DESC"]);
    $this->view->newsitems = array_fill(0,10, 1);

    $this->view->pick('default/news');
  }


  /**
   * 新闻内页
   */
  public function newsDetailAction()
  {
    $this->view->pick('default/news-detail');
  }

  /**
   * 最佳优惠
   */
  public function bonusAction()
  {
    $this->view->bonusitems = \Egameboard\Models\Bonus::find(["order" => "level ASC"]);

    $this->view->providers = \Egameboard\Models\Providers::find(["order" => "rank DESC"]);

    return $this->view->pick('default/bonus');
  }


  // /**
  //  * 优惠内页
  //  */
  // public function bonusDetailAction()
  // {
  //   return $this->view->pick('default/bonus-detail');
  // }


  /**
   * 电竞指南
   */
  public function guidesAction()
  {
    $this->view->newsitems = array_fill(0,10, 1);

    return $this->view->pick('default/guides');
  }


  /**
   * 电竞指南内页
   */
  public function guidesDetailAction()
  {
    return $this->view->pick('default/guides-detail');
  }


  /**
   * 关于我们
   */
  public function aboutusAction()
  {
    return $this->view->pick('default/aboutus');
  }


  /**
   * 联系我们
   */
  public function contactusAction()
  {
    return $this->view->pick('default/contactus');
  }


  /**
   * 免责声明
   */
  public function statementsAction()
  {
    return $this->view->pick('default/statements');
  }


  public function notfound404Action()
  { }
}

