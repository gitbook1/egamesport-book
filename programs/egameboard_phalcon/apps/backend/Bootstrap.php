<?php

namespace Egameboard\Backend;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        if( $di->has('autoloader') ){
            $loader = $di->get('autoloader');
        }else{
            $loader = new Loader();
        }

        $namespace = $loader->getNamespaces();
        //module's controller / form /
        $loader->registerNamespaces(array_merge($namespace, array(
            ucfirst(PPS_APP_PROJECTNAME) . '\Backend\Controllers'    => __DIR__ .'/controllers/',
        )));

        $loader->register();
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        $view=$di->get('view');
        $view->setLayout('page-backend');
        $view->setVar('breadcrumb', []);
        $di->setShared('view', $view);

        if( $di->has('dispatcher') ){
            $dispatcher = $di->get('dispatcher');
        }else{
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
        }
        $dispatcher->setDefaultNamespace(ucfirst(PPS_APP_PROJECTNAME) . '\Backend\Controllers');
    }
}
