<?php
/*
  电竞新闻管理機制
 */
namespace Egameboard\Backend\Controllers;

class EgameController extends \Personalwork\Mvc\Controller\Base\Application
{

  /**
   * 爬蟲程式設定介面
   *
   * @return void
   */
  public function configAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $this->view->pick('egame/config');
  }

  /**
   * 呈現爬蟲原始數據列表
   *
   * @return void
   */
  public function datalistAction()
  {
    if( !$this->session->has('AUTHENTICATION') ){
        return $this->response->redirect('/backend/login');
    }

    $this->view->pick('egame/datalist');
  }
}

