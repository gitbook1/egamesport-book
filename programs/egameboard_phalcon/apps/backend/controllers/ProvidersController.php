<?php
/*
  公司推荐管理機制
 */
namespace Egameboard\Backend\Controllers;

class ProvidersController extends \Personalwork\Mvc\Controller\Base\Application
{
  protected $breadcrumb;

  public function initialize(){
    $this->breadcrumb[] = [
        'icon' => 'fa fa-dashboard',
        'url' => $this->url->get('/backend'),
        'label' => '後台首頁',
    ];

    parent::initialize();
  }


  public function indexAction()
  {
    if( !$this->session->has('AUTHENTICATION') ){
        return $this->response->redirect('/backend/login');
    }

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle',
        'url' => $this->url->get('/backend/providers'),
        'label' => '公司推薦管理列表',
    ];


    $providers = \Egameboard\Models\Providers::find(['order'=> 'SortOrder ASC'])->filter(function($row){
      $data = $row->toArray();
      $data['pId']=$row->ProviderId;
      return $data;
    });

    $this->view->setVars([
      'ID' => 'providersTable',
      'theads' => [
        [
          'label'=> '平台商資訊',
          'datakey' => 'Name',
          'width'=> '50%',
          'classsets'=> null,
        ],
        [
          'label'=> '評分值',
          'datakey' => 'rank',
          'width'=> '10%',
          'classsets'=> null,
        ],
        [
          'label'=> '是否顯示',
          'datakey' => 'IsVisible',
          'width'=> '10%',
          'classsets'=> null,
        ],
        [
          'label'=> '是否啟用',
          'datakey' => 'IsEnabled',
          'width'=> '10%',
          'classsets'=> null,
        ],
        [
          'label'=> '操作',
          'datakey' => 'optfunc',
          'width'=> '20%',
          'classsets'=> null,
        ]
      ],
      'tabledatas' => $providers

    ]);
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/providers');
  }


  public function formAction()
  {
    $this->assets
      ->collection('headerStyle')
      ->addCss('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css', true);
    $this->assets
      ->collection('headerScript')
      ->addJs('misc/js/bootstrap-slider/bootstrap-slider.js')
      ->addJs('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js', true);

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle-o',
        'url' => $this->url->get('/backend/providers'),
        'label' => '公司推薦管理列表',
    ];

    $this->view->LICENSES = \Egameboard\Models\ProvidersExtends::LICENSE_keys;
    $this->view->PAYMENTS = \Egameboard\Models\ProvidersExtends::PAYMENT_keys;
    $this->view->REGIONS = \Egameboard\Models\ProvidersExtends::REGIONS_keys;

    // 若有傳遞編號，取得資料顯示於表單內欄位
    if( !empty($primaryId = $this->dispatcher->getParam('pId')) ){
      $this->view->formData = \Egameboard\Models\Providers::findFirst($primaryId);
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/providers'),
        'label' => '編輯[ '.$this->view->formData->Name.' ]公司資訊',
      ];
    }else{
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/providers'),
        'label' => '新增公司資訊',
      ];
    }
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/providers-form');
  }


  public function saveAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('ProviderId', 'int');
    if( intval($id) ){
      $provider = \Egameboard\Models\Providers::findFirst($id);
    }else{
      $provider = new \Egameboard\Models\Providers;
      $maximum = \Egameboard\Models\Providers::maximum(['column'=>'SortOrder']);
      $provider->SortOrder = $maximum+1;
    }

    $provider->Name = $this->request->getPost('Name', 'string');
    $provider->IsVisible = $this->request->getPost('IsVisible', 'int');
    $provider->IsEnabled = $this->request->getPost('IsEnabled', 'int');
    $provider->Link = $this->request->getPost('Link', 'string');
    $provider->rank = $this->request->getPost('rank', 'int');

    $string = str_replace(array("\r", "\n", "\r\n", "\n\r"), '', $_POST['description']);
    $string = preg_replace('/\s\s+/', ' ', $string);
    $provider->description = htmlentities(htmlspecialchars($string));

    $ProvidersExtends=[];
    foreach($_POST as $key => $content ){
      if( preg_match('/BLOCK_(\d+)/ms', $key, $m) ){
        if( !empty($provider->ProviderId) ){
          $exProvider = \Egameboard\Models\ProvidersExtends::findFirst(["block='{$key}'"]);
          if( !$exProvider ){
            $exProvider = new \Egameboard\Models\ProvidersExtends();
            $exProvider->block = $key;
          }
        }else{
          $exProvider = new \Egameboard\Models\ProvidersExtends();
          $exProvider->block = $key;
        }

        $string = str_replace(array("\r", "\n", "\r\n", "\n\r"), '', $content);
        $string = preg_replace('/\s\s+/', ' ', $string);
        $exProvider->content = htmlentities(htmlspecialchars($string));
        $ProvidersExtends[] = $exProvider;
      }

      if( in_array($key, ['lisenses', 'payments', 'regions'] ) ){
        $exProvider = \Egameboard\Models\ProvidersExtends::findFirst(["block='{$key}'"]);
        if ($exProvider) {
          $exProvider->delete();
        }
        $exProvider = new \Egameboard\Models\ProvidersExtends();
        $exProvider->block = $key;
        $exProvider->content = implode(',', $content);
        $ProvidersExtends[] = $exProvider;
      }
    }

    $provider->ProvidersExtends = $ProvidersExtends;

    if( !empty($_FILES['localIcon']) && $_FILES['localIcon']['error'] == 0 ){
      $tmp = explode('.', $_FILES['localIcon']['name']);
      $filename = uniqid().'.'.$tmp[1];
      $abs_path = realpath(PPS_APP_APPSPATH.'/../public/uploads/');
      $relat_path = '/uploads/'.$filename;

      if( !is_writeable($abs_path) ){
        dd('can\'t write');
      }
      if( move_uploaded_file($_FILES["localIcon"]["tmp_name"], $abs_path.'/'.$filename) ){
        $provider->localIcon = $relat_path;
      }else{
        dd('save file error!');
        $this->flashSession->warning('上傳圖示發生錯誤，'.$_FILES["localIcon"]["error"]);
      }
    }

    if( !($provider->save()) ){
      $this->flashSession->warning('儲存數據發生錯誤，'.implode(',', $provider->getMessages()));
    }else{
      $this->flashSession->success('已完成數據儲存');
    }

    return $this->response->redirect('/backend/providers/form/'.$provider->ProviderId);
  }


  public function deleteAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('id', 'int');
    $provider = \Egameboard\Models\Providers::findFirst($id);
    if( !$provider->delete() ){
      $response = [
        'code' => 500,
        'codeType' => 'ERR',
        'msg' => '刪除數據發生錯誤，' . implode(',', $provider->getMessages()),
      ];
    } else {
      $response = [
        'code' => 200,
        'codeType' => 'OK',
        'msg' => '已刪除數據',
      ];
    }

    $this->response->setStatusCode($response['code'], $response['codeType']);
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      var_dump($response);
    } else {
      $this->response->setContent(json_encode($response));
      return $this->response->send();
    }

  }
}

