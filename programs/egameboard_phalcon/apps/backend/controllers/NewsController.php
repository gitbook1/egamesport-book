<?php
/*
  电竞新闻管理機制
 */
namespace Egameboard\Backend\Controllers;

class NewsController extends \Personalwork\Mvc\Controller\Base\Application
{
  protected $breadcrumb;

  public function initialize(){
    $this->breadcrumb[] = [
        'icon' => 'fa fa-dashboard',
        'url' => $this->url->get('/backend'),
        'label' => '後台首頁',
    ];

    parent::initialize();
  }


  public function indexAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle',
        'url' => $this->url->get('/backend/news'),
        'label' => '最新消息管理列表',
    ];

    $newses = \Egameboard\Models\News::find(['order'=> 'initTime DESC'])->filter(function($row){
      $data = $row->toArray();
      $data['pId']=$row->Id;
      $data['initTimeFt'] = date('Y-m-d H:i:s', $row->initTime);
      $data['statecodeLabel']= $row->getStatecodeLabel();
      return $data;
    });

    $this->view->setVars([
      'ID' => 'newsTable',
      'theads' => [
        [
          'label'=> '消息標題',
          'datakey' => 'title',
          'width'=> '20%',
          'classsets'=> null,
        ],
        [
          'label'=> '摘要',
          'datakey' => 'summary',
          'width'=> '30%',
          'classsets'=> null,
        ],
        [
          'label'=> '發布時間',
          'datakey' => 'initTimeFt',
          'width'=> '20%',
          'classsets'=> null,
        ],
        [
          'label'=> '內容狀態',
          'datakey' => 'statecodeLabel',
          'width'=> '10%',
          'classsets'=> null,
        ],
        [
          'label'=> '操作',
          'datakey' => 'optfunc',
          'width'=> '20%',
          'classsets'=> null,
        ]
      ],
      'tabledatas' => $newses

    ]);

    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/news');
  }


  public function formAction()
  {
    $this->assets
         ->collection('headerStyle')
         ->addCss('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css', true)
         ->addCss('https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css', true)
         ->addCss('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css', true);
    $this->assets
         ->collection('headerScript')
         ->addJs('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js', true)
         ->addJs('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js', true);

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle',
        'url' => $this->url->get('/backend/news'),
        'label' => '最新消息管理列表',
    ];

    // 若預設沒加標籤使用有設定圖示的遊戲名稱做標籤
    $categories = \Egameboard\Models\Category::find(['columns' => ['label']]);
    if( !count($categories) ){
      $categories = \Egameboard\Models\Egames::find(["Iconpath IS NOT NULL",'columns'=>['label'=>'alias']]);
    }
    $this->view->categories = $categories->toArray();


    if( !empty($id = $this->dispatcher->getParam('pId')) ){
      $this->view->formData = \Egameboard\Models\News::findFirst($id);

      $tmp = $this->view->categories;
      foreach($this->view->formData->NewsCategory as $category){
        $k = array_search($category->label, array_column($tmp, 'label'));
        $tmp[$k]['selected'] = '1';
      }
      $this->view->categories = $tmp;

      $this->view->statelabels = \Egameboard\Models\News::getStatecodeLabels();
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/news/form/'.$id),
        'label' => '編輯[ '.$this->view->formData->title.' ]消息資訊',
      ];
    }else{
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/news/form'),
        'label' => '新增最新消息',
      ];
    }
    $this->view->breadcrumb = $this->breadcrumb;



    return $this->view->pick('backend/news-form');
  }


  /**
   * 處理設定上架下架
   * @todo
   * 1. 調整dataTable功能
   * 2. dataTable使用checkbox直接配置是否置頂
   *
   * */
  public function setStatusCodeAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    if( !($news->save()) ){
      $this->flashSession->warning('儲存數據發生錯誤，'.implode(',', $news->getMessages()));
    }else{
      $this->flashSession->success('已完成數據儲存');
    }


    $this->response->setStatusCode($response['code'], $response['codeType']);
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      var_dump($response);
    } else {
      $this->response->setContent(json_encode($response));
      return $this->response->send();
    }
  }


  public function saveAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }
    // dd($_POST, $_FILES);

    $id = $this->request->getPost('Id', 'int');
    if( intval($id) ){
      $news = \Egameboard\Models\News::findFirst($id);
      $news->statecode = $this->request->getPost('statecode', 'int');
      $news->isTop = $this->request->getPost('isTop', 'int');
    }else{
      $news = new \Egameboard\Models\News;
      $news->statecode = 0;
      $news->initTime = time();
      $news->isTop = 0;
    }

    $news->title = $this->request->getPost('title', 'string');
    $news->summary = $this->request->getPost('summary', 'string');
    $string = str_replace(array("\r", "\n", "\r\n", "\n\r"), '', $_POST['content']);
    $string = preg_replace('/\s\s+/', ' ', $string);
    $news->content = htmlentities(htmlspecialchars($string));

    if( !empty($_FILES['photo']) && $_FILES['photo']['error'] == 0 ){
      $tmp = explode('.', $_FILES['photo']['name']);
      $filename = uniqid().'.'.$tmp[1];
      $abs_path = realpath(PPS_APP_APPSPATH.'/../public/uploads/');
      $relat_path = '/uploads/'.$filename;

      if( !is_writeable($abs_path) ){
        dd('can\'t write');
      }
      if( move_uploaded_file($_FILES["photo"]["tmp_name"], $abs_path.'/'.$filename) ){
        $news->photo = $relat_path;
      }else{
        dd('save file error!');
        $this->flashSession->warning('上傳照片發生錯誤，'.$_FILES["photo"]["error"]);
      }
    }


    if( !$news->save() ){
      $this->flashSession->warning('儲存數據發生錯誤，'.implode(',', $news->getMessages()));
    }else{

      $rows=\Egameboard\Models\NewsCategory::find(["NewsId={$news->Id}"]);
      if( count($rows) ){
        $rows->delete();
      }
      if( count($_POST['categories']) ){
        $lib = new \Personalwork\Package\Pinyin();
        foreach($_POST['categories'] as $category){
          $item=\Egameboard\Models\Category::findFirst(["label='{$category}'"]);
          if( !$item ){
            $item=new \Egameboard\Models\Category();
            $item->label = $category;
            $item->alias = $lib->converter($category);
            $item->save();
          }

          $nc = new \Egameboard\Models\NewsCategory();
          $nc->NewsId = $news->Id;
          $nc->CategoryId = $item->Id;
          $nc->save();
        }
      }

      $this->flashSession->success('已完成數據儲存');
    }

    return $this->response->redirect('/backend/news/form/'.$news->Id);
  }


  public function deleteAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('id', 'int');
    $news = \Egameboard\Models\News::findFirst($id);
    if( !$news->delete() ){
      $response = [
        'code' => 500,
        'codeType' => 'ERR',
        'msg' => '刪除數據發生錯誤，' . implode(',', $news->getMessages()),
      ];
    } else {
      $response = [
        'code' => 200,
        'codeType' => 'OK',
        'msg' => '已刪除數據',
      ];
    }

    $this->response->setStatusCode($response['code'], $response['codeType']);
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      var_dump($response);
    } else {
      $this->response->setContent(json_encode($response));
      return $this->response->send();
    }
  }
}

