<?php
/*
  电竞指南管理機制
 */
namespace Egameboard\Backend\Controllers;

class GuidesController extends \Personalwork\Mvc\Controller\Base\Application
{
  protected $breadcrumb;

  public function initialize(){
    $this->breadcrumb[] = [
        'icon' => 'fa fa-dashboard',
        'url' => $this->url->get('/backend'),
        'label' => '後台首頁',
    ];

    parent::initialize();
  }



  public function indexAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    // $this->assets
    //      ->collection('headerScript')
    //      ->addJs('misc/js/bootstrap-slider/bootstrap-slider.js');

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle',
        'url' => $this->url->get('/backend/guides'),
        'label' => '遊戲指南管理列表',
    ];

    $guides = \Egameboard\Models\Guides::find(['order'=> 'initTime DESC'])->filter(function($row){
      $data = $row->toArray();
      $data['pId']=$row->Id;
      $data['initTimeFt'] = $row->initTimeFt;
      return $data;
    });

    $this->view->setVars([
      'ID' => 'guidesTable',
      'theads' => [
        [
          'label'=> '遊戲標題',
          'datakey' => 'title',
          'width'=> '40%',
          'classsets'=> null,
        ],
        [
          'label'=> '摘要',
          'datakey' => 'summary',
          'width'=> '20%',
          'classsets'=> null,
        ],
        [
          'label'=> '发布时间',
          'datakey' => 'initTimeFt',
          'width'=> '20%',
          'classsets'=> null,
        ],
        [
          'label'=> '操作',
          'datakey' => 'optfunc',
          'width'=> '20%',
          'classsets'=> null,
        ]
      ],
      'tabledatas' => $guides

    ]);

    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/guides');
  }


  public function formAction()
  {
    $this->assets
         ->collection('headerStyle')
         ->addCss('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css', true)
         ->addCss('https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css', true)
         ->addCss('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css', true);
    $this->assets
         ->collection('headerScript')
         ->addJs('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js', true)
         ->addJs('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js', true);

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle',
        'url' => $this->url->get('/backend/guides'),
        'label' => '遊戲指南管理列表',
    ];

    if( !empty($id = $this->dispatcher->getParam('pId')) ){
      $this->view->formData = \Egameboard\Models\Guides::findFirst($id);
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/guides/form/'.$id),
        'label' => '編輯[ '.$this->view->formData->title.' ]遊戲指南資訊',
      ];
    }else{
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/guides/form'),
        'label' => '新增遊戲指南',
      ];
    }
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/guides-form');
  }


  public function saveAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('Id', 'int');
    if( intval($id) ){
      $guides = \Egameboard\Models\Guides::findFirst($id);
    }else{
      $guides = new \Egameboard\Models\Guides;
      $guides->initTime = time();
    }

    $guides->title = $this->request->getPost('title', 'string');
    $guides->summary = $this->request->getPost('summary', 'string');
    // $string = preg_replace('/\R+/', ' ', $_POST['content']);
    // dd($_POST['content'], $string);
    // $string = preg_replace('/\s\s+/', ' ', $string);
    // $string = preg_replace('/[[:^print:]]/', '', $string);
    $guides->content = htmlentities(htmlspecialchars($_POST['content']));
    // dd($_POST['content'], $string, $guides->content);


    if( !empty($_FILES['photo']) && $_FILES['photo']['error'] == 0 ){
      $tmp = explode('.', $_FILES['photo']['name']);
      $filename = uniqid().'.'.$tmp[1];
      $abs_path = realpath(PPS_APP_APPSPATH.'/../public/uploads/');
      $relat_path = '/uploads/'.$filename;

      if( !is_writeable($abs_path) ){
        dd('can\'t write');
      }
      if( move_uploaded_file($_FILES["photo"]["tmp_name"], $abs_path.'/'.$filename) ){
        $guides->photo = $relat_path;
      }else{
        dd('save file error!');
        $this->flashSession->warning('上傳照片發生錯誤，'.$_FILES["photo"]["error"]);
      }
    }


    if( !($guides->save()) ){
      $this->flashSession->warning('儲存數據發生錯誤，'.implode(',', $guides->getMessages()));
    }else{
      $this->flashSession->success('已完成數據儲存');
    }

    return $this->response->redirect('/backend/guides/form/'.$guides->Id);
  }


  public function deleteAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('id', 'int');
    $guides = \Egameboard\Models\Guides::findFirst($id);
    if( !$guides->delete() ){
      $response = [
        'code' => 500,
        'codeType' => 'ERR',
        'msg' => '刪除數據發生錯誤，' . implode(',', $guides->getMessages()),
      ];
    } else {
      $response = [
        'code' => 200,
        'codeType' => 'OK',
        'msg' => '已刪除數據',
      ];
    }

    $this->response->setStatusCode($response['code'], $response['codeType']);
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      var_dump($response);
    } else {
      $this->response->setContent(json_encode($response));
      return $this->response->send();
    }
  }
}

