<?php

namespace Egameboard\Backend\Controllers;

class DefaultController extends \Personalwork\Mvc\Controller\Base\Application
{
  protected $breadcrumb;

  public function initialize()
  {
    parent::initialize();
  }

  /**
   * 預設後台首頁
   */
  public function indexAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    // 以抓取時間為群組，列出每次取得資料筆數。
    $this->view->datacount = \Egameboard\Models\Boarddata::count(["columns"=>['dateTime'], "group" => "dateTime", "limit"=> 10]);

    $this->view->categories = \Egameboard\Models\Category::find();

    // 顯示爬蟲擷取數據內容
    $this->view->boarddatas = \Egameboard\Models\Boarddata::find(["order" => "dateTime DESC, StartTime DESC", "limit" => 300]);

    // 聯絡我們信箱設定
    $this->view->email = \Egameboard\Models\Config::findFirst([" key='EMAIL' "]);

    $this->view->pick('backend/index');
  }


  public function configSaveAction()
  {
    $config = \Egameboard\Models\Config::findFirst([" key='EMAIL' "]);
    if( !$config ){
      $config = new \Egameboard\Models\Config();
      $config->key = 'EMAIL';
    }
    $config->value = $this->request->getPost('contactemail', 'string');
    $config->save();

    $response = [
      'code' => 'OK',
      'msg' => '已更新聯繫我們接收郵件',
    ];

    $this->response->setContent(json_encode($response));
    return $this->response->send();
  }


  public function categoryDeleteAction()
  {
    $flag = false;
    foreach( $_POST['category'] as $id ){
      $category = \Egameboard\Models\Category::findFirst($id);
      if( !$category ){
        $flag = true;
        $errMsg[] = "分類編號{$id}不存在無法移除";
      }else{
        $rmid[] = '#btn_'.$id;
        $category->delete();
      }
    }

    if( !$flag ){
      $response= [
        'code' => 'OK',
        'ids' => $rmid
      ];
    }else{
      $response = [
        'code' => 'ERR',
        'msg' => implode(', ', $errMsg),
        // 'ids' => $rmid
      ];
    }

    $this->response->setContent(json_encode($response));
    return $this->response->send();
  }


  /**
   * 登入介面
   * */
  public function loginAction()
  {
    if ($this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend');
    } elseif ($this->request->isPost()) {
      if ($_POST['account'] == 'admin' && $_POST['password'] == 'admin123') {
        $this->session->set('AUTHENTICATION', true);
        return $this->response->redirect('/backend');
      }else{
        $this->flashSession->warning('您的帳號密碼輸入錯誤請重新輸入！');
      }
    }

    $this->view->setLayout('page-blank');
    $this->view->pick('backend/login');
  }


  /**
   * 登出流程
   */
  public function logoutAction()
  {
    $this->session->remove('AUTHENTICATION');
    // $this->logger->setEvent(\Personalwork\Logger\Adapter\Database::GLOBAL_AUTH_LOGOUT)
    //              ->info('帳號'.$this->session->get("auth-identity")->account.'於系統預設介面進行登出動作結果成功');

    $this->flashSession->success('您已經成功登出，若要繼續操作請重新登入。');

    return $this->response->redirect('/backend/login');
  }


  /**
   * 忘記密碼流程
   *
   * @todo 跟客戶確認形式1. 配置Email 2.回答指定問題直接重設
   * */
  public function forgetpsAction()
  {
    $this->view->setLayout('page-blank');
  }


  /**
   * 設定聯繫我們收發郵件！
   * */
  public function contactusAction()
  {
    $this->breadcrumb[] = [
      'icon' => 'fa fa-dashboard',
      'url' => $this->url->get('/backend'),
      'label' => '後台首頁',
    ];
    $this->breadcrumb[] = [
      'icon' => 'fa fa-circle',
      'url' => $this->url->get('/backend/guides'),
      'label' => '聯繫我們列表',
    ];

    $contacts = \Egameboard\Models\Contact::find(['order' => 'initTime DESC'])->filter(function ($row) {
      $data = $row->toArray();
      $data['pId'] = $row->Id;
      return $data;
    });

    $this->view->setVars([
      'ID' => 'guidesTable',
      'theads' => [
        [
          'label' => '聯繫資訊',
          'datakey' => 'infos',
          'width' => '50%',
          'classsets' => null,
        ],
        [
          'label' => '主旨',
          'datakey' => 'summary',
          'width' => '10%',
          'classsets' => null,
        ],
        [
          'label' => '留言時間',
          'datakey' => 'initTime',
          'width' => '10%',
          'classsets' => null,
        ],
        [
          'label' => '操作',
          'datakey' => 'optfunc',
          'width' => '20%',
          'classsets' => null,
        ]
      ],
      'tabledatas' => $contacts

    ]);

    $this->view->breadcrumb = $this->breadcrumb;

    $this->view->pick('backend/contactus');
  }


  public function notfound404Action()
  { }
}