<?php
/*
  最佳优惠管理機制
 */

namespace Egameboard\Backend\Controllers;

class AdvertismentController extends \Personalwork\Mvc\Controller\Base\Application
{
  protected $breadcrumb;

  public function initialize()
  {
    $this->breadcrumb[] = [
      'icon' => 'fa fa-dashboard',
      'url' => $this->url->get('/backend'),
      'label' => '后台首页',
    ];

    parent::initialize();
  }

  public function indexAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $this->breadcrumb[] = [
      'icon' => 'fa fa-circle',
      'url' => $this->url->get('/backend/advertisment'),
      'label' => '广告管理列表',
    ];

    $advertisments = \Egameboard\Models\Advertisments::find()->filter(function ($row) {
      $data = $row->toArray();
      if( is_file(realpath(PPS_APP_APPSPATH . '/../public/' . $row->photo)) ){
        $data['Photo'] = '<img class="img-responsive" src='.$row->photo.' />';
      }
      $data['pId'] = $row->Id;
      $data['positionLabel'] = $row->getPositionLabel();
      return $data;
    });

    $this->view->setVars([
      'ID' => 'advertismentTable',
      'theads' => [
        [
          'label' => '广告图片',
          'datakey' => 'Photo',
          'width' => '35%',
          'classsets' => null,
        ],
        [
          'label' => '投放位置',
          'datakey' => 'positionLabel',
          'width' => '35%',
          'classsets' => null,
        ],
        [
          'label' => '连结网址',
          'datakey' => 'link',
          'width' => '15%',
          'classsets' => null,
        ],
        [
          'label' => '操作',
          'datakey' => 'optfunc',
          'width' => '15%',
          'classsets' => null,
        ]
      ],
      'tabledatas' => $advertisments

    ]);
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/advertisment');
  }


  public function formAction()
  {
    $this->breadcrumb[] = [
      'icon' => 'fa fa-circle',
      'url' => $this->url->get('/backend/advertisment'),
      'label' => '广告投放管理列表',
    ];

    $this->view->positions = \Egameboard\Models\Advertisments::getPositionLabels();

    if (!empty($id = $this->dispatcher->getParam('pId'))) {
      $this->view->formData = \Egameboard\Models\Advertisments::findFirst($id);
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/advertisment/form/' . $id),
        'label' => '编辑广告资讯',
      ];
    } else {
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/advertisment/form'),
        'label' => '新增广告资讯',
      ];
    }
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/advertisment-form');
  }



  public function saveAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('Id', 'int');
    if (intval($id)) {
      $advertisment = \Egameboard\Models\Advertisments::findFirst($id);
    } else {
      $advertisment = new \Egameboard\Models\Advertisments;
    }

    $advertisment->position = $this->request->getPost('position', 'int');
    $advertisment->intro = $this->request->getPost('intro', 'string');
    $advertisment->link = $this->request->getPost('link', 'string');

    if (!empty($_FILES['photo']) && $_FILES['photo']['error'] == 0) {
      $tmp = explode('.', $_FILES['photo']['name']);
      $filename = uniqid() . '.' . $tmp[1];
      $abs_path = realpath(PPS_APP_APPSPATH . '/../public/uploads/');
      $relat_path = '/uploads/' . $filename;

      if (!is_writeable($abs_path)) {
        dd('can\'t write');
      }
      if (move_uploaded_file($_FILES["photo"]["tmp_name"], $abs_path . '/' . $filename)) {
        $advertisment->photo = $relat_path;
      } else {
        dd('save file error!');
        $this->flashSession->warning('上傳照片發生錯誤，' . $_FILES["photo"]["error"]);
      }
    }

    if (!($advertisment->save())) {
      $this->flashSession->warning('儲存數據發生錯誤，' . implode(',', $advertisment->getMessages()));
    } else {
      $this->flashSession->success('已完成數據儲存');
    }

    return $this->response->redirect('/backend/advertisment/form/' . $advertisment->Id);
  }


  public function deleteAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('id', 'int');
    $advertisment = \Egameboard\Models\Advertisments::findFirst($id);
    if (!$advertisment->delete()) {
      $response = [
        'code' => 500,
        'codeType' => 'ERR',
        'msg' => '刪除數據發生錯誤，' . implode(',', $advertisment->getMessages()),
      ];
    } else {
      $response = [
        'code' => 200,
        'codeType' => 'OK',
        'msg' => '已刪除數據',
      ];
    }

    $this->response->setStatusCode($response['code'], $response['codeType']);
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      var_dump($response);
    } else {
      $this->response->setContent(json_encode($response));
      return $this->response->send();
    }
  }
}
