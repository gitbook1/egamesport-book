<?php
/*
  最佳优惠管理機制
 */

namespace Egameboard\Backend\Controllers;

class EgamesController extends \Personalwork\Mvc\Controller\Base\Application
{
  protected $breadcrumb;

  public function initialize()
  {
    $this->breadcrumb[] = [
      'icon' => 'fa fa-dashboard',
      'url' => $this->url->get('/backend'),
      'label' => '后台首页',
    ];

    parent::initialize();
  }

  public function indexAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $this->breadcrumb[] = [
      'icon' => 'fa fa-circle',
      'url' => $this->url->get('/backend/egames'),
      'label' => '电竞游戏列表',
    ];

    $egames = \Egameboard\Models\Egames::find()->filter(function ($row) {
      $data = $row->toArray();
      if( is_file(realpath(PPS_APP_APPSPATH . '/../public/' . $row->Iconpath)) ){
        $data['displayicon'] = '<img class="img-responsive" style="height:100px;margin:-20px;" src='.$row->Iconpath.' />';
      }else{
        $data['displayicon'] = '';
      }
      $data['pId'] = $row->Id;
      return $data;
    });

    $this->view->setVars([
      'ID' => 'egamesTable',
      'theads' => [
        [
          'label' => '电竞游戏',
          'datakey' => 'Name',
          'width' => '25%',
          'classsets' => null,
        ],
        [
          'label' => '显示名称',
          'datakey' => 'alias',
          'width' => '25%',
          'classsets' => null,
        ],
        [
          'label' => '游戏显示图示',
          'datakey' => 'displayicon',
          'width' => '20%',
          'classsets' => '',
        ],
        [
          'label' => '操作',
          'datakey' => 'optfunc',
          'width' => '30%',
          'classsets' => null,
        ]
      ],
      'tabledatas' => $egames

    ]);
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/egames');
  }


  public function formAction()
  {
    $this->breadcrumb[] = [
      'icon' => 'fa fa-circle',
      'url' => $this->url->get('/backend/egames'),
      'label' => '电竞游戏管理列表',
    ];

    if (!empty($id = $this->dispatcher->getParam('pId'))) {
      $this->view->formData = \Egameboard\Models\Egames::findFirst($id);
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/egames/form/' . $id),
        'label' => '编辑电竞游戏资讯',
      ];
    } else {
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/egames/form'),
        'label' => '新增电竞游戏资讯',
      ];
    }
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/egames-form');
  }



  public function saveAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('Id', 'int');
    if (intval($id)) {
      $egames = \Egameboard\Models\Egames::findFirst($id);
    } else {
      $egames = new \Egameboard\Models\Egames;
    }

    $egames->Name = $this->request->getPost('Name', 'string');
    $egames->alias = $this->request->getPost('alias', 'string');

    if (!empty($_FILES['Iconpath']) && $_FILES['Iconpath']['error'] == 0) {
      $tmp = explode('.', $_FILES['Iconpath']['name']);
      $filename = uniqid() . '.' . $tmp[1];
      $abs_path = realpath(PPS_APP_APPSPATH . '/../public/uploads/');
      $relat_path = '/uploads/' . $filename;

      if (!is_writeable($abs_path)) {
        dd('can\'t write');
      }
      if (move_uploaded_file($_FILES["Iconpath"]["tmp_name"], $abs_path . '/' . $filename)) {
        $egames->Iconpath = $relat_path;
      } else {
        dd('save file error!');
        $this->flashSession->warning('上傳照片發生錯誤，' . $_FILES["Iconpath"]["error"]);
      }
    }

    if (!($egames->save())) {
      $this->flashSession->warning('儲存數據發生錯誤，' . implode(',', $egames->getMessages()));
    } else {
      $this->flashSession->success('已完成數據儲存');
    }

    return $this->response->redirect('/backend/egames/form/' . $egames->Id);
  }


  public function deleteAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('id', 'int');
    $egames = \Egameboard\Models\Egames::findFirst($id);
    if (!$egames->delete()) {
      $response = [
        'code' => 500,
        'codeType' => 'ERR',
        'msg' => '刪除數據發生錯誤，' . implode(',', $egames->getMessages()),
      ];
    } else {
      $response = [
        'code' => 200,
        'codeType' => 'OK',
        'msg' => '已刪除數據',
      ];
    }

    $this->response->setStatusCode($response['code'], $response['codeType']);
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      var_dump($response);
    } else {
      $this->response->setContent(json_encode($response));
      return $this->response->send();
    }
  }
}
