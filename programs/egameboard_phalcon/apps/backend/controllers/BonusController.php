<?php
/*
  最佳优惠管理機制
 */
namespace Egameboard\Backend\Controllers;

class BonusController extends \Personalwork\Mvc\Controller\Base\Application
{
  protected $breadcrumb;

  public function initialize(){
    $this->breadcrumb[] = [
        'icon' => 'fa fa-dashboard',
        'url' => $this->url->get('/backend'),
        'label' => '後台首頁',
    ];

    parent::initialize();
  }

  public function indexAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle',
        'url' => $this->url->get('/backend/bonus'),
        'label' => '最佳優化管理列表',
    ];

    $bonuses = \Egameboard\Models\Bonus::find(['order'=> 'level DESC'])->filter(function($row){
      $data = $row->toArray();
      $data['pId']=$row->Id;
      $data['ProviderName']=  $row->Providers->Name;
      return $data;
    });

    $this->view->setVars([
      'ID' => 'bonusTable',
      'theads' => [
        [
          'label'=> '平台商資訊',
          'datakey' => 'ProviderName',
          'width'=> '10%',
          'classsets'=> null,
        ],
        [
          'label'=> '優惠名次',
          'datakey' => 'level',
          'width'=> '10%',
          'classsets'=> null,
        ],
        [
          'label'=> '評分值',
          'datakey' => 'rank',
          'width'=> '10%',
          'classsets'=> null,
        ],
        [
          'label'=> '強調文字',
          'datakey' => 'intro',
          'width'=> '35%',
          'classsets'=> null,
        ],
        [
          'label'=> '框線文字',
          'datakey' => 'infomation',
          'width'=> '15%',
          'classsets'=> null,
        ],
        [
          'label'=> '操作',
          'datakey' => 'optfunc',
          'width'=> '20%',
          'classsets'=> null,
        ]
      ],
      'tabledatas' => $bonuses

    ]);
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/bonus');
  }


  public function formAction()
  {
    $this->assets
         ->collection('headerStyle')
         ->addCss('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css', true);
    $this->assets
         ->collection('headerScript')
         ->addJs('misc/js/bootstrap-slider/bootstrap-slider.js')
         ->addJs('https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js', true);

    $this->breadcrumb[] = [
        'icon' => 'fa fa-circle',
        'url' => $this->url->get('/backend/bonus'),
        'label' => '最佳優化管理列表',
    ];

    $this->view->Providers = \Egameboard\Models\Providers::find(['order'=> 'SortOrder ASC'])->toArray();

    if( !empty($id = $this->dispatcher->getParam('pId')) ){
      $this->view->formData = \Egameboard\Models\Bonus::findFirst($id);
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/bonus/form/'.$id),
        'label' => '編輯[ '.$this->view->formData->Providers->Name.' ]最佳優惠資訊',
      ];
    }else{
      $this->breadcrumb[] = [
        'icon' => 'fa fa-dot-circle-o',
        'url' => $this->url->get('/backend/bonus/form'),
        'label' => '新增最佳優惠資訊',
      ];
    }
    $this->view->breadcrumb = $this->breadcrumb;

    return $this->view->pick('backend/bonus-form');
  }


  public function loadProviderAction()
  {
    $providerId = $this->request->getPost('id','int');
    $provider = \Egameboard\Models\Providers::findFirst($providerId);
    // dd($provider->toArray());

    $html = 'No Icon';
    if( $provider->localIcon ){
      $html = '<img class="img-responsive img-thumbnail" src="'.$provider->localIcon.'" style="max-width: 300px;" />';
    }elseif( $provider->Icon ){
      $html = '<img class="img-responsive img-thumbnail" src="'.$provider->Icon.'" style="max-width: 300px;" />';
    }

    $this->response->setContent($html);
    return $this->response->send();
  }


  public function saveAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('Id', 'int');
    if( intval($id) ){
      $bonus = \Egameboard\Models\Bonus::findFirst($id);
    }else{
      $bonus = new \Egameboard\Models\Bonus;
    }

    $bonus->ProviderId = $this->request->getPost('ProviderId', 'int');
    $bonus->level = $this->request->getPost('level', 'int');

    $string = str_replace(array("\r", "\n", "\r\n", "\n\r"), '', $_POST['intro']);
    $string = preg_replace('/\s\s+/', ' ', $string);
    $bonus->intro = htmlentities(htmlspecialchars($string));
    $bonus->rank = $this->request->getPost('rank', 'int');
    $bonus->infomation = $this->request->getPost('infomation', 'string');

    if( !($bonus->save()) ){
      $this->flashSession->warning('儲存數據發生錯誤，'.implode(',', $bonus->getMessages()));
    }else{
      $this->flashSession->success('已完成數據儲存');
    }

    return $this->response->redirect('/backend/bonus/form/'.$bonus->Id);
  }


  public function deleteAction()
  {
    if (!$this->session->has('AUTHENTICATION')) {
      return $this->response->redirect('/backend/login');
    }

    $id = $this->request->getPost('id', 'int');
    $bonus = \Egameboard\Models\Bonus::findFirst($id);
    if( !$bonus->delete() ){
      $response = [
        'code' => 500,
        'codeType' => 'ERR',
        'msg' => '刪除數據發生錯誤，' . implode(',', $bonus->getMessages()),
      ];
    } else {
      $response = [
        'code' => 200,
        'codeType' => 'OK',
        'msg' => '已刪除數據',
      ];
    }

    $this->response->setStatusCode($response['code'], $response['codeType']);
    if ($_GET['DEBUG']) {
      $this->response->setContentType('text/html;charset=UTF-8;');
      var_dump($response);
    } else {
      $this->response->setContent(json_encode($response));
      return $this->response->send();
    }
  }
}

