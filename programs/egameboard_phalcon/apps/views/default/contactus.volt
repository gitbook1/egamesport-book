<div class="container main-content">
  <h1 class="contact-header">联系我们</h1>

  <div class="row">
    <div class="col-xs-12 col-sm-offset-2 col-sm-4" >
      <div class="form-group text-left">
        <p>客户服务：(早上10:00 - 晚上18:30)</p>
        <label class="control-label">Skype：moduodds</label><br/>
        <label class="control-label">邮箱：cs@moduodds.com</label>
      </div>
    </div>

    <div class="col-xs-12 col-sm-5">
      <div class="form-group text-left">
        <p>商务＆广告合作</p>
        <label class="control-label">联系人：monica</label><br/>
        <label class="control-label">Telegram：moduodds</label><br/>
        <label class="control-label">邮箱：ad@moduodds.com</label>
      </div>
    </div>


    <form class="col-xs-12 col-sm-offset-2 col-sm-8 form-horizontal" role="form">
      <div class="form-group text-left">
        <label for="contact-name" class="control-label">联系人</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="联系人" />
      </div>
      <div class="form-group text-left">
        <label for="contact-email" class="control-label">邮箱</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="邮箱" />
      </div>

      <div class="form-group text-left">
        <label for="contact-subject" class="control-label">主旨</label>
        <input type="text" class="form-control" id="subject" name="subject" placeholder="主旨" />
      </div>

      <div class="form-group text-left">
        <label for="contact-message" class="control-label">訊息內容</label>
        <textarea class="form-control" id="message" name="message" rows="5" cols="30" placeholder="訊息內容"></textarea>
      </div>

      <div class="form-group">
          <button type="submit" class="btn btn-primary" id="sendBtn">送出</button>
      </div>
    </form>

  </div>

</div>


<script type="text/javascript">
$(function(){
  $("form").submit(function(){
    $.ajax({
      url: "{{ url('/contact-post') }}",
      data: $(this).serialize(),
      dataType: 'json',
      type: 'POST',
      beforeSend: function(){
        $("#sendBtn").prop('disabled', true);
      },
      success: function( resp ){

      },
      complete: function(){
        $("#sendBtn").prop('disabled', false);
      }
    });
    return false;
  });
});
</script>

<style type="text/css">
.contact-header{
  margin-bottom: 15px;
}

p{
  color: #D2D2D2;
  text-align: left;
}

.form-group {
  color: #D2D2D2;
}
</style>