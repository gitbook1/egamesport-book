<div class="right-block">
  <h4 class="right-header text-left">最新電競新聞</h4>
  <div class="panel modu-block">
    <div class="panel-body">

      {% set leftitems = [1, 2, 3] %}
      {% for key, value in leftitems %}
      <div class="right-entry clickable">
        <div class="right-news-img">
          <img class="img-responsive" data-no-lazy="1" alt="Turkish League of Legends team sanctioned for mistreating players" src="https://www.esports.net/wp-content/uploads/2019/12/xgalatasary-esports-turkish-lol-team-banned-300x169.jpg.pagespeed.ic.rMawtyb_k7.jpg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">
        </div>
        <div class="date-gray">31/12/2019</div>
        <div class="label-color">LoL</div>
          <a class="right-title" href="https://www.esports.net/news/lol/turkish-league-of-legends-team-sanctioned-for-mistreating-players/">Turkish League of Legends team sanctioned for mistreating players</a>
          <div class="right-excerpt">League of Legends is the most popular esport in the world – teams from all over the planet
          <span class="read-more">More</span>
        </div>
      </div>
      {% endfor %}

    </div>
    <div class="panel-footer modu-block-footer"> more>> </div>
  </div>
</div>