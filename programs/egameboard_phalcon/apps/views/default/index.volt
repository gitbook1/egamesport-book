<div class="main-content">

<div class="scrolltop hidden-xs">
  <button class="btn btn-lg btn-primary opt-totop">回頂部</button>
</div>

<div class="container">
  <p class="lead index-lead">最好赔率参考。 魔都赔率是一个由电竞迷打造的电竞网站。在CSGO，英雄联盟和DotA
    2等顶级游戏中查找您需要的所有电子竞技投注赔率和调度信息。我们与许多运营商合作，并实时接收赔率更新，为您提供最大的机会找到您想要的利润。</p>

  <div class="row" style="margin: 20px auto;">
    <div id="index-top-carousel" class="col-xs-12 carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        {% for key, carousel in advertisments['carousel'] %}
        <li data-target="#index-top-carousel" data-slide-to="{{key}}" {% if key == 0 %} class="active" {% endif %}></li>
        {% endfor %}
      </ol>

      <div class="carousel-inner" role="listbox">
        {% for key, carousel in advertisments['carousel'] %}
        {% if key == 0 %}
          <div class="item active">
        {% else %}
          <div class="item">
        {% endif %}
            <a href="{{carousel.link}}" target="_blank">
            <img src="{{carousel.photo}}" alt="{{carousel.photo}}">
            </a>
            <div class="carousel-caption">
              {{carousel.intro}}
            </div>

          </div>
        {% endfor %}
      </div>

      <a class="left carousel-control" href="#index-top-carousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#index-top-carousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <div class="input-container">
        <input type="text" id="q" placeholder="Enter your search here" />
        <img class="" alt="Search Icon" src="/misc/img/moduodds/search-icon-blue.png" />
      </div>
    </div>
    <div class="col-xs-12 col-sm-2">
      <div class="filter-cell filter-all">全部</div>
    </div>
    <div class="col-xs-6 col-sm-2 dropdown">
      <div class="filter-cell filter-mobi-xs">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
          role="button" aria-haspopup="true" aria-expanded="false">
          賠率 <span class="odds-selected">{{DEFAULT_ODD}}</span> <span class="caret"></span>
        </a>
        <ul id="oddslist" class="dropdown-menu menu-modu">
          {% for key, val in ODDS %}
          <li><a href="javascript:void(0);" data-key="{{key}}">{{val}}</a></li>
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="col-xs-6 col-sm-2 dropdown">
      <div class="filter-cell filter-mobi-xs">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
          role="button" aria-haspopup="true" aria-expanded="false">
          時區 <span class="timezone-selected">{{DEFAULT_TIMEZONE}}</span> <span class="caret"></span>
        </a>
        <ul id="timezonelist" class="dropdown-menu menu-modu">
          {% for hrs, timezone in TIMEZONES %}
          <li id="{{hrs}}"><a href="javascript:void(0);">{{timezone}}</a></li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
  <div class="filter-container">
  {% for key, egame in sports %}
    <div class="filter-cell filter-egame" data-egame="{{ key }}">
      {% if egame['icon'] is not empty %}
      <img class="img-responsive img-filter" src="{{ egame['icon'] }}" />
      {% endif %}
      <span>{{ egame['name'] }}</span>
    </div>
  {% endfor %}
  </div>

    {% for key, gamedata in boarddatas %}
    <div class="odds-row {{gamedata.SportName}} {% if key > 9 %}hide{% endif %} ">
      <div class="row-cell icon">
        <img class="img-responsive img-dataicon" src="{{ sports[gamedata.SportName]['icon'] }}" />
      </div>
      <div class="row-cell time">
        <div class="title">
          <span class="q-text zonechange" data-utctime="{{ gamedata.StartTimeUTC }}">{{ gamedata.StartTimeFt }}</span>
        </div>
      </div>

      <div class="row-cell display">
        <div class="title">
          <span class="q-text">{{ gamedata.TournamentName }}</span>
        </div>
      </div>

      <div class="row-cell team-holder">
        <div class="row-cell team-name">
          <a class="q-text" href="{{ gamedata.bestLeftLink }}" target="_blank">{{ gamedata.HomeTeam }}</a>
        </div>
        <div class="row-cell odd-logo-holder">
          <div class="row-odds odds-home">
            <span class="oddchange"
                  data-american="{{gamedata.bestLeftValueAmerican}}"
                  data-decimal="{{gamedata.bestLeftValue}}"
                  data-Fractions="{{gamedata.bestLeftValueFractions}}">{{ gamedata.bestLeftValue }}</span>
          </div>
          <div class="logo-container">
            <div class="logo-wrap">
                <img class="img-responsive board-logo" alt="{{ gamedata.bestLeftName }}" src="{{ gamedata.bestLeftIcon }}" />
            </div>
          </div>
        </div>
      </div>

      <div class="row-cell vsicon">
        <span class="fa fa-times fa-2x"></span>
      </div>

      <div class="row-cell team-holder">
        <div class="row-cell odd-logo-holder">
          <div class="row-odds">
            <span class="oddchange"
                  data-american="{{gamedata.bestRightValueAmerican}}"
                  data-decimal="{{gamedata.bestRightValue}}"
                  data-Fractions="{{gamedata.bestRightValueFractions}}">{{ gamedata.bestRightValue }}</span>
          </div>
          <div class="logo-container">
            <div class="logo-wrap">
                <img class="img-responsive board-logo" alt="{{ gamedata.bestRightName }}"
                  src="{{ gamedata.bestRightIcon }}" />
            </div>
          </div>
        </div>
        <div class="row-cell team-name">
          <a class="q-text" href="{{ gamedata.bestRightLink }}" target="_blank">{{ gamedata.AwayTeam }}</a>
        </div>
      </div>

      <div class="row-cell append" data-toggle="modal" data-target="#modal{{gamedata.boarddataId}}">
        <span  >Full Odds</span>
        <span class="fa fa-arrows-alt fa-2x"></span>
      </div>
    </div>

    {% endfor %}


    <button class="odds-row btn-more opt-more">加载更多</button>

</div>

<div class="container hidden-xs">
  {{ partial('default/odd-fulltable') }}
</div>

<div id="footAdBlock" class="container">
<div class="row">
  <div class="col-xs-12 col-sm-4 adblock-item">
    {% if advertisments['boxleft'] is not empty %}
    <a href="{{advertisments['boxleft'].link}}">
      <img class="img-responsive" src="{{advertisments['boxleft'].photo}}" alt="{{advertisments['boxleft'].intro}}" />
    </a>
    {% endif %}
  </div>
  <div class="col-xs-12 col-sm-4 adblock-item">
    {% if advertisments['boxmiddle'] is not empty %}
    <a href="{{advertisments['boxmiddle'].link}}">
      <img class="img-responsive" src="{{advertisments['boxmiddle'].photo}}" alt="{{advertisments['boxmiddle'].intro}}" />
    </a>
    {% endif %}
  </div>
  <div class="col-xs-12 col-sm-4 adblock-item">
    {% if advertisments['boxright'] is not empty %}
    <a href="{{advertisments['boxright'].link}}">
      <img class="img-responsive" src="{{advertisments['boxright'].photo}}" alt="{{advertisments['boxright'].intro}}" />
    </a>
    {% endif %}
  </div>
</div>
</div>

</div> {# .main-content #}

{% for key, gamedata in boarddatas %}
<div id="modal{{gamedata.boarddataId}}" class="modal fade append-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">

  <div class="modal-dialog modal-lg" role="document">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="fa fa-times"></span>
      </button>
      <h4 class="modal-title" id="detailHeader">{{ gamedata.TournamentName }}</h4>
      <h5 class="modal-subtitle zonechange" data-utctime="{{ gamedata.StartTimeUTC }}"> {{ gamedata.StartTimeFt2 }}</h5>
    </div>

    <div class="modal-body">
      <div class="row">

        <div class="col-xs-6">
          <div class="team-name">{{ gamedata.bestLeftName }}</div>

          {% for key, odds in gamedata.BoarddataOdds %}
            {% if key%2==1 %}
            <div class="odds-cell">
              <div class="odd-container">
                <div class="logo-holder">
                  <img src="{{odds.Providers.displayIcon}}" class="img-responsive" alt="{{odds.Providers.Name}}" />
                </div>
                <div class="odd-holder {% if odds.isBest %}best-odd{% endif %}">
                  <span class="oddchange" data-american="{{odds.ValueAmerican}}" data-decimal="{{odds.ValueFractions}}" data-Fractions="{{odds.ValueFractions}}">{{odds.Value}}</span>
                </div>
              </div>
            </div>
            {% endif %}
          {% endfor %}
        </div>

        <div class="col-xs-6">
          <div class="team-name">{{ gamedata.bestRightName }}</div>

          {% for key, odds in gamedata.BoarddataOdds %}
            {% if key%2==0 %}
            <div class="odds-cell">
              <div class="odd-container">
                <div class="logo-holder">
                  <img src="{{odds.Providers.displayIcon}}" class="img-responsive" alt="{{odds.Providers.Name}}" />
                </div>
                <div class="odd-holder {% if odds.isBest %}best-odd{% endif %}">
                  <span class="oddchange" data-american="{{odds.ValueAmerican}}" data-decimal="{{odds.ValueFractions}}" data-Fractions="{{odds.ValueFractions}}">{{odds.Value}}</span>
                </div>
              </div>
            </div>
            {% endif %}
          {% endfor %}
        </div>

      </div>
    </div>

  </div>
</div>
{% endfor %}





<script type="text/javascript">
$(function(){
  // Offset hours:
  var hrs = -(new Date().getTimezoneOffset() / 60);
  $("li#"+hrs).addClass('active');


  // 載入更多
  $(".opt-more").click(function(){
    $(".event-row").css({opacity:1});
    $(".odds-row, .event-row").removeClass('hide');
  });

  $('.append-modal')
  .on('show.bs.modal', function (e) {
    $(".container").hide();
  })
  .on('hide.bs.modal', function (e) {
    $(".container").show();
  });

  // 切換時間
  $("#timezonelist > li").click(function(){
    $("#timezonelist > li").removeClass('active');
    $(this).addClass('active');
    $(".timezone-selected").text( $(this).find('a').text() );

    select_hour = $(this).prop('id');

    $(".zonechange").each(function(){
      var utc = $(this).data('utctime');
      diff = parseInt(select_hour)*3600;
      // console.log( 'utc: ', utc, 'newtime: ', utc+diff);
      // conver to microsecond
      dt = moment.utc( (utc+diff)*1000 );
      // console.log( dt.format("MMM-DD HH:mm")  );
      $(this).text( dt.format("MMM-DD HH:mm") );
    });
    $(".zonechange-up").each(function(){
      var utc = $(this).data('utctime');
      diff = parseInt(select_hour)*3600;
      dt = moment.utc( (utc+diff)*1000 );
      $(this).text( dt.format("MMM-DD") );
    });
    $(".zonechange-down").each(function(){
      var utc = $(this).data('utctime');
      diff = parseInt(select_hour)*3600;
      dt = moment.utc( (utc+diff)*1000 );
      $(this).text( dt.format("HH:mm") );
    });
  });

  // 切換賠率
  $("#oddslist > li").click(function(){
    $("#oddslist > li").removeClass('active');
    $(this).addClass('active');
    $(".odds-selected").text( $(this).find('a').text() );

    select_odd = $(this).find('a').data('key').toLowerCase();

    $(".oddchange").each(function(){
      var setVal = $(this).data( select_odd );
      $(this).text( setVal );
    });
  });


  // 全文搜尋
  $("#q").keyup(function(){
    $(".odds-row, .event-row").removeClass('hide');
    $(".event-row").css({opacity:1});
    var search = $(this).val();
    if (search) {
      $(".odds-row, .event-row").not(":contains(" + search + ")").addClass('hide');
      $(".opt-more").hide();
    }
  });

  // 切換遊戲
  $(".filter-egame").click(function(){
    $(".odds-row, .event-row").addClass('hide');
    var filter_egame = $(this).data('egame');
    $(".odds-row, .event-row").each(function(){
      if( $(this).hasClass(filter_egame) ){
        $(this).removeClass('hide').css({opacity:1});
      }
    });
    $(".opt-more").hide();
  });
  $(".filter-all").click(function(){
    $(".odds-row, .event-row").removeClass('hide');
    $(".event-row").css({opacity:1});
  });


  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('.opt-totop').fadeIn();
    } else {
      $('.opt-totop').fadeOut();
    }
  });
  $('.opt-totop').click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 100);
    return false;
  });

});
</script>