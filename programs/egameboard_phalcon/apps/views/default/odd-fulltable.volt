<div id="odds-table-container-desktop">
    <div class="adapt-container">
      <div class="odds-table-wrap">

        <div class="title-row">
          <div class="blue-indent"></div>
          <div class="title-container"><h2>Best Esports Odds</h2></div>
        </div>

        <div class="header-row">
          <div class="load-more">
            <div class="index-cell"> </div>
          </div>
          <div class="left">
            <div class="index-cell game ">Esport</div>
            <div class="index-cell when">Time</div>
            <div class="index-cell match">Match</div>
            <div class="index-cell">Event</div>
          </div>
          <div class="right">

            <a class="index-cell" href="/en/full-table/gt/BuffBet">
              <img src="/misc/img/moduodds/providers/BuffBet-logo.png" alt="BuffBet" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/Pinnacle">
              <img src="/misc/img/moduodds/providers/Pinnacle-logo.png" alt="Pinnacle" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/1XBet">
              <img src="/misc/img/moduodds/providers/1XBet-logo.png" alt="1XBet" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/gg.bet">
              <img src="/misc/img/moduodds/providers/gg.bet-logo.png" alt="gg.bet" class="gg.img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/EGB">
              <img src="/misc/img/moduodds/providers/EGB-logo.png" alt="EGB" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/BetHard">
              <img src="/misc/img/moduodds/providers/BetHard-logo.png" alt="BetHard" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/rivalry.gg">
              <img src="/misc/img/moduodds/providers/rivalry.gg-logo.png" alt="rivalry.gg" class="rivalry.img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/Dafabet">
              <img src="/misc/img/moduodds/providers/Dafabet-logo.png" alt="Dafabet" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/LootBet">
              <img src="/misc/img/moduodds/providers/LootBet-logo.png" alt="LootBet" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/VBet">
              <img src="/misc/img/moduodds/providers/VBet-logo.png" alt="VBet" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/Betspawn">
              <img src="/misc/img/moduodds/providers/Betspawn-logo.png" alt="Betspawn" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/WilliamHill">
              <img src="/misc/img/moduodds/providers/WilliamHill-logo.png" alt="WilliamHill" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/XBet">
              <img src="/misc/img/moduodds/providers/XBet-logo.png" alt="XBet" class="img-responsive header-cell-img">
            </a>

            <a class="index-cell" href="/en/full-table/gt/Unikrn">
              <img src="/misc/img/moduodds/providers/Unikrn-logo.png" alt="Unikrn" class="img-responsive header-cell-img">
            </a>

          </div>
        </div>

        {% for key, data in tabledatas %}
        {% if data['index'] < 10 %}
        <div class="event-row {{data['SportName']}}" style="display: block; opacity: 1;">
        {% elseif data['index'] < 12 %}
        <div class="event-row {{data['SportName']}}" style="display: block; opacity: 0.2;">
        {% else %}
        <div class="event-row {{data['SportName']}} hide" style="display: block; opacity: 1;">
        {% endif %}

            <div class="load-more" data-toggle="modal" data-target="#modal{{key}}">
              <span>+</span>
            </div>

            <div class="left">
              <div class="index-cell game">
                <img alt="" src="{{data['left'][0]}}" />
              </div>
              <div class="index-cell">
                <div class="text-container">
                  <span>
                    <font class="zonechange-up" data-utctime="{{ data['left'][1] }}">{{data['left'][2]}}</font><br><font  class="zonechange-down" data-utctime="{{ data['left'][1] }}">{{data['left'][3]}}</font>
                  </span>
                </div>
              </div>
              <div class="index-cell match">
                <div class="text-container">
                  <span>{{data['left'][4]}}</span>
                </div>
              </div>
              <div class="index-cell">
                <div class="text-container">
                  <span>{{data['left'][5]}}</span>
                </div>
              </div>
            </div>

            <div class="right">
              {% for tableval in data['right'] %}
                <div class="index-cell-pair desktop-provider-click">
                    <div class="index-cell">
                      {% if tableval[0][0] != '-' %}
                      <span class="oddchange {% if tableval[0][3] == 1 %}best{% endif %}"
                            data-decimal="{{tableval[0][0]}}"
                            data-american="{{tableval[0][1]}}"
                            data-Fractions="{{tableval[0][2]}}"
                      >{{tableval[0][0]}}</span>
                      {% if tableval[0][3] == 1 %}<div class="best-triangle "></div>{% endif %}
                      {% else %}
                      {{tableval[0][0]}}
                      {% endif %}
                    </div>
                    <div class="index-cell">
                      {% if tableval[1][0] != '-' %}
                      <span class="oddchange {% if tableval[1][3] == 1 %}best{% endif %}"
                            data-decimal="{{tableval[1][0]}}"
                            data-american="{{tableval[1][1]}}"
                            data-Fractions="{{tableval[1][2]}}"
                      >{{tableval[1][0]}}</span>
                      {% if tableval[1][3] == 1 %}<div class="best-triangle "></div>{% endif %}
                      {% else %}
                      {{tableval[1][0]}}
                      {% endif %}
                    </div>
                    <div class="line"></div>
                </div>
              {% endfor %}
            </div>
          </div>
        {% endfor %}
        <div class="load-more-matches-container" style="display: flex;">
          <div class="text-center">
            <button class="opt-more blue-button">加载更多</button>
          </div>
        </div>


      </div>

    </div>
</div>

