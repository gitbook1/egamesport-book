<div class="main-content container">
  <div class="row">

    <div class="col-xs-12 col-sm-8 col-md-9">

      <div class="box top-news-container">
        <div class="modu-block box-entry bigger">

          <div class="news-image-container" style="background-image:url(https://www.esports.net/wp-content/uploads/2019/11/esports-betting-tipster.jpg)">
            <div class="title-box">
                <time class="date-gray">31/12/2019</time><br>
                <div class="category-purple">LOL</div>
                <div class="category-purple">LOL</div>
                <h3 class="h3"><a href="https://www.esports.net/wiki/guides/esports-betting-tipster/">Esports Betting Tipster: Your Ultimate Guide to Find Winning Betting Tips</a></h3>
            </div>
          </div>
          <div class="news-excerpt">If you’re a fan of Esports, you’ve likely tried out some Esports betting. Although, unlike traditional sports, it can be a little harder to find some tips and winning bets for Esports. This Esports betting guide tells you where to find an Esports betting tipster, and how to know which ones are worth listening to.
            <span class="read-more">More ></span>
          </div>

        </div>

        <div class="modu-block box-entry">
          <div class="news-image-container" style="background-image:url(https://www.esports.net/wp-content/uploads/2019/12/fortnite-new-years-2020.jpg)">
            <div class="title-box">
              <div class="date-gray">30/12/2019</div><br>
              <div class="category-purple">LOL</div>
              <h3 class="h3"><a href="https://www.esports.net/news/fortnite/fortnites-next-live-event-for-new-years-2020-leaks/">Fortnite’s Next Live Event for New Years 2020 Leaks</a></h3>
            </div>
          </div>
          <div class="news-excerpt">Fortnite Chapter 2 opened with one of the biggest and<span class="read-more">More ></span></div>
        </div>

        <div class="modu-block box-entry">
          <div class="news-image-container" style="background-image:url(https://www.esports.net/wp-content/uploads/2019/12/fortnite-new-years-2020.jpg)">
            <div class="title-box">
              <div class="date-gray">30/12/2019</div><br>
              <div class="category-purple">LOL</div>
              <h3 class="h3"><a href="https://www.esports.net/news/fortnite/fortnites-next-live-event-for-new-years-2020-leaks/">Fortnite’s Next Live Event for New Years 2020 Leaks</a></h3>
            </div>
          </div>
          <div class="news-excerpt">Fortnite Chapter 2 opened with one of the biggest and<span class="read-more">More ></span></div>
        </div>
      </div>

      <div class="news-box">
        {% for key, news in newsitems %}
        <div class="media news-item modu-block">
          <div class="media-left">
            <a href="#">
              <img class="media-object news-thumbnail" src="https://www.esports.net/wp-content/uploads/2019/12/fortnite-new-years-2020.jpg" alt="..." />
            </a>
          </div>
          <div class="media-body text-left">
            <div class="date-gray">30/12/2019</div>
            <div class="category-purple">LOL</div>
            <h4 class="media-heading text-left">Media heading</h4>
            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.
          </div>
        </div>
        {% endfor %}

      </div>

    </div>


    <div class="col-xs-12 col-sm-4 col-md-3">

      {{ partial('default/rightblock-news') }}

      {{ partial('default/rightblock-guides') }}

    </div>


  </div>
</div>


