<div class="main-content container">

  <div class="row">

    <h1 class="h1 col-xs-12 text-left">电子竞技博彩奖金</h1>
    <h2 class="h2 col-xs-12 text-left">您需要了解的有关电子竞技博彩奖金和促销的所有信息</h2>
    <p class="text-muted col-xs-10  col-xs-offset-1 col-sm-12 col-sm-offset-0">投注电子竞技是一个相对较新的现象，但是很多人都从投注《 Dota 2》和《英雄联盟》到《 CS：GO》和《风暴英雄》等游戏中获得了丰厚的收益。随着电子竞技开始进入主流，越来越多的传统博彩网站现在为您提供了一种简单安全的方法来尝试一些电子竞技博彩。从William Hill这样的传统博彩公司到esportsbetting.com等专门的电子竞技博彩网站，您的电子竞技博彩肯定有很多选择。随着电子竞技博彩业的竞争日趋激烈，许多博彩网站将提供一些可观的奖励，以吸引您注册他们的服务。</p>
    <p class="text-muted col-xs-10  col-xs-offset-1 col-sm-12 col-sm-offset-0">因此，我们将对来自这些博彩网站的最佳电子竞技奖金进行详细比较。这将帮助您确定哪种奖金最适合您的电子竞技博彩风格。因此，请阅读我们的电子竞技促销指南，以了解如何通过押注自己喜欢的游戏来获得可观的利润增长。</p>
  </div>

  <div class="row">
    {% for key, bonus in bonusitems %}
    {% if key < 4 %}
    <div class="col-xs-12 col-sm-3">
    <div class="modu-block top-betting row">
        <div class="col-xs-6 col-sm-8 logo">
          <a href="{{ bonus.providers.Link }}" rel="nofollow external" target="_blank" class="logo-link">
            <img class="img-responsive" style="background-color:#181617;" alt="{{ bonus.providers.Name }}" src="{{ bonus.providers.localIcon }}" />
          </a>
        </div>
        <div class="col-xs-6 col-sm-4 boxcell rating">
            <div class="value">No.<span>{{bonus.level}}</span></div>
            <div class="rating-stars">
                <div class="rating-stars-active" style="width:{{ bonus.rank*10 }}%"></div>
            </div>
        </div>
        <div class="col-xs-12 highlights">
            <div class="cell-title text-left">{{ bonus.providers.Name }} Bonus</div>
            <h4 class="text-left">{{ bonus.intro }}</h4>
        </div>

        <div class="col-xs-12 bonus">
            <div class="smaller-text legal-advice">T&amp;C Apply, 18 +</div>
        </div>
        <div class="col-xs-12 conversion">
          <a href="{{ bonus.providers.Link }}" class="button fullwidth conv" rel="nofollow external" target="_blank">{{ bonus.providers.Name }}</a>
          <div class="smaller-text review-link">
            <a href="{{ url('/betting/'~bonus.ProviderId) }}">{{ bonus.providers.Name }} Review</a>
          </div>
        </div>
    </div>
    </div>
    {% endif %}
    {% endfor %}
  </div>

</div>

<div class="container">
  <div class="row">
    <h3 class="col-xs-12 text-left">公司推荐</h3>

    <div class="col-xs-12 col-sm-8 col-md-9">

      <div class="row ">
        {% for key, bonus in bonusitems %}
        {% if key > 3 %}
        <div class="col-xs-12">
          <div class="modu-block list-betting">

            <div class="cell list-logo">
              <a class="list-logo-link" href="#">
                <img class="img-responsive"
                     style="background-color:#181617; max-height: 70px;"
                     alt="{{ bonus.providers.Name }}"
                     src="{{ bonus.providers.localIcon }}" />
              </a>
            </div>

            <div class="cell list-rating">
                <div class="value">
                  <span><em>{{ provider.rank }}</em> / </span> 10
                </div>
                <div class="rating-stars">
                    <div class="rating-stars-active" style="width:{{ bonus.providers.rank*10 }}%"></div>
                </div>
            </div>

            <div class="cell list-highlights">
              <div class="cell-title">{{ bonus.providers.Name }}</div>
              {{ bonus.intro }}
            </div>

            <div class="cell list-bonus">
              <div class="bonus-value ">{{ bonus.infomation }}</div>
              <div class="smaller-text legal-advice">T&amp;C Apply, 18 +</div>
            </div>

            <div class="cell list-button">
              <a class="button fullwidth" href="{{ url('/betting/'~bonus.ProviderId) }}">{{ bonus.providers.Name }}</a>
            </div>

          </div>
        </div>
      {% endif %}
      {% endfor %}
       </div>

    </div>

    <div class="col-xs-12 col-sm-4 col-md-3">

      {{ partial('default/rightblock-advertisment') }}

      {{ partial('default/rightblock-news') }}

      {{ partial('default/rightblock-guides') }}

    </div>

  </div>

</div>

</div>