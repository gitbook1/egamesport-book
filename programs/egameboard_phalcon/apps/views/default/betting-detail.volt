<div class="main-content container">

  <div class="row">

  <div class="col-xs-12">
    <div class="provider-headinfo">

        <div class="left-info">
            <div class="inner-grid">
                <div class="logo">
                  <a href="{{provider.Link}}" target="_blank">
                    <img src="{{provider.localIcon}}" alt="{{provider.Name}}">
                  </a>
                </div>

                <div class="provider-data">
                    <div class="rating-box">
                        <div class="value">
                          <span><em>{{provider.rank}}</em>/</span> 100
                          <div class="rating-stars">
                              <div class="rating-stars-active" style="width:98%"></div>
                          </div>
                        </div>
                    </div>
                    <div class="bonus-data">
                        <div class="bonus-title">BUFF.bet Bonus</div>
                        <div class="bonus">100% Deposit, up to €100</div>
                        <div class="legal-advice">T&amp;C Apply, 18 +</div>
                    </div>
                </div>

                <div class="conversion-btn">
                  <a href="{{ provider.Link }}" class="button fullwidth" target="_blank">Go to {{provider.Name}}</a>
                  <a href="{{ url('/betting/'~provider.ProviderId) }}" class="reviewlink">{{provider.Name}} Review</a>
                </div>
            </div>
        </div>

        <div class="detail-info">
          <div class="offered-games">
            <div class="detail-title"> Betting Offer</div>
            <div class="flexible-menu-wrapper games">
              <ul class="flexible-menu">
                {% for key, egame in egames %}
                  <li><span><img alt="{{egame.alias}}" title="{{egame.Icon}}" src="{{egame.Iconpath}}" ></span></li>
                {% endfor %}
              </ul>
            </div>
          </div>
          <div class="sub-line">
              <div class="license">
                  <div class="detail-title">License</div>
                  <ul>
                      <li><img alt="Curacao" title="Curacao" src="https://www.esports.net/wp-content/themes/esports-net/images/licenses/curacao.png"></li>
                  </ul>
              </div>
              <div class="payment">
                  <div class="detail-title">Payment</div>
                  <ul>
                      <li><img alt="Visa" title="Visa" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/visa.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Mastercard" title="Mastercard" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/mastercard.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Bitcoin" title="Bitcoin" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/bitcoin.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Skinpay" title="Skinpay" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/skinpay.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                  </ul>
              </div>
              <div class="countries">
                  <div class="detail-title">Regions Accepted</div>
                  <ul class="inline">
                      <li><img alt="EU" title="EU" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/eu.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Japan" title="Japan" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/japan.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Russia" title="Russia" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/russia.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="China" title="China" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/china.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                  </ul>
              </div>
          </div>
        </div>

    </div>
  </div>

  </div>

</div>



<div class="container">

  <div class="row">
    <div class="col-xs-12 col-sm-8 col-md-9">

      <div class="modu-block modu-bkcolor betting-intro">
        {{ provider.description }}
      </div>

      <nav id="scrollspy-nav" class="navbar navbar-default">
        <ul class="nav nav-pills">
          {% for key, label in providerblocks %}
          <li class="nav-item"><a class="nav-link" href="#{{key}}">{{label}}</a></li>
          {% endfor %}
        </ul>
      </nav>

      <article class="modu-block modu-bkcolor betting-scrollspy" data-spy="scroll" data-target="#scrollspy-nav" data-offset="0">
          {% for key, blockdata in provider.blocks %}
          <h4 id="{{blockdata['id']}}" class="text-left">{{blockdata['label']}}</h4>
          <div class="scrollspy-block">
            {{ blockdata['content'] }}
          </div>
          {% endfor %}

      </article>

    </div>

    <div class="col-xs-12 col-sm-4 col-md-3">

      <div class="right-block">
        <h4 class="right-header text-left">本月熱門促銷</h4>

        <div class="panel modu-block">
          <div class="panel-body text-center">

            <img class="img-responsive" alt="" style="margin: 0 auto;" src="https://via.placeholder.com/240x240" />

          </div>
        </div>

      </div>

      {{ partial('default/rightblock-advertisment') }}


      {{ partial('default/rightblock-news') }}

      {{ partial('default/rightblock-guides') }}


    </div>

  </div>

</div>

</div>