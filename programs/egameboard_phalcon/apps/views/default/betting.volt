<div class="main-content container">

  <div class="row">

    <h1 class="h1 col-xs-12 text-left">查找最佳电子竞技博彩网站</h1>
    <h2 class="h2 col-xs-12 text-left">展示最好的电子竞技博彩公司</h2>
    <p class="text-muted col-xs-10  col-xs-offset-1 col-sm-12 col-sm-offset-0">在过去的五年中，电子竞技的人气激增，诸如CS：GO，Dota 2，英雄联盟，守望先锋，坦克世界，PUBG，Fortnite，炉石传说和风暴英雄等头衔变得家喻户晓。有了几乎永无止境的动作包装锦标赛和联赛清单，许多博彩公司现在提供了供赌客在其上进行电子竞技投注的市场。在这里，我们在Esports.net上对许多最佳的电子竞技博彩网站进行了全面审查。</p>
  </div>

  <div class="col-xs-12">
    <div class="provider-headinfo">

        <div class="left-info">
            <div class="inner-grid">
                <div class="logo">
                  <span class="provider-bedget">1.</span>
                  <a href="{{providers[0].Link}}" target="_blank">
                    <img src="{{providers[0].localIcon}}" alt="{{providers[0].Name}}">
                  </a>
                </div>

                <div class="provider-data">
                    <div class="rating-box">
                        <div class="value">
                          <span><em>{{providers[0].rank}}</em>/</span> 100
                          <div class="rating-stars">
                              <div class="rating-stars-active" style="width:{{providers[0].rank}}%"></div>
                          </div>
                        </div>
                    </div>
                    <div class="bonus-data">
                        <div class="bonus-title">{{providers[0].Name}} Bonus</div>
                        <div class="bonus">{% if providers[0].Bonus.infomation is defined %}{{providers[0].Bonus.infomation}}{% else %}100% Deposit, up to €100{% endif %}</div>
                        <div class="legal-advice">T&amp;C Apply, 18 +</div>
                    </div>
                </div>

                <div class="conversion-btn">
                  <a href="{{ providers[0].Link }}" class="button fullwidth" target="_blank">Go to {{providers[0].Name}}</a>
                  <a href="{{ url('/betting/'~providers[0].ProviderId) }}" class="reviewlink">{{providers[0].Name}} Review</a>
                </div>
            </div>
        </div>

        <div class="detail-info">
          <div class="offered-games">
            <div class="detail-title"> Betting Offer</div>
            <div class="flexible-menu-wrapper games">
              <ul class="flexible-menu">
                {% for key, egame in egames %}
                  <li><span><img alt="{{egame.alias}}" title="{{egame.Icon}}" src="{{egame.Iconpath}}" ></span></li>
                {% endfor %}
              </ul>
            </div>
          </div>
          <div class="sub-line">
              <div class="license">
                  <div class="detail-title">License</div>
                  <ul>
                      <li><img alt="Curacao" title="Curacao" src="https://www.esports.net/wp-content/themes/esports-net/images/licenses/curacao.png"></li>
                  </ul>
              </div>
              <div class="payment">
                  <div class="detail-title">Payment</div>
                  <ul>
                      <li><img alt="Visa" title="Visa" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/visa.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Mastercard" title="Mastercard" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/mastercard.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Bitcoin" title="Bitcoin" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/bitcoin.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Skinpay" title="Skinpay" src="https://www.esports.net/wp-content/themes/esports-net/images/payment/skinpay.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                  </ul>
              </div>
              <div class="countries">
                  <div class="detail-title">Regions Accepted</div>
                  <ul class="inline">
                      <li><img alt="EU" title="EU" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/eu.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Japan" title="Japan" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/japan.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="Russia" title="Russia" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/russia.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                      <li><img alt="China" title="China" src="https://www.esports.net/wp-content/themes/esports-net/images/flags/china.svg" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"></li>
                  </ul>
              </div>
          </div>
        </div>

    </div>
  </div>


</div>



<div class="container">

  <div class="row">
    <div class="col-xs-12 col-sm-8 col-md-9">
      <div class="row">
        <h3 class="col-xs-12 text-left">公司推薦</h3>
        {% set topitems = [1, 2, 3] %}
        {% for key in topitems %}
        <div class="col-xs-12 col-sm-4">
        <div class="modu-block top-betting row">
            <div class="col-xs-6 col-sm-8 logo">
              <a href="{{ providers[key].Link }}" rel="nofollow external" target="_blank" class="logo-link">
                <span class="position">2.</span>
                <img class="img-responsive" style="background-color:#181617;" alt="{{ providers[key].Name }}" src="{{ providers[key].localIcon }}" />
              </a>
            </div>
            <div class="col-xs-6 col-sm-4 boxcell rating">
                <div class="value"><span><em>{{ providers[key].rank }}</em>/</span>100</div>
                <div class="rating-stars">
                    <div class="rating-stars-active" style="width:{{ providers[key].rank }}%"></div>
                </div>
            </div>
            <div class="col-xs-12 highlights">
                <div class="cell-title text-left">{{ providers[key].Name }}</div>
                {% if providers[key].Bonus.intro is defined %}
                {{ providers[key].Bonus.intro }}
                {% else %}
                <ul>
                    <li class="text-left">[固定格式需要實際文字]</li>
                    <li class="text-left">[固定格式需要實際文字]</li>
                </ul>
                {% endif %}
            </div>

            <div class="col-xs-12 bonus">
                <div class="bonus-value ">{% if providers[key].Bonus.infomation is defined %}{{providers[key].Bonus.infomation}}{% else %}[固定格式需要實際文字]{% endif %}</div>
                <div class="smaller-text legal-advice">T&amp;C Apply, 18 +</div>
            </div>
            <div class="col-xs-12 conversion">
              <a href="{{ providers[key].Link }}" class="button fullwidth conv" rel="nofollow external" target="_blank">{{ providers[key].Name }}</a>
              <div class="smaller-text review-link">
                <a href="{{ url('/betting/'~providers[key].ProviderId) }}">{{ providers[key].Name }} Review</a>
              </div>
            </div>
        </div>
        </div>
        {% endfor %}
      </div>


      <div class="row ">
      {% for key, provider in providers %}
      {% if key > 3 %}

        <div class="col-xs-12">
          <div class="modu-block list-betting">

            <div class="cell list-logo">
              <a class="list-logo-link" href="#">
                <img class="img-responsive"
                     style="background-color:#181617; max-height: 70px;"
                     alt="{{ provider.Name }}"
                     src="{{ provider.localIcon }}" />
              </a>
            </div>

            <div class="cell list-rating">
                <div class="value">
                  <span><em>{{ provider.rank }}</em> / </span> 100
                </div>
                <div class="rating-stars">
                    <div class="rating-stars-active" style="width:{{ provider.rank }}%"></div>
                </div>
            </div>

            <div class="cell list-highlights">
              <div class="cell-title">{{ provider.Name }}</div>
              {% if provider.Bonus.intro is defined %}
                {{ provider.Bonus.intro }}
              {% else %}
              <ul>
                  <li class="text-left">[固定格式需要實際文字]</li>
                  <li class="text-left">[固定格式需要實際文字]</li>
              </ul>
              {% endif %}
            </div>

            <div class="cell list-bonus">
              <div class="bonus-value ">{% if provider.Bonus.infomation is defined %}{{provider.Bonus.infomation}}{% else %}[固定格式需要實際文字]{% endif %}</div>
              <div class="smaller-text legal-advice">T&amp;C Apply, 18 +</div>
            </div>

            <div class="cell list-button">
              <a class="button" href="{{ url('/betting/'~provider.ProviderId) }}">{{ provider.Name }}</a>
            </div>

          </div>
        </div>
      {% endif %}
      {% endfor %}
       </div>

    </div>

    <div class="col-xs-12 col-sm-4 col-md-3">

      {{ partial('default/rightblock-advertisment') }}

      {{ partial('default/rightblock-news') }}

      {{ partial('default/rightblock-guides') }}

    </div>

  </div>

</div>

</div>