<div class="main-content">
  <div class="container">
    <div class="row">
      <h1 class="h1 col-xs-12">关于我们</h1>

      <p class="col-xs-12 text-muted">
      魔都赔率（<a href="https://www.moduodds.com/">www.moduodds.com</a>）是一个独立的电竞赔率比较网站，面向亚洲喜爱电竞体育用户服务，提供最快速准确的电竞体育赔率，帮助您找到最佳的电竞赔率，为避免差异和错失赔率，请随时将显示的赔率与实际电竞网站进行比较。我们致力成为业界最专业的电竞赔率比较网站和数据服务商。我们不能保证其百分百的准确性，但我们会不断更新相关竞技赔率数据，以便服务广大互联网电竞体育用户，并为振兴电竞体育贡献一份力量。
      </p>

    </div>
  </div>
</div>


