<div class="modal fade" id="layoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div>

<div class="wrapper">
    <div class="top-bar">
        <div class="container">
            <div class="row">
                <!-- logo -->
                <div class="col-xs-6 col-md-2 logo">
                    <a href="/" alt="{{ config.personalwork.projectname }}">
                        <img src="/misc/img/kingadmin-logo-white.png" alt="{{ config.personalwork.projectname }}" />
                    </a>
                    <h1 class="sr-only">{{ config.personalwork.projectname }}</h1>
                </div>

                <div class="col-xs-6 col-md-10">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="top-bar-right">
                                <!-- 用來維持topbar高度！ -->
                                <a type="button" class="btn btn-link" style="visibility: hidden;"><i
                                        class="fa fa-refresh"></i> 使用說明</a>

                                <div class="logged-user">
                                    <!-- 暫時隱藏個別帳號選單 -->
                                    <div class="btn-group hide">
                                        <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-user fa-2x"></i>
                                            <span class="name">{{ session.get('')}}</span> <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-user"></i>
                                                    <span class="text">個人資訊</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-cog"></i>
                                                    <span class="text">設定</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-power-off"></i>
                                                    <span class="text">登出</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /top-bar-right -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="bottom">
        <div class="container">
            <div class="row">
                <!-- LEFT SIDEBAR -->
                <div id="navLeftside" class="col-sm-3 col-md-2 left-sidebar">
                    {{ partial('partials/block/nav-leftside') }}
                </div>

                <div id="mainContent" class="col-sm-9 col-md-10 content-wrapper">
                    <!-- Alert Block -->
                    {{ partial('partials/block/alert-top') }}

                    <div class="content">

                        {{ content() }}

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
<footer class="footer">
    &copy; 2019 Personalwork.Tw all right reserved.
</footer>
