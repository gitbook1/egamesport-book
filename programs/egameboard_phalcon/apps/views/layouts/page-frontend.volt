<header>
    <nav class="navbar navbar-modu">
    <div class="container index-container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ url('/') }}">
                <img class="img-responsive navbar-brand-modu" src="{{ url('misc/img/moduodds/MODUODDS_LOGO.png') }}" />
            </a>
        </div>

        <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav modu-main-nav">
            <li class="active">
                <a href="{{ url('/betting') }}">公司推荐 <span class="sr-only">(current)</span></a>
            </li>
            <li>
                <a href="{{ url('/news') }}">电竞新闻 <span class="sr-only">(current)</span></a>
            </li>
            <li>
                <a href="{{ url('/bonus') }}">最佳优惠 <span class="sr-only">(current)</span></a>
            <li>
                <a href="{{ url('/guides') }}" >电竞指南 <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div><!-- /.container-fluid -->
    </nav>

</header>

<div class="wrapper text-center">

    <div class="inner-page">
    {{ content() }}
    </div>

</div>

<footer class="footer">
    <div class="row footer-inside">
        <p>2019 ModuOdds - All Right Reserved </p>
        <ul class="col-sm-12 wrap">
            <li><a href="{{ url('/aboutus') }}">关于我们</a></li>
            <li><a href="{{ url('/contactus') }}">联系我们</a></li>
            <li><a href="{{ url('/statements') }}">免责声明</a></li>
        </ul>
    </div>
</footer>