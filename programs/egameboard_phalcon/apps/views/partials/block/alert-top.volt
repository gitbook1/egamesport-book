{% if this.flashSession.getMessages(NULL, FALSE) is not empty and count(this.flashSession.getMessages(NULL, FALSE)) > 0 %}
{{ this.flashSession.output() }}
{% endif %}
<!-- sample -->
<div class="alert alert-danger top-general-alert hide">
    <span>If you <strong>can't see the logo</strong> on the top left, please reset the style on right style switcher (for upgraded theme only).</span>
    <button type="button" class="close">×</button>
</div>
