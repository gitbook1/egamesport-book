{% if count(breadcrumb) %}
<ul class="breadcrumb">
    {% for key, nav in breadcrumb %}
        <li>
        <i class="{{nav['icon']}}"></i>
        {% if key == count(breadcrumb)-1 %}
        <span>{{nav['label']}}</span>
        {% else %}
        <a href="{{nav['url']}}">{{nav['label']}}</a>
        {% endif %}
        </li>
    {% endfor %}
</ul>
{% endif %}