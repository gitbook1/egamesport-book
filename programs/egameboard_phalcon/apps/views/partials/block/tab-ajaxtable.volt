<!--
介面view檔案，只需要配置頁籤or直接表格結構並
呼叫 _tabs_toggle_datatable_ajaxload(); 做dynamtic binding click event
 -->

<ul class="nav nav-tabs block-tabs-ajaxtable">
{% for key, value in tabitems %}
    {% if activetab == value['tabid'] %}
    <li class="active">
    {% else %}
    <li>
    {% endif %}

        <a href="#{{value['tabid']}}" data-toggle="tab" aria-expanded="true"
           data-tableid="#{{value['tableId']}}"
           data-dtconf="{{ url( ajaxTabAction~value['tabid']) }}">
        <i class="fa {{value['faico']}}"></i> {{value['label']}}</a>
    </li>
{% endfor %}
</ul>

<div class="tab-content no-padding">
{% for key, value in tabitems %}
    {% if activetab == value['tabid'] %}
    <div class="tab-pane fade active in" id="{{value['tabid']}}">
    {% else %}
    <div class="tab-pane fade" id="{{value['tabid']}}">
    {% endif %}

        {{ partial('partials/datatable/header-setup-with-php-sample', ['ID':value['tableId'] ,'theads':value['tableHead']]) }}

    </div>
{% endfor %}
</div>

<script>
$(function(){
    // 預設頁籤就觸發載入datatables
    _trigger_datatables_ajaxload( $("ul.block-tabs-ajaxtable > li.active > a") );

    // 常態切tab載入
    _tabs_toggle_datatable_ajaxload();
});
</script>
