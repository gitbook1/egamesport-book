<nav class="main-nav">
<ul class="main-menu">
    <li><a href="{{ url('/') }}" target="_blank">
        <i class="fa fa-home fa-fw"></i><span class="text">前往前台</span>
    </a></li>
    <li><a href="{{ url('/backend') }}">
        <i class="fa fa-dashboard fa-fw"></i><span class="text">後台首頁</span>
    </a></li>
    <li><a href="{{ url('/backend/advertisment') }}">
        <i class="fa fa-cogs fa-fw"></i><span class="text">广告投放管理</span>
    </a></li>
    <li><a href="{{ url('/backend/egames') }}">
        <i class="fa fa-cogs fa-fw"></i><span class="text">电竞游戏管理</span>
    </a></li>
    <li><a href="{{ url('/backend/providers') }}">
        <i class="fa fa-cogs fa-fw"></i><span class="text">公司推荐管理</span>
    </a></li>
    <li><a href="{{ url('/backend/bonus') }}">
        <i class="fa fa-cogs fa-fw"></i><span class="text">最佳优惠管理</span>
    </a></li>
    <li><a href="{{ url('/backend/news') }}">
        <i class="fa fa-cogs fa-fw"></i><span class="text">电竞新闻管理</span>
    </a></li>
    <li><a href="{{ url('/backend/guides') }}">
        <i class="fa fa-cogs fa-fw"></i><span class="text">电竞指南管理</span>
    </a></li>
    <li><a href="/backend/logout"><i class="fa fa-briefcase"></i><span class="text">登出</span></a></li>
</ul>
</nav>