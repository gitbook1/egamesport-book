{{ partial('partials/block/breadcrumb') }}
<h3>{{ pageHead }}</h3>
<section class="data-table table-responsive">
    <table id="dateTable" class="table table-sorting table-hover datatable">
        <thead>
            <!-- act1. 配置標題 -->
            <tr>
                <th>標題一</th>
                <th>標題二</th>
                <th>標題三</th>
            </tr>
        </thead>
    </table>
</section>
<script type="text/javascript">
$(function() {

    TABLE = $("#dateTable");
    // act2. 根據act1.配置對應標題參數
    cols = [{
            "data": "field1",
            "width": "33%",
            "searchable": true,
            "sortable": true
        },
        {
            "data": "field2",
            "width": "33%",
            "searchable": true,
            "sortable": true
        },
        {
            "data": "field3",
            "width": "33%",
            "searchable": true,
            "sortable": true
        },
    ];

    if (!$.fn.DataTable.isDataTable(TABLE)) {
        var dTable = $(TABLE).DataTable({
            destroy: true,
            processing: true,
            serverSide: true,
            ajax: {
                // act3. 設定後端取得data資料來源
                url: "{{ url('urlpath') }}",
                type: "POST"
            },
            order: [
                [1, "desc"]
            ],
            columns: cols
        });
    } else {
        var dTable = $(TABLE).DataTable();
        dTable.ajax.reload();
    }

    // act4. 根據act1.配置對應欄位過濾套件
    yadcf.init(dTable, [
        { column_number: 1, filter_type: "text", filter_delay: 700 },
        {
            column_number: 2,
            column_data_type: "html",
            html_data_type: "text",
            data: [{ value: 'store', label: '購買' }, { value: 'pay', label: '打賞' }, { value: 'assign', label: '指定贈送' }]
        },
        { column_number: 3, filter_type: "text", filter_delay: 900 },
    ]);

});
</script>