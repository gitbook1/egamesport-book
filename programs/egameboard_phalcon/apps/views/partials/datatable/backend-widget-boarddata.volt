<div class="table-responsive container">
  <table id="widgetTable" class="table table-sorting table-hover" width="100%">
  <thead>
    <tr>
      <th id='MatchId'>數據源編號</th>
      <th id='AwayTeam'>客隊</th>
      <th id='CategoryName'>類型</th>
      <th id='HomeTeam'>主隊</th>
      <th id='TournamentId'>關聯競賽平台編號</th>
      <th id='TournamentName'>競賽平台</th>
      <th id='SportId'>關聯電競遊戲編號</th>
      <th id='MatchName'>比賽資訊</th>
      <th id='StartTime'>競賽時間</th>
    </tr>
  </thead>
  <tbody>
  {% for tddata in boarddatas %}
    <tr>
        <td id=''>{{ tddata.MatchId }}</td>
        <td id=''>{{ tddata.AwayTeam }}</td>
        <td id=''>{{ tddata.CategoryName }}</td>
        <td id=''>{{ tddata.HomeTeam }}</td>
        <td id=''>{{ tddata.TournamentId }}</td>
        <td id=''>{{ tddata.TournamentName }}</td>
        <td id=''>{{ tddata.SportId }}</td>
        <td id=''>{{ tddata.MatchName }}</td>
        <td id=''>{{ tddata.StartTime }}</td>
    </tr>
    {% endfor %}
  </tbody>
  </table>
</div>


<script type="text/javascript">
$(function(){
    $( "#widgetTable" ).DataTable({
      pageLength: 20,
      lengthChange: false
    });
});
</script>