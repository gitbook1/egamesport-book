{{ tag.getDoctype() }}
<!--[if IE 9 ]>
<html class="ie ie9" lang="en" class="no-js"> <![endif]-->
<!--[if !(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="keyword" content="魔都赔率, Modu赔率, 电竞, 电竞赔率, 赔率比较, 电子竞投, 最新电竞赔率" />
        <meta name="description" content="魔都赔率, Modu赔率, 电竞, 电竞赔率, 赔率比较, 电子竞投, 最新电竞赔率" />
        <meta name="author" content="San Huang <san@perosnalwork.tw>">
        <title>魔都电竞赔率ModuOdds, 涵盖CSGO、英雄联盟、Dota2、王者荣室耀等多种等级电竞游戏。</title>

        {{ assets.outputCss('headerStyle') }}

        <!--[if lte IE 9]>
        <link href="misc/css/main-ie.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="misc/css/main-ie-part2.css" rel="stylesheet" type="text/css" media="screen" />
        <![endif]-->

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/misc/ico/kingadmin-favicon144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/misc/ico/kingadmin-favicon114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/misc/ico/kingadmin-favicon72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/misc/ico/kingadmin-favicon57x57.png" />
        <link rel="shortcut icon" href="/misc/ico/favicon.png" />

        {{ assets.outputJs('headerScript') }}

    </head>
    <body class="dashboard2 topnav-fixed">
        {{ content() }}
        {{ assets.outputJs('footerScript') }}
    </body>
    {{ assets.outputJs('footerScript') }}
</html>
