<div class="page-header text-center">
    <a href="/backend"><img src="/misc/img/kingadmin-logo.png" alt="" /></a>
</div>

<!-- top general alert -->
{{ partial('partials/block/alert-top') }}
<!-- end top general alert -->

<div class="inner-page">

    <div class="login-box center-block">
        <form class="form-horizontal" role="form" method="post" action="">
            <p class="title">登入系統</p>
            <div class="form-group">
                <label for="username" class="control-label sr-only">帳號</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="text" placeholder="帳號" id="account" name="account" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div>
            </div>
            <label for="password" class="control-label sr-only">密碼</label>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="password" placeholder="密碼" id="password" name="password" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    </div>
                </div>
            </div>
            <button class="btn btn-custom-primary btn-lg btn-block btn-login"><i class="fa fa-arrow-circle-o-right"></i> 登入系統</button>
        </form>

        <div class="links">
            <span> <a href="/backend/forgetps">忘記密碼?</a> </span>
        </div>
    </div>
</div>
