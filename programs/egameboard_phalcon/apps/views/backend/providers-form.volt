{{ partial('partials/block/nav-breadcrumb') }}

<div class="widget">
  <div class="widget-header">
    <h3><i class="fa fa-edit"></i> 建立平台商資訊</h3>
  </div>
  <div class="widget-content">
  <form class="form-horizontal label-left" role="form"
        method="POST" action="{{ url('/backend/providers/save') }}"
        enctype="multipart/form-data"
  >
  {% if formData.ProviderId is not empty %}
  <input type="hidden" name="ProviderId" value="{{ formData.ProviderId }}" />
  {% endif %}

        <div class="form-group">
          <label for="Name" class="col-sm-3 control-label">平台商名稱</label>
          <div class="col-sm-9">
            <input type="text" id="Name" name="Name" class="form-control" value="{{ formData.Name }}" />
          </div>
        </div>
        <div class="form-group">
          <label for="Link" class="col-sm-3 control-label">資訊連結</label>
          <div class="col-sm-9">
            <input type="text" id="Link" name="Link" class="form-control" value="{{ formData.Link }}" />
          </div>
        </div>
        <div class="form-group">
          <label for="localIcon" class="col-sm-3 control-label">平台商圖標</label>
          <div class="col-sm-9">

            {% if formData.localIcon is not empty %}
            <img class="img-responsive img-thumb" src="{{ formData.localIcon }}" />
            {% elseif formData.Icon is not empty %}
            <img class="img-responsive img-thumb" src="{{ formData.Icon }}" />
            {% endif %}

            <input type="file" id="localIcon" name="localIcon" class="form-control" accept="image/*" />
            <p class="help-block"><em>Valid file type: .jpg, .gif, .png. File size max: 5MB</em></p>
          </div>
        </div>
        <div class="form-group">
          <label for="rank" class="col-sm-3 control-label">評分值</label>
          <div class="col-sm-3">
              <input type="text" class="bootstrap-slider" id="rank" name="rank" value="{{ formData.rank }}" />
          </div>
          <div class="col-sm-2">
            <span class="label label-warning label-slider">{{ formData.rank }}</span>
          </div>
        </div>
        <div class="form-group">
          <label for="tax-id" class="col-sm-3 control-label">是否顯示</label>
          <div class="col-sm-9">
            <label for="IsVisible_0" class="control-inline fancy-radio">
              <input type="radio" id="IsVisible_0" name="IsVisible" value="0" {% if formData.IsVisible == 0 %}checked{% endif %} />
              <span><i></i>否</span>
            </label>
            <label for="IsVisible_1" class="control-inline fancy-radio">
              <input type="radio" id="IsVisible_1" name="IsVisible" value="1" {% if formData.IsVisible != 0 or formData.IsVisible is not defined%}checked{% endif %} />
              <span><i></i>是</span>
            </label>
          </div>
        </div>
        <div class="form-group">
          <label for="tax-id" class="col-sm-3 control-label">是否啟用</label>
          <div class="col-sm-9">
            <label for="IsEnabled_0" class="control-inline fancy-radio">
              <input type="radio" id="IsEnabled_0" name="IsEnabled" value="0" {% if formData.IsEnabled == 0 %}checked{% endif %} />
              <span><i></i>否</span>
            </label>
            <label for="IsEnabled_1" class="control-inline fancy-radio">
              <input type="radio" id="IsEnabled_1" name="IsEnabled" value="1" {% if formData.IsEnabled == 1 or formData.IsEnabled is not defined %}checked{% endif %} />
              <span><i></i>是</span>
            </label>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            置頂區塊固定內容 <em></em>
          </div>
          <div class="widget-content">

            <div class="form-group">
              <label for="tax-id" class="col-sm-3 control-label">授權</label>
              <div class="col-sm-9">
                {% for label, value in LICENSES %}
                <label class="checkbox-inline" for="lisenses_{{label}}">
                    {% if formData.lisenses is defined  %}
                      {% if label in formData.lisenses %}
                    <input checked type="checkbox" id="lisenses_{{label}}" name="lisenses[]" value="{{label}}" />
                      {% endif %}
                    {% else %}
                    <input type="checkbox" id="lisenses_{{label}}" name="lisenses[]" value="{{label}}" />
                    {% endif %}

                    <img class="img-responsive" src="{{value}}" alt="{{label}}" style="height:1em; display: inline;" />
                    {{label}}
                </label>
                {% endfor %}
              </div>
            </div>

            <div class="form-group">
              <label for="tax-id" class="col-sm-3 control-label">支付方式</label>
              <div class="col-sm-9">
                {% for label, value in PAYMENTS %}
                <label class="checkbox-inline" for="payments_{{label}}">
                    {% if formData.payments is defined  %}
                      {% if label in formData.payments %}
                    <input checked type="checkbox" id="payments_{{label}}" name="payments[]" value="{{label}}" />
                      {% endif %}
                    {% else %}
                    <input type="checkbox" id="payments_{{label}}" name="payments[]" value="{{label}}" />
                    {% endif %}

                    <img class="img-responsive" src="{{value}}" alt="{{label}}" style="height:1em; display: inline;" />
                    {{label}}
                </label>
                {% endfor %}
              </div>
            </div>

            <div class="form-group">
              <label for="tax-id" class="col-sm-3 control-label">接受國家</label>
              <div class="col-sm-9">
                {% for label, value in REGIONS %}
                <label class="checkbox-inline" for="regions_{{label}}">
                    {% if formData.regions is defined  %}
                      {% if label in formData.regions %}
                    <input checked type="checkbox" id="regions_{{label}}" name="regions[]" value="{{label}}" />
                      {% endif %}
                    {% else %}
                    <input type="checkbox" id="regions_{{label}}" name="regions[]" value="{{label}}">
                    {% endif %}
                    <img class="img-responsive" src="{{value}}" alt="{{label}}" style="height:1em; display: inline;" />
                    {{label}}
                </label>
                {% endfor %}
              </div>
            </div>

          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            公司介紹 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="description" id="description" value="">
            <div id="summernote_desc" class="summernote" data-target="description">
            {%if formData.description is not empty %}{{ formData.description }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>


        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            網站設計區塊 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="BLOCK_0" id="BLOCK_0" value="">
            <div id="summernote_0" class="summernote" data-target="BLOCK_0">
            {%if formData.BLOCK_0 is not empty %}{{ formData.BLOCK_0 }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            優惠紅利區塊 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="BLOCK_1" id="BLOCK_1" value="">
            <div id="summernote_1" class="summernote" data-target="BLOCK_1">
            {%if formData.BLOCK_1 is not empty %}{{ formData.BLOCK_1 }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            支付方式區塊 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="BLOCK_2" id="BLOCK_2" value="">
            <div id="summernote_2" class="summernote" data-target="BLOCK_2">
            {%if formData.BLOCK_2 is not empty %}{{ formData.BLOCK_2 }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            存提時間區塊 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="BLOCK_3" id="BLOCK_3" value="">
            <div id="summernote_3" class="summernote" data-target="BLOCK_3">
            {%if formData.BLOCK_3 is not empty %}{{ formData.BLOCK_3 }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            客戶服務區塊 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="BLOCK_4" id="BLOCK_4" value="">
            <div id="summernote_4" class="summernote" data-target="BLOCK_4">
            {%if formData.BLOCK_4 is not empty  %}{{ formData.BLOCK_4 }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            監管牌照區塊 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="BLOCK_5" id="BLOCK_5" value="">
            <div id="summernote_5" class="summernote" data-target="BLOCK_5">
            {%if formData.BLOCK_5 is not empty %}{{ formData.BLOCK_5 }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>

        <div class="widget">
          <div class="widget-header">
            <h3><i class="fa fa-bar-chart-o"></i></h3>
            用戶體驗區塊 <em></em>
          </div>
          <div class="widget-content no-padding">
            <input type="hidden" name="BLOCK_5" id="BLOCK_6" value="">
            <div id="summernote_6" class="summernote" data-target="BLOCK_6">
            {%if formData.BLOCK_6 is not empty %}{{ formData.BLOCK_6 }}{%else%}<p>在此编辑内容</p>{%endif%}
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary btn-block">儲存</button>
          </div>
        </div>
  </form>
  </div>
</div>


<script type="text/javascript">
$(function(){
  var sliderChanged = function() {
		$('.label-slider').text( theSlider.getValue() );
	};

  var theSlider = $('.bootstrap-slider').slider({
      min: 0,
      max: 100,
      value: {{ formData.rank|default(0) }},
      tooltip: 'hide',
      handle: 'square'
  }).on('slide', sliderChanged).data('slider');

  $('.label-slider').text( theSlider.getValue() );

  $('.summernote').summernote({
    height: 300,
    focus: true,
    onpaste: function() {
      // alert('You have pasted something to the editor');
    }
  });

  $("form").submit(function(){
    $('.summernote').each(function(){
      var id = $(this).data('target');
      var markupStr = $(this).summernote('code');
      $("#"+id).prop('value', markupStr);
    });
    // return false;
    // console.log( markupStr );
    // $("#content")
  });
});
</script>