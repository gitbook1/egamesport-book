<div class="inner-page">

    <div class="logo">
        <a href="/"><img src="/misc/img/kingadmin-logo.png" alt="" /></a>
    </div>

    <div class="login-box center-block">
        <form class="form-horizontal" role="form" method="post" action="">
            <p class="title">忘記密碼，輸入帳號與郵件接收新的密碼</p>
            <div class="form-group">
                <label for="username" class="control-label sr-only">帳號</label>
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="text" placeholder="帳號" id="account" name="account" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div>
            </div>
            <label for="password" class="control-label sr-only">郵件信箱</label>
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="input-group">
                        <input type="email" placeholder="註冊的電子郵件" id="email" name="email" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-email"></i></span>
                    </div>
                </div>
            </div>
            <button class="btn btn-custom-primary btn-lg btn-block btn-login"><i class="fa fa-arrow-circle-o-right"></i> 重設密碼</button>
        </form>

        <div class="links">
            <span> <a href="/login">返回登入畫面</a> </span>
        </div>
    </div>
</div>
