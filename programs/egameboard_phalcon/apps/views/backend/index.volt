<div class="row">

  <section class="col-sm-6">
  <div class="widget">
    <div class="widget-header">
      <h3>設定聯繫我們接收郵件</h3>
    </div>
    <div class="widget-content">

      <form id="configForm" class="form-horizontal" >
        <div class="form-group">
          <label class="col-md-2 control-label">郵件信箱</label>
          <div class="col-md-10">
            <textarea class="form-control" placeholder="使用逗號分隔可加入多組" rows="4" id="contactemail" name="contactemail">{% if email is not empty %}{{email.value}}{% endif %}</textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-offset-2 col-md-10">
            <button type="button" class="btn btn-primary opt-config">設定</button>
          </div>
        </div>
      </form>

    </div>
  </div>
  </section>

  <section class="col-sm-6">
  <div class="widget">
    <div class="widget-header">
      <h3>電競數據統計資訊</h3>
    </div>
    <div class="widget-content">

      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>更新數據量</th>
              <th>時間點</th>
            </tr>
          </thead>
          <tbody>
      {% for key, data in datacount %}
            <tr>
              <td>{{ key+1 }}</td>
              <td>{{data.rowcount}}</td>
              <td>{{date('H:i - m月 d, Y', data.dateTime)}}</td>
            </tr>
      {% endfor %}
          </tbody>
        </table>
      </div>
    </div>
  </div>
  </section>

  <form class="col-sm-12" id="categoryForm" >
  <div class="widget">
    <div class="widget-header">
      <h3>新聞與指南標籤</h3>
      <div class="widget-header-toolbar">
        <button type="button" class="btn btn-primary opt-delete">刪除所選分類</button>
      </div>
    </div>
    <div class="widget-content">

      {% for key, category in categories %}
        <button id="btn_{{ category.Id }}" type="button" class="btn btn-sm btn-info">
            <input type="checkbox" autocomplete="off" name="category[]" id="category_{{ category.Id }}" value="{{ category.Id }}" {% if tddata["isTop"] == 1 %}checked{% endif %} />
            <label for="category_{{ category.Id }}">{{category.label}}</label>
        </button>
      {% endfor %}

    </div>
    <div class="widget-footer">
    </div>
  </div>
  </form>


  <section class="col-sm-12">
  <div class="widget">
    <div class="widget-header">
      <h3>電競數據列表</h3>
    </div>
    <div class="widget-content">

      {{ partial('partials/datatable/backend-widget-boarddata') }}

    </div>
    <div class="widget-footer">
    </div>
  </div>
  </section>

</div>

<script type="text/javascript">
$(function(){
  $(".opt-delete").click(function(){

    if( confirm('確定要刪除勾選的分類標籤？(一但刪除標籤將無法恢復)') ){
      $.ajax({
        url: '{{ url('backend/categoryDelete') }}',
        data: $("#categoryForm").serialize(),
        dataType: 'json',
        type: 'POST',
        beforeSend: function(){
          $(this).prop('disabled', true);
        },
        success: function( resp ){
          if( resp.code == 'OK' ){
            alert('已刪除所選分類');
            resp.ids.map(function(elm){
              $(elm).remove();
            });
          }else{
            alert(resp.msg);
          }
        },
        complete: function( ){
          $(this).prop('disabled', false);
        }
      });
    }
  });

  $(".opt-config").click(function(){
    $.ajax({
      url: '{{ url('backend/configSave') }}',
      data: $("#configForm").serialize(),
      dataType: 'json',
      type: 'POST',
      beforeSend: function(){
        $(this).prop('disabled', true);
      },
      success: function( resp ){
        alert(resp.msg);
      },
      complete: function( ){
        $(this).prop('disabled', false);
      }
    });
  });
});
</script>