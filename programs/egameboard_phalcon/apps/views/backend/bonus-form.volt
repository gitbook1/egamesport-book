{{ partial('partials/block/nav-breadcrumb') }}

<div class="widget">
  <div class="widget-header">
    <h3><i class="fa fa-edit"></i> 新增表單</h3>
  </div>
  <div class="widget-content">

    <form class="form-horizontal label-left" role="form"
        method="POST" action="{{ url('/backend/bonus/save') }}"
    >
    {% if formData.Id is not empty %}
    <input type="hidden" name="Id" value="{{ formData.Id }}" />
    {% endif %}

      <div class="form-group">
        <label for="Name" class="col-sm-3 control-label">平台商名稱</label>
        <div class="col-sm-9">
          <select class="form-control" id="ProviderId" name="ProviderId">
            {% if formData.ProviderId is not defined %}
            <option value="">請先選擇</option>
            {% endif %}
            {% for key, provider in Providers %}
            <option value="{{provider['ProviderId']}}" {% if formData.ProviderId == provider['ProviderId']%}selected{% endif %}>{{provider['Name']}}</option>
            {% endfor %}
          </select>
          <p class="help-block">
            {% if formData.ProviderId is defined %}
              {% if formData.Providers.localIcon is defined %}
              <img class="img-responsive img-thumbnail" src="{{ formData.Providers.localIcon }}" style="max-width: 300px;" />
              {% elseif formData.Providers.Icon is defined %}
              <img class="img-responsive img-thumbnail" src="{{ formData.Providers.Icon }}" style="max-width: 300px;" />
              {% else %}
              No Icon
              {% endif %}
            {% endif %}
          </p>
        </div>
      </div>
      <!-- 檢視資訊(Icon/介紹網址) -->

      <div class="form-group">
        <label for="level" class="col-sm-3 control-label">設定優惠顯示名次</label>
        <div class="col-sm-9">
          <input type="text" id="level" name="level" class="form-control" value="{{ formData.level }}" />
        </div>
      </div>

      <div class="form-group">
          <label for="rank" class="col-sm-3 control-label">評分值</label>
          <div class="col-sm-3">
              <input type="text" class="bootstrap-slider" id="rank" name="rank" value="{{ formData.rank }}" />
          </div>
          <div class="col-sm-2">
            <span class="label label-warning label-slider">{{ formData.rank }}</span>
          </div>
      </div>

      <!-- <div class="form-group">
        <label for="intro" class="col-sm-3 control-label">優惠資訊</label>
        <div class="col-sm-9">
          <textarea class="form-control textarea-with-counter" id="intro" name="intro"
              rows="6" cols="30" maxlength="99">{{ formData.intro }}</textarea>
        </div>
      </div> -->

      <div class="widget">
        <div class="widget-header">
          <h3><i class="fa fa-bar-chart-o"></i></h3>
          優惠資訊 <em></em>
        </div>
        <div class="widget-content no-padding">
          <input type="hidden" name="intro" id="intro" value="">
          <div id="summernote_5" class="summernote" data-target="intro">
          {%if formData.intro is not empty %}{{ formData.intro }}{%else%}<p>在此编辑内容</p>{%endif%}
          </div>
        </div>
      </div>

      <div class="form-group">
        <label for="intro" class="col-sm-3 control-label">框線強調訊息</label>
        <div class="col-sm-9">
          <input type="text" id="infomation" name="infomation" class="form-control" value="{{ formData.infomation }}" placeholder="" />
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <button type="submit" class="btn btn-primary btn-block">儲存</button>
        </div>
      </div>

    </form>

  </div>
</div>


<script type="text/javascript">
$(function(){
  var sliderChanged = function() {
      $('.label-slider').text( theSlider.getValue() );
    }

  var theSlider = $('.bootstrap-slider')
    .slider({
      min: 0,
      max: 10,
      value: {{ formData.rank|default(0) }},
      tooltip: 'hide',
      handle: 'square'
    }).on('slide', sliderChanged).data('slider');

  $('.label-slider').text( theSlider.getValue() );

  $("#ProviderId").change(function(){
    var providerId = $(this).val();
    $.ajax({
        url: '{{ url('backend/bonus/loadProvider') }}',
        type: 'POST',
        data: {
            id: providerId
        },
        dataType: 'html',
        beforeSend: function(){
            // loading
            $(".help-block").empty();
            $("button").prop('disabled', true);
        },
        success: function( resp ){
          $(".help-block").html(resp);
        },
        error: function(){
        },
        complete: function(){
            // remove loading
            $("button").prop('disabled', false);
        }
    });
  });

  $('.summernote').summernote({
    height: 300,
    focus: true,
    onpaste: function() {
      // alert('You have pasted something to the editor');
    }
  });

  $("form").submit(function(){
    $('.summernote').each(function(){
      var id = $(this).data('target');
      var markupStr = $(this).summernote('code');
      $("#"+id).prop('value', markupStr);
    });
    // return false;
    // console.log( markupStr );
    // $("#content")
  });

});
</script>