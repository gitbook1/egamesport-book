{{ partial('partials/block/nav-breadcrumb') }}

<div class="widget">
  <div class="widget-header">
    <h3><i class="fa fa-edit"></i> 广告投放表单</h3>
  </div>
  <div class="widget-content">

    <form class="form-horizontal label-left" role="form"
        method="POST" action="{{ url('/backend/advertisment/save') }}"
        enctype="multipart/form-data"
    >
    {% if formData.Id is not empty %}
    <input type="hidden" name="Id" value="{{ formData.Id }}" />
    {% endif %}

      <div class="form-group">
        <label for="infomation" class="col-sm-3 control-label">投放位置</label>
        <div class="col-sm-9">
          <select id="position" name="position" class="form-control">
            {% for key, positionlabel in positions %}
              {% if formData.position == key %}
              <option value="{{key}}" selected>{{positionlabel}}</option>
              {% else %}
              <option value="{{key}}">{{positionlabel}}</option>
              {% endif %}
            {% endfor %}
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="link" class="col-sm-2 control-label">广告连结</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="link" name="link" placeholder="请输入网址" value="{{ formData.link }}" />
        </div>
      </div>

      <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">广告图片</label>
        <div class="col-sm-10">

          {% if formData.photo is not empty %}
          <img class="img-responsive img-thumb" src="{{ formData.photo }}" />
          {% endif %}

          <input type="file" id="photo" name="photo" class="form-control" accept="image/*" />
          <p class="help-block"><em>Valid file type: .jpg, .gif, .png. File size max: 5MB</em></p>
        </div>
      </div>

      <div class="form-group">
        <label for="intro" class="col-sm-3 control-label">介绍文字</label>
        <div class="col-sm-9">
          <input type="text" id="intro" name="intro" class="form-control" value="{{ formData.intro }}" />
        </div>
      </div>


      <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary btn-block">储存</button>
          </div>
        </div>

    </form>

  </div>
</div>


<script type="text/javascript">
$(function(){
});
</script>