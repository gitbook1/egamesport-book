<div class="widget">
  <div class="widget-header">
    <h3><i class="fa fa-edit"></i> 电竞新闻表單</h3>
  </div>
  <div class="widget-content">

    <form class="form-horizontal label-left" role="form"
          method="POST" action="{{ url('/backend/news/save') }}"
          enctype="multipart/form-data"
    >
    {% if formData.Id is not empty %}
    <input type="hidden" name="Id" value="{{ formData.Id }}" />

      <fieldset style="border: 2px solid #EEE;">
        <legend>設定屬性</legend>
        <div class="form-group">
        <label for="isTop_1" class="col-sm-2 control-label">是否置頂</label>
        <div class="col-sm-4">
          <label class="control-inline fancy-radio">
            <input type="radio" name="isTop" id="isTop_1" value="1" {% if formData.isTop == 1 %}checked{% endif %} />
            <span><i></i>是</span>
          </label>
          <label class="control-inline fancy-radio">
            <input type="radio" name="isTop" id="isTop_0" value="0" {% if formData.isTop == 0 %}checked{% endif %} />
            <span><i></i>否</span>
          </label>
        </div>

        <label for="statecode" class="col-sm-2 control-label">消息狀態</label>
        <div class="col-sm-4">
        <select id="statecode" name="statecode" class="form-control">
          {% for key, statelabel in statelabels %}
            {% if formData.statecode == key %}
            <option value="{{key}}" selected>{{statelabel}}</option>
            {% else %}
            <option value="{{key}}">{{statelabel}}</option>
            {% endif %}
          {% endfor %}
        </select>
        </div>
        </div>
      </fieldset>

    {% endif %}

      <div class="form-group">
        <label for="title" class="col-sm-2 control-label">电竞新闻標題</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="title" name="title" placeholder="电竞新闻" value="{{ formData.title }}" />
        </div>
      </div>

      <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">电竞新闻圖片</label>
        <div class="col-sm-10">

          {% if formData.photo is not empty %}
          <img class="img-responsive img-thumb" src="{{ formData.photo }}" />
          {% endif %}

          <input type="file" id="photo" name="photo" class="form-control" accept="image/*" />
          <p class="help-block"><em>Valid file type: .jpg, .gif, .png. File size max: 5MB</em></p>
        </div>
      </div>

      <div class="form-group">
        <label for="summary" class="control-label sr-only">摘要</label>
        <div class="col-sm-12">
          <textarea class="form-control" rows="3" id="summary" name="summary" placeholder="摘要">{{ formData.summary }}</textarea>
        </div>
      </div>

      <div class="widget">
        <div class="widget-header">
          <h3><i class="fa fa-edit"></i>电竞新闻內容</h3>
        </div>
        <div class="widget-content no-padding">
          <input type="hidden" id="content" name="content" value="" />
          <div id="summernote" class="summernote">
          {%if formData.content %}{{ formData.content }}{%else%}<p>在此编辑内容</p>{%endif%}
          </div>
        </div>
      </div>

      <div class="widget">
        <div class="widget-header">
          <h3><i class="fa fa-bar-chart-o"></i></h3> 設定分類 <em>desc</em>
        </div>
        <div class="widget-content">

          <select id="select2" name="categories[]" class="form-control" multiple>
            {% for item in categories %}
              {% if item['selected'] is defined  %}
                <option value="{{item['label']}}" selected>{{item['label']}}</option>
              {% else %}
                <option value="{{item['label']}}">{{item['label']}}</option>
              {% endif %}
            {% endfor %}
          </select>

        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-block opt-save">儲存</button>
        </div>
      </div>

    </form>

  </div>
</div>


<script type="text/javascript">
$(function(){
  $("#select2").select2({
    tags: true
  });

  /* summernote */
  $('.summernote').summernote({
    height: 300,
    focus: true,
    onpaste: function() {
      // alert('You have pasted something to the editor');
    }
  });

  $("form").submit(function(){
    var markupStr = $('#summernote').summernote('code');
    // console.log( markupStr );
    $("#content").prop('value', markupStr);
  });
});
</script>