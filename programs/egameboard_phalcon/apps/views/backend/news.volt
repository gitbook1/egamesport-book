{{ partial('partials/block/nav-breadcrumb') }}

<div class="table-responsive container">
<br>
<table id="{{ID}}" class="table table-sorting table-hover" width="100%">
    <thead>
        <tr>
        {% for th in theads %}
        <th class="{{th['classsets']}}" width="{{th['width']}}">{{th['label']}}</th>
        {% endfor %}
        </tr>
    </thead>
    <tbody>
        {% for tddata in tabledatas %}
        <tr>
            {% for th in theads %}
            {% if tddata[th['datakey']] is defined %}
            <td>{{ tddata[th['datakey']] }}</td>
            {% endif %}
            {% endfor %}
            <td>
                <!-- <button class="btn btn-sm btn-info opt-istop" data-id="{{ tddata["pId"] }}">
                    <input type="checkbox" autocomplete="off" name="isTop" id="istop_{{ tddata["pId"] }}" value="1" {% if tddata["isTop"] == 1 %}checked{% endif %} />
                    <label for="istop_{{ tddata["pId"] }}">置頂</label>
                </button> -->
                <button class="btn btn-sm btn-primary opt-edit" data-id="{{ tddata["pId"] }}">編輯</button>
                <button class="btn btn-sm btn-danger opt-delete" data-id="{{ tddata["pId"] }}">刪除</button>
            </td>
        </tr>
        {% endfor %}
    </tbody>
</table>
</div>


<script type="text/javascript">
$(function(){
    $( "#{{ID}}"  ).DataTable({
        dom : "<'row'<'col-sm-6'B><'col-sm-6'f>>" +
              "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        buttons: {
            buttons: [
                {
                    name: 'create',
                    className: 'btn btn-success',
                    text: '新增項目',
                    action: function(){
                        window.location = window.location.href+'/form';
                    }
                }
            ]
        },
        scrollY: 500,
        paging: false,
    });

    //@todo 設定置頂(放在編輯頁面處理！)

    //編輯表單
    $(document).on('click', ".opt-edit", function(e){
        window.location = window.location.href+'/form/'+$(this).data('id');
    });

    //刪除機制
    $(document).on('click', ".opt-delete", function(e){
        if( confirm('確認從資料庫內刪除該筆數據？') ){
            onclickTr = $(this).parents('tr');

            $.ajax({
                url: window.location.href+'/delete',
                type: 'POST',
                data: {
                    id: $(this).data('id')
                },
                dataType: 'json',
                beforeSend: function(){
                    // loading
                    $("button").prop('disabled', true);
                },
                success: function( resp ){
                    // DataTable get this row remove
                    $( "#{{ID}}"  ).DataTable()
                    .row( onclickTr )
                    .remove()
                    .draw();
                },
                error: function(){

                },
                complete: function(){
                    // remove loading
                    $("button").prop('disabled', false);
                }
            });
        }
    });
});
</script>
