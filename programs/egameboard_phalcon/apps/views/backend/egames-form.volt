{{ partial('partials/block/nav-breadcrumb') }}

<div class="widget">
  <div class="widget-header">
    <h3><i class="fa fa-edit"></i> 电竞游戏表单</h3>
  </div>
  <div class="widget-content">

    <form class="form-horizontal label-left" role="form"
        method="POST" action="{{ url('/backend/egames/save') }}"
        enctype="multipart/form-data"
    >
    {% if formData.Id is not empty %}
    <input type="hidden" name="Id" value="{{ formData.Id }}" />
    {% endif %}

      <div class="form-group">
        <label for="infomation" class="col-sm-3 control-label">電競遊戲</label>
        <div class="col-sm-9">
          <input type="text" id="Name" name="Name" class="form-control" value="{{ formData.Name }}" />
        </div>
      </div>

      <div class="form-group">
        <label for="Iconpath" class="col-sm-2 control-label">遊戲圖示</label>
        <div class="col-sm-10">

          {% if formData.Iconpath is not empty %}
          <img class="img-responsive img-thumb" src="{{ formData.Iconpath }}" />
          {% endif %}

          <input type="file" id="Iconpath" name="Iconpath" class="form-control" accept="image/*" />
          <p class="help-block"><em>Valid file type: .jpg, .gif, .png. File size max: 5MB</em></p>
        </div>
      </div>

      <div class="form-group">
        <label for="alias" class="col-sm-3 control-label">遊戲顯示名稱</label>
        <div class="col-sm-9">
          <input type="text" id="alias" name="alias" class="form-control" value="{{ formData.alias }}" />
        </div>
      </div>


      <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary btn-block">储存</button>
          </div>
        </div>

    </form>

  </div>
</div>


<script type="text/javascript">
$(function(){
});
</script>