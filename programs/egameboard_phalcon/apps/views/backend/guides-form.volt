{{ partial('partials/block/nav-breadcrumb') }}

<div class="widget">
  <div class="widget-header">
    <h3><i class="fa fa-edit"></i> 遊戲指南表單</h3>
  </div>
  <div class="widget-content">

    <form class="form-horizontal label-left" role="form"
        method="POST" action="{{ url('/backend/guides/save') }}"
        enctype="multipart/form-data"
    >
    {% if formData.Id is not empty %}
    <input type="hidden" name="Id" value="{{ formData.Id }}" />
    {% endif %}
      <div class="form-group">
        <label for="title" class="control-label sr-only">游戏指南标题</label>
        <div class="col-sm-12">
          <input type="text" class="form-control" id="title" name="title" placeholder="游戏指南标题" value="{{ formData.title }}" />
        </div>
      </div>

      <div class="form-group">
        <label for="photo" class="col-sm-2 control-label">游戏指南圖片</label>
        <div class="col-sm-10">

          {% if formData.photo is not empty %}
          <img class="img-responsive img-thumb" src="{{ formData.photo }}" />
          {% endif %}

          <input type="file" id="photo" name="photo" class="form-control" accept="image/*" />
          <p class="help-block"><em>Valid file type: .jpg, .gif, .png. File size max: 5MB</em></p>
        </div>
      </div>

      <div class="form-group">
        <label for="summary" class="control-label sr-only">摘要</label>
        <div class="col-sm-12">
          <textarea class="form-control" rows="3" id="summary" name="summary" placeholder="摘要">{{ formData.summary }}</textarea>
        </div>
      </div>

      <div class="widget">
        <div class="widget-header">
          <h3><i class="fa fa-edit"></i>游戏指南内容</h3>
        </div>
        <div class="widget-content no-padding">
          <input type="hidden" name="content" id="content" value="">
          <div id="summernote" class="summernote">
          {%if formData.content %}{{ formData.content }}{%else%}<p>在此编辑内容</p>{%endif%}
          </div>
        </div>
      </div>

      <div class="form-group">
        <div class="col-sm-12">
          <button type="submit" class="btn btn-primary btn-block">儲存</button>
        </div>
      </div>

    </form>

  </div>
</div>


<script type="text/javascript">
$(function(){
  $("#select2").select2({
    tags: true
  });

  /* summernote */
  $('.summernote').summernote({
    height: 300,
    focus: true,
    onpaste: function() {
      // alert('You have pasted something to the editor');
    }
  });

  $("form").submit(function(){
    var markupStr = $('#summernote').summernote('code');
    // console.log( markupStr );
    $("#content").prop('value', markupStr);
  });
});
</script>