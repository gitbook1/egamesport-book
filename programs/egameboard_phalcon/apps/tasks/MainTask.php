<?php
use Personalwork\Logger\Adapter\Database as Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;

class MainTask extends \Phalcon\CLI\Task
{
  protected $_agents = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:71.0) Gecko/20100101 Firefox/71.0'
  ];


  /**
   * Usage : php cli.php main test aa bb
   *
   * @param array $params
   * @return void
   */
  public function testAction(array $params) {
      echo sprintf('hello %s', $params[0]) . PHP_EOL;
      echo sprintf('best regards, %s', $params[1]) . PHP_EOL;
  }



  /**
   * Usage : php cli.php main fetch
   *
   * @param array $params
   * @return void
   */
  public function fetchAction(array $params) {

    $target = $this->config->egame->{"apiurl_$params[0]"};
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $target);
    curl_setopt($curl, CURLOPT_USERAGENT, $this->_agents[0]);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('User-Agent: '. $this->_agents[0], 'Referer: https://oddsloot.com/en', 'Content-Type: application/json'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $jsondata = curl_exec($curl);
    // dd($jsondata);
    curl_close($curl);

    $this->{"_insertinto_$params[0]"}( json_decode($jsondata) );


    $storepath = PPS_APP_APPSPATH . '/../data/crawldaily';
    // dd($storepath);
    if( !is_dir($storepath) ){
      mkdir($storepath, 0777);
      chmod($storepath, 0777);
    }
    $filename = date('Ymd_H').'_'.$params[0].'.json';

    $fp = fopen(realpath($storepath).'/'.$filename, 'w+');
    fwrite($fp, json_encode(json_decode($jsondata), JSON_PRETTY_PRINT));
    fclose($fp);
    chmod( realpath($storepath) . '/' . $filename, 0777);  //changed to add the zero

    exit();
  }


  protected function _insertinto_boarddata($jsondata)
  {
    $timestamp = time();

    $inc = 0;
    foreach ($jsondata as $raw) {
      $raw->dateTime = $timestamp;

      $Boarddata = \Egameboard\Models\Boarddata::findFirst([
        "MatchId=:m:", 'bind' => [ 'm' => $raw->MatchId, ]
      ]);

      // 目前只要原本的MatchId有寫入過不再處理該筆紀錄！
      if ($Boarddata) {
        continue ;

        // 比對其他欄位值(可能需要跑迴圈單純比對數值且需忽略型態、排序！)
        $odata = $Boarddata->toArray();
        $comobj = new stdClass;
        $comobj->MatchId = (int)$Boarddata->MatchId;
        $comobj->LinkMatchId = (int)$Boarddata->LinkMatchId;
        $comobj->AwayTeam = $Boarddata->AwayTeam;
        $comobj->AwayTeamIconUrl = $Boarddata->AwayTeamIconUrl;
        $comobj->CategoryName = $Boarddata->CategoryName;
        $comobj->HomeTeam = $Boarddata->HomeTeam;
        $comobj->HomeTeamIconUrl = $Boarddata->HomeTeamIconUrl;
        $comobj->TournamentId = (int)$Boarddata->TournamentId;
        $comobj->TournamentName = $Boarddata->TournamentName;
        $comobj->SportId = (int)$Boarddata->SportId;
        $comobj->MatchName = $Boarddata->MatchName;
        $comobj->StartTime = $Boarddata->StartTime;

        $Boarddata->boarddataOdds->filter(function($row) use ( &$comobj ){
          $data = new stdClass;
          $data->Id = (int)$row->Id;
          $data->OddTypeId = (int)$row->OddTypeId;
          $data->ProviderId = (int)$row->ProviderId;
          $data->Value = (float)$row->Value;
          $data->isBest = (int)$row->isBest;
          $data->sort_best = ($row->sort_best != null) ? (int)$row->sort_best : null;
          $data->sort = (int)$row->sort;
          $comobj->Odds[] = $data;
        });

      }else{
        $Boarddata = new \Egameboard\Models\Boarddata();
      }

      $BoarddataOdds=[];
      foreach ($raw->Odds as $idx => $odds) {
        $BoarddataOdds[$idx] = new \Egameboard\Models\BoarddataOdds();

        if (($sort_best = array_search($odds->Id, array_column((array) $raw->BestOdds, 'Id'))) !== false) {
          $raw->Odds[$idx]->isBest = 1;
          $raw->Odds[$idx]->sort_best = $sort_best;
        } else {
          $raw->Odds[$idx]->isBest = 0;
          $raw->Odds[$idx]->sort_best = null;
        }
        $raw->Odds[$idx]->sort = $idx;

        // $BoarddataOdds[$idx]->BoarddataId = $odds->BoarddataId;
        $BoarddataOdds[$idx]->Id = $odds->Id;
        $BoarddataOdds[$idx]->OddTypeId = $odds->OddTypeId;
        $BoarddataOdds[$idx]->ProviderId = $odds->ProviderId;
        $BoarddataOdds[$idx]->Value = $odds->Value;
        $BoarddataOdds[$idx]->isBest = $raw->Odds[$idx]->isBest;
        $BoarddataOdds[$idx]->sort = $raw->Odds[$idx]->sort;
        $BoarddataOdds[$idx]->sort_best = $raw->Odds[$idx]->sort_best;

        // var_dump((array)$raw->Odds);
      }
      // unset($raw->BestOdds);
      // unset($raw->Odds);

      $Boarddata->assign((array) $raw);
      $Boarddata->BoarddataOdds = $BoarddataOdds;
      if (!$Boarddata->save()) {
        var_dump((array) $raw);
        exit('Insert into boarddata error occure! '. implode(',', $Boarddata->getMessages()));
      }

      $inc++;
    }

    if( $inc ){
      echo ('Aleardy insert into '.$inc.' records');
    }else{
      echo ('No new records');
    }
  }


  protected function _insertinto_bettypes($jsondata)
  {
    foreach ($jsondata as $raw) {
      $data=\Egameboard\Models\Bettypes::findFirst([
        "MarketId=:m: AND Name=:n: AND LinkOddTypeId=:l:", 'bind'=>[
        'm' => $raw->MarketId,
        'n' => $raw->Name,
        'l' => $raw->LinkOddTypeId,
      ]]);
      if( $data ){ continue; }

      $Bettypes = new \Egameboard\Models\Bettypes();
      $Bettypes->assign((array)$raw);
      if( !$Bettypes->save() ){
        var_dump((array) $raw);
        exit('Insert into bettypes error occure! ');
      }
    }
  }


  protected function _insertinto_providers($jsondata)
  {
    foreach ($jsondata as $raw) {
      $Providers = \Egameboard\Models\Providers::findFirst($raw->ProviderId);
      if (!$Providers) {
        $Providers = new \Egameboard\Models\Providers();
      }
      $raw->IsVisible = ($raw->IsVisible) ? 1 : 0;
      $raw->IsEnabled = ($raw->IsEnabled) ? 1 : 0;

      $Providers->assign((array) $raw);
      if (!$Providers->save()) {
        var_dump((array) $raw);
        exit('Insert into providers error occure! ');
      }
    }
  }


  protected function _insertinto_sports($jsondata)
  {
    foreach ($jsondata as $raw) {
      $Sports = \Egameboard\Models\Sports::findFirst($raw->Id);
      if (!$Sports) {
        $Sports = new \Egameboard\Models\Sports();
      }
      $raw->IsEnabled = ($raw->IsEnabled) ? 1 : 0;

      $Sports->assign((array) $raw);
      if (!$Sports->save()) {
        var_dump((array) $raw);
        exit('Insert into sports error occure! ');
      }
    }
  }


  protected function _insertinto_translations($jsondata)
  {
    foreach ($jsondata as $raw) {
      $Translations = \Egameboard\Models\Translations::findFirst([
        "Label=:l: AND LanguageCode=:c:", 'bind' => [
          'l' => $raw->Label,
          'c' => $raw->LanguageCode,
        ]
      ]);
      if (!$Translations) {
        $Translations = new \Egameboard\Models\Translations();
      }

      $Translations->assign((array) $raw);
      if (!$Translations->save()) {
        var_dump((array) $raw);
        exit('Insert into translations error occure! ');
      }
    }
  }
}

