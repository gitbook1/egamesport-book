<?php
/**
 *
 */
namespace Egameboard\Plugins;

class LoadAssets extends \Personalwork\Annotations\LoadAssets
{
  public function afterDispatchLoop(\Phalcon\Events\Event $event, \Phalcon\Mvc\Dispatcher $dispatcher)
  {
      $annotations = $this->annotations->getMethod(
          $dispatcher->getControllerClass(),
          $dispatcher->getActiveMethod()
      );

      if( $dispatcher->getModuleName() == 'backend' ){
        $this->assets->collection("headerStyle")->addCss('misc/css/main.css', true);
      }elseif( $dispatcher->getModuleName() == 'core' ){
        $this->assets->collection("headerStyle")->addCss('misc/css/my-custom-styles.css', true);
      }

  }
}