<?php
use Phalcon\Mvc\Router\Group as RouterGroup;

$router = new \Phalcon\Mvc\Router\Annotations(false);
$router->setUriSource(\Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

/**
 * 配置預設route指向初始化Action
 * */
$router->add('/', array(
    'namespace' => ucfirst('egameboard').'\Core\Controllers',
    'module' => 'core',
    'controller' => 'default',
    'action' => 'index'
));

/**
 * 指向DefaultController下Action
 * 預設針對loginAction與logoutAction
 * */
$router->add('/:action', array(
    'namespace' => ucfirst('egameboard') . '\Core\Controllers',
    'module' => 'core',
    'controller' => 'default',
    'action' => 1
));
$router->add('/betting/{id:[0-9]+}', array(
    'namespace' => ucfirst('egameboard') . '\Core\Controllers',
    'module' => 'core',
    'controller' => 'default',
    'action' => 'bettingDetail',
));

/**
 * 配置後台相關routes
 * */
$router->add('/backend', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 'default',
    'action' => 'index'
));
$router->add('/backend/:controller/:action/:int', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 1,
    'action' => 2,
    'pId' => 3,
));
$router->add('/backend/:controller/:action', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 1,
    'action' => 2
));
$router->add('/backend/:controller', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 1,
    'action' => 'index'
));
$router->add('/backend/categoryDelete', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 'default',
    'action' => 'categoryDelete'
));
$router->add('/backend/configSave', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 'default',
    'action' => 'configSave'
));
$router->add('/backend/contactus', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 'default',
    'action' => 'contactus'
));
$router->add('/backend/login', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 'default',
    'action' => 'login'
));
$router->add('/backend/logout', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 'default',
    'action' => 'logout'
));
$router->add('/backend/forgetps', array(
    'namespace' => ucfirst('egameboard') . '\Backend\Controllers',
    'module' => 'backend',
    'controller' => 'default',
    'action' => 'forgetps'
));


/**
 * 使用Annotation routes設定方式
 * */
$router
    // ->addResource(ucfirst('egameboard')."\Controllers\Index", "/")
    ->notFound(
        [
            "namespace"  => ucfirst('egameboard').'\Core\Controllers',
            "controller" => "index",
            "action"     => "notfound404"
        ]
    )
    ->removeExtraSlashes(true);

return $router;