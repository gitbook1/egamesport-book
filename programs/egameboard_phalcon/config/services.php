<?php

$di = new \Phalcon\Di\FactoryDefault();

/**
 * Registering a global config
 */
$di->setShared('config', function () {

    $config = include PPS_APP_APPSPATH . "/../config/config.php";

    if ( is_readable(PPS_APP_APPSPATH . '/../config/config-dev.php')) {
        $override = include PPS_APP_APPSPATH . '/../config/config-dev.php';
        $config->merge($override);
    }
    if ( is_readable(PPS_APP_APPSPATH . '/../config/config-local.php')) {
        $override = include PPS_APP_APPSPATH . '/../config/config-local.php';
        $config->merge($override);
    }

    return $config;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $url = new Phalcon\Mvc\Url();
    $url->setBaseUri($this->get('config')->application->baseUri);
    return $url;
});


// composer vendor
if (!file_exists(PPS_APP_APPSPATH . '/../vendor/autoload.php')) {
    echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
    exit(1);
}
require_once PPS_APP_APPSPATH . '/../vendor/autoload.php';

/**
 * Register composer vendor in autoloader
 **/
$loader = new \Phalcon\Loader();
$loader->registerNamespaces([
    ucfirst('egameboard').'\Controllers'       => $di->get('config')->application->controllersDir,
    ucfirst('egameboard').'\Models'            => $di->get('config')->application->modelsDir,
    ucfirst('egameboard').'\Plugins'           => $di->get('config')->application->pluginsDir,
    ucfirst('egameboard').'\Forms'             => $di->get('config')->application->formsDir,
])->register();
$di->set('autoloader', function() use ($loader) {
    $loader->registerDirs([
        $this->get('config')->application->libraryDir."/OAuth/",
    ]);
    return $loader;
});


$di->setShared('router', function () {
    $router = include_once 'routes.php';
    return $router;
});


if( is_file(PPS_APP_APPSPATH . '/../config/navigation.json') ){
$di['navigation'] = function() {
    $nav = new \Phalcon\Config\Adapter\Json( PPS_APP_APPSPATH . '/../config/navigation.json' );
    return new \Personalwork\Navigation( $nav->navigation );
};
}

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di['db'] = function () {
    $db = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
        "host"      => $this->get('config')->database->host,
        "username"  => $this->get('config')->database->username,
        "password"  => $this->get('config')->database->password,
        "dbname"    => $this->get('config')->database->dbname,
        'charset'   => $this->get('config')->database->charset,
        "options"   => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ));
    // if timezone for Asia\Taiepi
    // $db->query("SET time_zone = '+8:00';");

    return $db;
};

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new \Phalcon\Mvc\Model\Metadata\Memory();
});

/**
 * Set the models cache service
 * 後續只要直接在ORM物件內
 * 配置array('cache'=>array('key'=>[keyname],'lifetime'=>[sec]))
 * 即可使用
 * */
$di->set('modelsCache', function () {
    // Cache data for one day by default
    $frontCache = new \Phalcon\Cache\Frontend\Data(
        array(
            "lifetime" => 86400
        )
    );

    $path=PPS_APP_APPSPATH."/../data/cache";
    if( !is_dir($path) ){
        @mkdir($path, 0777);
        @chmod($path, 0777);
    }
    $cachepath = realpath($path).'/';
    // Memcached connection settings
    $cache = new \Phalcon\Cache\Backend\File(
        $frontCache,
        array(
            "cacheDir" => $cachepath
        )
    );

    return $cache;
});


$di->set('logger', function () {

    $logger = new \Personalwork\Logger\Adapter\Database(array(
                    'db'    => $this->get('db'),
                    'table' => "logger",
                    'name'  => 'info',
                    'router'=> $this->get('router'),
                    'request'=> $this->get('request'),
                    'session'=> $this->get('session'),
                  ));

    return $logger;
});


/**
 * Starts the session the first time some component requests the session service
 */
$di->setShared('session', function () {
    $path=PPS_APP_APPSPATH."/../data/cache";
    if( !is_dir($path) ){
        @mkdir($path, 0777);
        @chmod($path, 0777);
    }
    $cachepath = realpath($path);
    // set session path
    ini_set('session.save_path', $cachepath );
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();

    return $session;
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set( 'flashSession', function(){
    return new \Phalcon\Flash\Session(array(
        'error' => 'alert alert-danger text-center',
        'success' => 'alert alert-success text-center',
        'notice' => 'alert alert-info text-center',
        'warning' => 'alert alert-warning text-center',
    ));
});


/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($di) {
    $view = new \Phalcon\Mvc\View();
    $view->registerEngines(array(
        '.volt' => function ($view, $di){
            $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
            $volt->setOptions(array(
                'compiledPath' => $di->get('config')->application->cacheDir,
                'compiledSeparator' => '_',
                'stat' => true,
                'compileAlways' => true
            ));
            $compiler = $volt->getCompiler();
            $compiler->addFunction('count',
                        function($key) {
                            return "count({$key})";
                        });
            // 語系機制
            $compiler->addFunction(
                'lang',
                function () {
                    // return 'Vokuro\Utils\MyTags::lang()';
                }
            );

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));
    $view->setViewsDir($di->get('config')->application->viewsDir);
    $view->setLayout('page-default');
    return $view;
});


/**
 * 實作類似Zend-Framework aaBa => aa-ba
 * @see http://docs.phalconphp.com/en/latest/reference/dispatching.html#camelize-action-names
 *
 * @todo 附加LoadAssets.php載入前端第三方套件
 * @todo 附加身份驗證檢查
 */
$di['dispatcher'] = function() use ($di) {
    $dispatcher = new \Phalcon\Mvc\Dispatcher();

    $eventsManager = $di->getShared('eventsManager');
    //Camelize actions
    $eventsManager->attach("dispatch:beforeDispatchLoop", function($event, $dispatcher) {
        $dispatcher->setActionName(\Phalcon\Text::camelize($dispatcher->getActionName()));
    });

    $pluginname = ucfirst('egameboard').'\Plugins\LoadAssets';
    $eventsManager->attach("dispatch", new $pluginname($this) );

    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
};