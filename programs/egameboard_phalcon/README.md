# egameboard 專案說明文件

---
## 更版資訊
> 首號0為開發/測試階段，1為正式上線後 [0].6dev
> 次號為新功能完成或更新時跳號紀錄 0.[6]dev
> 次子號單數為相對發佈版(功能為調整、bug修正)、雙數為臨時釋出紀錄版(上版後補充修正時) 0.6.[1]dev
> 附加標記：dev開發 | alpha前期測試 | beta測試 | release發布 | stable穩定釋出


## 結構注意
- apps/controllers/DefaultController.php檔案內必須配置
```php
    public function initialize() {
        // 呼叫Phalcon建構子
        parent::initialize();
    }
```


## composer套件說明與連結
### require
1. [ext-phalcon]
  phalcon預設函式庫
2. [phalcon/incubator](https://github.com/phalcon/incubator)
  This is a repository to publish/share/experiment with new adapters, prototypes or functionality that can potentially be incorporated into the Phalcon Framework.
3. [sanhuang/phalcon-personalwork]
  參考php第三方套件自行開發的擴充函式庫

### require-dev
1. [snowair/phalcon-debugbar](https://github.com/snowair/phalcon-debugbar)
  使用非侵入式debuger套件來除錯phalcon專案
2. [phpunit/phpunit](https://github.com/sebastianbergmann/phpunit)
  PHPUnit is a programmer-oriented testing framework for PHP. It is an instance of the xUnit architecture for unit testing frameworks.
3. [fzaninotto/faker](https://github.com/fzaninotto/Faker)
  透過程式直接快速產生大量測試用假資料，包含檔案、圖片、文字內容並可指定格式，產出到資料庫、指定檔案路徑。
4. [phalcon/dd](https://github.com/phalcon/dd)
  This package will add the dd and dump helpers to your Phalcon application.
5. [m1ome/phalcon-datatables](https://github.com/m1ome/phalcon-datatables)
  This is a Phalcon Framework adapter for DataTables.
6. [phalcon/ide-stubs](https://github.com/phalcon/ide-stubs)
  This repo provide the most complete Phalcon Framework stubs which enables autocompletion in modern IDEs


## 專案建置後需額外安裝指令說明

- 專案建立後必須在終端機初始化指令順序
> 針對composer.json
> $ composer install

> 針對npm安裝grunt與wiredep外掛
(將grunt / grunt-wiredep 加到node全域套件內。)
> $npm install grunt --save-dev
> $npm install grunt-wiredep --save-dev
> $npm install grunt-contrib-watch --save-dev


**使用bower（javascript第三方程式前端管理套件**
```bash
#初始化安裝預設套件於.bowerrc指定目錄public/js
$bower install
#或者用於開發中專案
$bower install [packagename] --save
#透過grunt與grunt-wiredep配置
$grunt wiredep
```

**完整建置指令複製**
```bash
$ composer install ; npm install grunt --save-dev ; npm install grunt-wiredep --save-dev ; npm install grunt-contrib-watch --save-dev ; bower install ; grunt wiredep
```


## 使用phalcon指令建立ORM程式碼
首先確認在終端機下可以直接呼叫phalcon
```bash
$phalcon

Phalcon DevTools (3.4.2)
Available commands:
  commands         (alias of: list, enumerate)
  controller       (alias of: create-controller)
  module           (alias of: create-module)
  model            (alias of: create-model)
  all-models       (alias of: create-all-models)
  project          (alias of: create-project)
  scaffold         (alias of: create-scaffold)
  migration        (alias of: create-migration)
  webtools         (alias of: create-webtools)
```
由於已經配置好`configs/config.php`內必要的參數，接著就可以以下列方式產生ORM
```bash
# tableName代表congih.php設定資料庫內對應的table名稱
$phalcon model [tableName] --namespace=Personalwork\\Mvc\\Model --get-set --doc --force
```
**參數注意**
1. 配置--namespace必須以\\跳出反斜線
2. --get-set可以忽略，會對應每個table field產生get/set method，若表格欄位很多，檔案會非常長。
3. --doc附加表格註解（我附加的功能）

建置專案後，在/vendor/sanhuang/phalcon-personalwork/schemas/會有預設schema產生people / people_extendinfo / logger基本表與配置預設可登入帳號(admin/admin)。

### 使用openlitespeed本地端開發環境需設定以下

修改 /.htaccess
```bash
<IfModule mod_rewrite.c>
  RewriteEngine on
  RewriteRule  ^$ public/    [L]
  RewriteRule  (.*) public/$1 [L]
</IfModule>

#改成

<IfModule mod_rewrite.c>
  RewriteEngine on
  RewriteRule  ^$ /public/    [L]
  RewriteRule  ^(.*)$ /public/$1 [L]
</IfModule>

```

修改 /public/.htaccess
```bash
AddDefaultCharset UTF-8

<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ index.php?_url=/$1 [QSA,L]
</IfModule>

#改成

<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ /index.php?_url=/$1 [QSA,L]
</IfModule>

```